import { Injectable } from '@angular/core';
import { I18nService } from '@eui/core';

@Injectable({
  providedIn: 'root'
})
export class AppStarterService {
  constructor(protected i18nService: I18nService) {}

  start() {
    return this.i18nService.init();
  }
}
