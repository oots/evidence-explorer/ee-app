import { ELifeEventIds, IProcedure } from '@shared/models/procedure.model';
import { CountryModel } from '@shared/models/common.model';

interface ILifeEvent {
  id: string;
  name?: string;
  iconUrl?: string;
}

export const ProceduresList: IProcedure[] = [
  {
    code: 'R1',
    name: 'Requesting proof of registration of birth',
    lifeEventLabel: ELifeEventIds.birth,
    lifeEventIcon: 'birth.svg'
  },
  {
    code: 'S1',
    name: 'Requesting proof of residence',
    lifeEventLabel: ELifeEventIds.residence,
    lifeEventIcon: 'residence.svg'
  },
  {
    code: 'T1',
    name: 'Applying for a tertiary education study financing, such as study grants and loans from a public body or institution',
    lifeEventLabel: ELifeEventIds.studying,
    lifeEventIcon: 'studying.svg'
  },
  {
    code: 'T2',
    name: 'Submitting an initial application for admission to public tertiary education institution',
    lifeEventLabel: ELifeEventIds.studying,
    lifeEventIcon: 'studying.svg'
  },
  {
    code: 'T3',
    name: 'Requesting academic recognition of diplomas, certificates or other proof of studies or courses',
    lifeEventLabel: ELifeEventIds.studying,
    lifeEventIcon: 'studying.svg'
  },
  {
    code: 'U1',
    name: 'Request for determination of applicable legislation in accordance with Title II of Regulation (EC) No 883/2004',
    lifeEventLabel: ELifeEventIds.working,
    lifeEventIcon: 'working.svg'
  },
  {
    code: 'U2',
    name: 'Notifying changes in the personal or professional circumstances of the person receiving social security benefits, relevant for such benefits',
    lifeEventLabel: ELifeEventIds.working,
    lifeEventIcon: 'working.svg'
  },
  {
    code: 'U3',
    name: 'Application for a European Health Insurance Card (EHIC)',
    lifeEventLabel: ELifeEventIds.working,
    lifeEventIcon: 'working.svg'
  },
  {
    code: 'U4',
    name: 'Submitting an income tax declaration',
    lifeEventLabel: ELifeEventIds.working,
    lifeEventIcon: 'working.svg'
  },
  {
    code: 'V1',
    name: 'Registering a change of address',
    lifeEventLabel: ELifeEventIds.moving,
    lifeEventIcon: 'moving.svg'
  },
  {
    code: 'V2',
    name: 'Registering a motor vehicle originating from or already registered in a Member State, in standard procedures',
    lifeEventLabel: ELifeEventIds.moving,
    lifeEventIcon: 'moving.svg'
  },
  {
    code: 'V3',
    name: 'Obtaining stickers for the use of the national road infrastructure: time-based charges (vignette), distance-based charges (toll), issued by a public body or institution',
    lifeEventLabel: ELifeEventIds.moving,
    lifeEventIcon: 'moving.svg'
  },
  {
    code: 'V4',
    name: 'Obtaining emission stickers issued by a public body or institution',
    lifeEventLabel: ELifeEventIds.moving,
    lifeEventIcon: 'moving.svg'
  },
  {
    code: 'W1',
    name: 'Claiming pension and pre-retirement benefits from compulsory schemes',
    lifeEventLabel: ELifeEventIds.retiring,
    lifeEventIcon: 'retiring.svg'
  },
  {
    code: 'W2',
    name: 'Requesting information on the data related to pension from compulsory schemes',
    lifeEventLabel: ELifeEventIds.retiring,
    lifeEventIcon: 'retiring.svg'
  },
  {
    code: 'X1',
    name: 'Notification of business activity, permission for exercising a business activity, changes of business activity and the termination of a business activity not involving insolvency or liquidation procedures, excluding the initial registration of a business activity with the business register and excluding procedures concerning the constitution of or any subsequent filing by companies or firms within the meaning of the second paragraph of Article 54 TFEU',
    lifeEventLabel: ELifeEventIds.business,
    lifeEventIcon: 'business.svg'
  },
  {
    code: 'X2',
    name: 'Registration of an employer (a natural person) with compulsory pension and insurance schemes',
    lifeEventLabel: ELifeEventIds.business,
    lifeEventIcon: 'business.svg'
  },
  {
    code: 'X3',
    name: 'Registration of employees with compulsory pension and insurance schemes',
    lifeEventLabel: ELifeEventIds.business,
    lifeEventIcon: 'business.svg'
  },
  {
    code: 'X4',
    name: 'Submitting a corporate tax declaration',
    lifeEventLabel: ELifeEventIds.business,
    lifeEventIcon: 'business.svg'
  },
  {
    code: 'X5',
    name: 'Notification to the social security schemes of the end of contract with an employee, excluding procedures for the collective termination of employee contracts',
    lifeEventLabel: ELifeEventIds.business,
    lifeEventIcon: 'business.svg'
  },
  {
    code: 'X6',
    name: 'Payment of social contributions for employees',
    lifeEventLabel: ELifeEventIds.business,
    lifeEventIcon: 'business.svg'
  }
];

export const COUNTRIES: CountryModel[] = [
  {
    code: 'AT',
    name: 'Austria',
    iconClass: 'austria'
  },
  {
    code: 'BE',
    name: 'Belgium',
    iconClass: 'belgium'
  },
  {
    code: 'BG',
    name: 'Bulgaria',
    iconClass: 'bulgaria'
  },
  {
    code: 'CY',
    name: 'Cyprus',
    iconClass: 'cyprus'
  },
  {
    code: 'CZ',
    name: 'Czechia',
    iconClass: 'czech-republic'
  },
  {
    code: 'DE',
    name: 'Germany',
    iconClass: 'germany'
  },
  {
    code: 'DK',
    name: 'Denmark',
    iconClass: 'denmark'
  },
  {
    code: 'EE',
    name: 'Estonia',
    iconClass: 'estonia'
  },
  {
    code: 'EL',
    name: 'Greece',
    iconClass: 'greece'
  },
  {
    code: 'ES',
    name: 'Spain',
    iconClass: 'spain'
  },
  {
    code: 'FI',
    name: 'Finland',
    iconClass: 'finland'
  },
  {
    code: 'FR',
    name: 'France',
    iconClass: 'france'
  },
  {
    code: 'HR',
    name: 'Croatia',
    iconClass: 'croatia'
  },
  {
    code: 'HU',
    name: 'Hungary',
    iconClass: 'hungary'
  },
  {
    code: 'IE',
    name: 'Ireland',
    iconClass: 'ireland'
  },
  // {
  //     code: "IS",
  //     name: "Iceland",
  //     iconClass: "iceland",
  // },
  {
    code: 'IT',
    name: 'Italy',
    iconClass: 'italy'
  },
  // {
  //     code: "LI",
  //     name: "Liechtenstein",
  //     iconClass: "liechtenstein",
  // },
  {
    code: 'LT',
    name: 'Lithuania',
    iconClass: 'lithuania'
  },
  {
    code: 'LU',
    name: 'Luxembourg',
    iconClass: 'luxembourg'
  },
  {
    code: 'LV',
    name: 'Latvia',
    iconClass: 'latvia'
  },
  {
    code: 'MT',
    name: 'Malta',
    iconClass: 'malta'
  },
  {
    code: 'NL',
    name: 'Netherlands',
    iconClass: 'netherlands'
  },
  // {
  //     code: "NO",
  //     name: "Norway",
  //     iconClass: "norway",
  // },
  {
    code: 'PL',
    name: 'Poland',
    iconClass: 'poland'
  },
  {
    code: 'PT',
    name: 'Portugal',
    iconClass: 'portugal'
  },
  {
    code: 'RO',
    name: 'Romania',
    iconClass: 'romania'
  },
  {
    code: 'SE',
    name: 'Sweden',
    iconClass: 'sweden'
  },
  {
    code: 'SI',
    name: 'Slovenia',
    iconClass: 'slovenia'
  },
  {
    code: 'SK',
    name: 'Slovakia',
    iconClass: 'slovakia'
  }
];

export const memberStatesTotalNumber = COUNTRIES.length;
