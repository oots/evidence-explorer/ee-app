export function getSubstring(text: string, maxLength: number): string {
  if (text.length > maxLength) {
    return `${text.slice(0, maxLength)}...`;
  }
  return text;
}

export function sortItems(valueA: string | number, valueB: string | number, order: 'ascending' | 'descending'): number {
  if (order === 'ascending') {
    if (valueA > valueB) return 1;
    if (valueA < valueB) return -1;
    return 0;
  }
  if (order === 'descending') {
    if (valueA < valueB) return 1;
    if (valueA > valueB) return -1;
    return 0;
  }
}
