export enum FilterEnum {
  DOMAIN = 'domains',
  PROCEDURE = 'parent',
  NAME = 'name',
  LIFE_EVENT = 'lifeEventId',
  STATUS = 'status',
  COUNTRY_CODE = 'countryCode'
}

export enum DataTypeEnum {
  PROCEDURE,
  REQUIREMENT,
  EVIDENCE,
  AUTHORITY
}

export enum SortOrderEnum {
  ascending = 'asc',
  descending = 'desc'
}
