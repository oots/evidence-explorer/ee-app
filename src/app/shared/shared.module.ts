import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';

import { EclAllModule } from '@eui/ecl';
import { MessageBoxComponent } from './components/message/message-box.component';
import { TableComponent } from './components/table/table.component';
import { CountDirective } from '@shared/directives/count.directive';
import { TruncatePipe } from './pipes/truncate.pipe';
import { EuiTooltipDirectiveModule } from '@eui/components/directives';
import { TablePaginationComponent } from './components/table/table-pagination/table-pagination.component';
import { MiniCardComponent } from '@shared/components/mini-card/mini-card.component';

const MODULES = [RouterModule, EclAllModule, TranslateModule, CommonModule, ReactiveFormsModule, EuiTooltipDirectiveModule];

const DECLARATIONS = [MessageBoxComponent, TableComponent, CountDirective, TruncatePipe, TablePaginationComponent, MiniCardComponent];

@NgModule({
  imports: [...MODULES],
  declarations: [...DECLARATIONS],
  exports: [...MODULES, ...DECLARATIONS]
})
export class SharedModule {}
