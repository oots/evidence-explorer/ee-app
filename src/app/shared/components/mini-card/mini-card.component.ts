import { Component, Input } from '@angular/core';
import { DataTypeEnum } from '@shared/enums/common.enum';

@Component({
  selector: 'app-mini-card',
  templateUrl: './mini-card.component.html',
  styleUrl: './mini-card.component.scss'
})
export class MiniCardComponent {
  @Input() cardData!: { type: DataTypeEnum; count: string | number };
}
