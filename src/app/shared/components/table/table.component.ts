import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  ETableColumnType,
  ILinkEmit,
  ITable,
  ITableCell,
  ITableColumn,
  ITableColumnWidthConfig,
  ITableConfig,
  ITableRow,
  ITableSelectFilter,
  ITableSortEvent
} from './models/table.model';
import { IMessageBoxData } from '../message/models/message-box.model';
import { sortItems } from '@shared/utils/utils';
import { memberStatesTotalNumber } from '@shared/utils/constants';
import { IPaginationEvent } from '@shared/models/common.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges {
  @Input() tableData: ITable;
  @Input() tableConfig: ITableConfig;
  @Input() currentTextFilter: string;
  @Input() currentSelectFilters: ITableSelectFilter[] = [];
  @Input() isMobile: boolean;
  @Input() backendSortAndPagination: boolean = false;
  @Input() defaultSort: ITableSortEvent;
  @Input() forcePageSize: boolean = false;
  @Output() rowClick: EventEmitter<string> = new EventEmitter<string>();
  @Output() linkClick: EventEmitter<ILinkEmit> = new EventEmitter<ILinkEmit>();
  @Output() updateTotalItems: EventEmitter<number> = new EventEmitter<number>();
  @Output() chipClicked: EventEmitter<string> = new EventEmitter<string>();
  @Output() paginationChanged: EventEmitter<IPaginationEvent> = new EventEmitter<IPaginationEvent>();
  @Output() sortChanged: EventEmitter<ITableSortEvent> = new EventEmitter<ITableSortEvent>();

  pageSizeSelector: FormControl = new FormControl();
  tableContent: ITableRow[] = [];
  dataSource: ITableRow[] = [];
  paginator: IPaginationEvent;
  currentSort: ITableSortEvent;
  messageBoxData: IMessageBoxData;
  tableColumnType = ETableColumnType;
  pageSize: number;
  pageSizeOptions: number[];
  backendPagination: IPaginationEvent = { page: 1, pageSize: 10, showingFrom: 1, showingTo: 10, totalItems: 0 };
  private activeSelectFilters: ITableSelectFilter[] = [];
  private defaultPageSizeOptions: number[] = [5, 10, 20, 50, 75, 100];

  constructor(private readonly router: Router) {}

  ngOnInit(): void {
    this.setTableData();
    this.setDefaultSort();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.tableData.body?.length > 0) {
      if (changes?.currentTextFilter && !changes.currentTextFilter.firstChange) {
        this.getTextFilterData();
      }
      if (changes?.currentSelectFilters && !changes.currentSelectFilters.firstChange) {
        this.activeSelectFilters = this.currentSelectFilters
          ? this.currentSelectFilters.filter((filter: ITableSelectFilter) => filter.filterValues?.length > 0)
          : [];
        this.getSelectFilterData();
      }
    }
    if (!changes['tableData']?.firstChange && this.router.url.includes('member-state')) {
      this.setTableData();
    }
  }

  sort = (event: ITableSortEvent): void => {
    if (this.backendSortAndPagination) {
      this.sortChanged.emit(event);
    } else {
      const sortOrder = event.sortOrder ? event.sortOrder : undefined;
      const columnId: string = event.columnId;
      if (sortOrder) {
        this.tableContent.sort((a: ITableRow, b: ITableRow): number => {
          const [valueA, valueB] = this.getSortedValues(a, b, columnId);
          return sortItems(valueA, valueB, sortOrder);
        });
      } else {
        this.tableContent = [...this.dataSource.slice(this.paginator.showingFrom - 1, this.paginator.showingTo)];
      }
      this.currentSort = { sortOrder, columnId };
    }
  };

  pagination = (event: IPaginationEvent): void => {
    if (this.backendSortAndPagination) {
      this.paginationChanged.emit(event);
    } else {
      this.paginator = {
        ...event
      };
      this.tableContent = [...this.dataSource.slice(event.showingFrom - 1, event.showingTo)];
      if (this.currentSort && this.currentSort.sortOrder) this.sort(this.currentSort);
    }
  };

  getColumnHeaderName = (id: string): string => {
    const column: ITableColumn = this.tableData.header.find((column: ITableColumn) => column.id === id);
    if (column) {
      return column.name;
    }
    return '';
  };

  getProgressBarFill(value: number): string {
    if (value) {
      const total: number = memberStatesTotalNumber;
      return `${(value / total) * 100}%`;
    }
    return '0%';
  }

  onLinkClick = (columnId: string, valueId: string): void => {
    this.linkClick.emit({ columnId, valueId });
  };

  onRowClick = (row: ITableRow): void => {
    if (this.tableConfig.clickableRows) {
      const valueId: string = row.find((cell: ITableCell) => cell.valueId)?.valueId[0];
      this.rowClick.emit(valueId);
    }
  };

  getColumnWidth(columnId: string): string {
    if (this.isMobile) {
      return 'auto';
    }
    const width: string = this.tableConfig.columnWidths
      ? this.tableConfig.columnWidths.find((columnWidthConfig: ITableColumnWidthConfig) => columnWidthConfig.columnId === columnId)?.width
      : 'auto';
    return width;
  }

  getColumnType = (columnId: string): string => {
    return this.tableData.header.find((column: ITableColumn) => column.id === columnId).type;
  };

  hasTooltip = (): boolean => {
    const index: number = this.tableData.header.findIndex((column: ITableColumn) => column.tooltipText && column.tooltipText.length > 0);
    if (index !== -1) return true;
    return false;
  };

  getTooltipText = (): string[] => {
    const index: number = this.tableData.header.findIndex((column: ITableColumn) => column.tooltipText && column.tooltipText.length > 0);
    if (index !== -1) {
      return this.tableData.header[index].tooltipText;
    }
    return [];
  };

  getEventPath = (eventIcon: string): string => {
    if (eventIcon) {
      return `assets/icons/${eventIcon}`;
    }
    return '';
  };

  checkSortableColumn = (columnId: string): string => {
    if (this.tableConfig.sortableColumns?.includes(columnId)) return columnId;
    return undefined;
  };

  getLocalName = (value: string, part: 'code' | 'title'): string => {
    const valueParts: string[] = value.split('#');
    const code: string = valueParts.length === 1 ? '' : valueParts[0];
    const title: string = valueParts.length === 1 ? valueParts[0] : valueParts[1];
    if (part === 'code') return code.toUpperCase();
    return title;
  };

  orderRowItems = (row: ITableCell[]): ITableCell[] => {
    const orderedRow: ITableCell[] = [];
    this.tableData.header.map((column: ITableColumn) => {
      const value: ITableCell = row.find((item: ITableCell) => item.columnId === column.id);
      orderedRow.push(value);
    });
    return orderedRow;
  };

  onChipClicked(id: string) {
    this.chipClicked.emit(id);
  }

  private getSortedValues(a: ITableRow, b: ITableRow, columnId: string): (string | number)[] {
    let valueA = a.find((cell: ITableCell) => cell.columnId === columnId)?.value;
    let valueB = b.find((cell: ITableCell) => cell.columnId === columnId)?.value;
    if (Array.isArray(valueA)) valueA = valueA[0];
    if (Array.isArray(valueB)) valueB = valueB[0];
    if (typeof valueA === 'string') valueA = valueA.toLowerCase();
    if (typeof valueB === 'string') valueB = valueB.toLowerCase();
    return [valueA, valueB];
  }

  private getTextFilterData = (provideFilteredValues?: boolean): ITableRow[] => {
    if (this.currentTextFilter) {
      const filteredItems: ITableRow[] = [];
      let initialData: ITableRow[];
      if (provideFilteredValues) {
        initialData = this.tableData.body;
      } else {
        initialData = this.activeSelectFilters.length > 0 ? this.getSelectFilterData(true) : this.tableData.body;
      }
      initialData.map((row: ITableRow) => {
        if (row.some(this.checkTextFilterValue)) {
          filteredItems.push(row);
        }
      });
      if (provideFilteredValues) {
        return filteredItems;
      } else {
        this.setFilteredValues(filteredItems);
      }
    } else {
      if (this.activeSelectFilters.length > 0) {
        this.getSelectFilterData();
      } else {
        this.setFilteredValues(this.tableData.body);
      }
    }
    return [];
  };

  private checkTextFilterValue = (cell: ITableCell): boolean => {
    const allowFilter: boolean = this.tableData.header.find((column: ITableColumn) => column.id === cell.columnId)?.allowFilter;
    if (allowFilter) {
      const valueToFilter: (string | number)[] = Array.isArray(cell.value) ? cell.value : [cell.value];
      const match = valueToFilter.find((value: string | number) =>
        value.toString().toLowerCase().includes(this.currentTextFilter.toLowerCase())
      );
      if (match) {
        return true;
      }
    }
    return false;
  };

  private getSelectFilterData = (provideFilteredValues?: boolean): ITableRow[] => {
    if (this.activeSelectFilters.length > 0) {
      const filteredItems: ITableRow[] = [];
      let initialData: ITableRow[];
      if (provideFilteredValues) {
        initialData = this.tableData.body;
      } else {
        initialData = this.currentTextFilter ? this.getTextFilterData(true) : this.tableData.body;
      }
      const filterColumnsIds: string[] = this.activeSelectFilters.map((filter: ITableSelectFilter) => filter.columnId);
      initialData.map((row: ITableRow) => {
        const cellsToFilter: ITableCell[] = row.filter((cell: ITableCell) => filterColumnsIds.includes(cell.columnId));
        if (cellsToFilter.every(this.checkSelectFilterValue)) {
          filteredItems.push(row);
        }
      });
      if (provideFilteredValues) {
        return filteredItems;
      } else {
        this.setFilteredValues(filteredItems);
      }
    } else {
      if (this.currentTextFilter) {
        this.getTextFilterData();
      } else {
        this.setFilteredValues(this.tableData.body);
      }
    }
    return [];
  };

  private checkSelectFilterValue = (cell: ITableCell): boolean => {
    const filterApplied: ITableSelectFilter = this.activeSelectFilters.find(
      (filter: ITableSelectFilter) => filter.columnId === cell.columnId
    );
    if (filterApplied) {
      const cellValues: (string | number)[] = Array.isArray(cell.value) ? cell.value : [cell.value];
      return filterApplied.filterValues.some((value) => cellValues.includes(value));
    }
    return false;
  };

  private setFilteredValues = (data: ITableRow[]): void => {
    if (data?.length > 0) {
      this.dataSource = [...data];
      this.setPageSizeParams();
      this.tableContent = [...data.slice(0, this.pageSize)];
    } else {
      this.dataSource = [];
      this.tableContent = [];
      this.messageBoxData = {
        title: 'requirements-page.main.filter.message-box.empty-state.title',
        description: 'requirements-page.main.filter.message-box.empty-state.description',
        state: 'info'
      };
    }
    this.updateTotalItems.emit(this.dataSource.length);
    this.resetPaginator();
    if (this.currentSort?.sortOrder && this.tableContent.length > 0) this.sort(this.currentSort);
  };

  private setPageSizeParams = (): void => {
    const length = this.backendPagination ? (this.forcePageSize ? this.tableData.totalItems : 10) : 20;
    if (this.tableConfig.showPaginator && this.dataSource.length > 5) {
      this.pageSizeOptions = [...this.defaultPageSizeOptions];
      this.pageSizeOptions = this.pageSizeOptions.filter((option: number) => option <= this.dataSource.length);
      if (!this.pageSizeOptions.includes(this.dataSource.length)) {
        this.pageSizeOptions.push(this.dataSource.length);
      }
      this.pageSize = this.dataSource.length >= length ? length : this.dataSource.length;
      this.pageSizeSelector.setValue(this.pageSize);
    } else {
      this.pageSize = this.dataSource.length;
    }
  };

  private trackPageSizeSelector = (): void => {
    this.pageSizeSelector.valueChanges.subscribe((value) => {
      this.pageSize = +value;
      this.pagination({ page: 1, showingFrom: 1, showingTo: this.pageSize });
    });
  };

  private resetPaginator = (): void => {
    this.paginator = {
      page: 1,
      showingFrom: 1,
      showingTo: this.pageSize
    };
  };

  private setTableData() {
    if (this.tableData?.body?.length > 0) {
      const tableData: ITableRow[] = this.applyExpandableLogic();
      if (this.backendSortAndPagination) {
        this.dataSource = [...tableData];
        this.setPageSizeParams();
        this.paginator = { page: 1, showingFrom: 1, showingTo: this.pageSize };
        this.trackPageSizeSelector();
      } else {
        this.setDataWithFrontedPaginationAndFiltering(tableData);
      }
    } else {
      this.setTableEmptyState();
    }
  }

  private setTableEmptyState() {
    this.dataSource = [];
    this.tableContent = [];
    this.messageBoxData = {
      title: this.tableConfig.emptyStateTitle,
      description: this.tableConfig.emptyStateDescription,
      state: 'info'
    };
  }

  private setDataWithFrontedPaginationAndFiltering(tableData: ITableRow[]) {
    this.dataSource = [...tableData];
    this.setPageSizeParams();
    this.paginator = { page: 1, showingFrom: 1, showingTo: this.pageSize };
    this.tableContent = [...tableData.slice(0, this.pageSize)];
    this.trackPageSizeSelector();
  }

  private applyExpandableLogic() {
    return this.tableData?.body?.map((row) => {
      const mappedRow = [...row];
      mappedRow['disableExpandableRowIcon'] = !(row.find((cell) => cell.columnId === ETableColumnType.linkedDataList)?.value as string[])
        ?.length;
      return mappedRow;
    });
  }

  private setDefaultSort = () => {
    if (this.defaultSort) {
      this.sort({ sortOrder: this.defaultSort.sortOrder, columnId: this.defaultSort.columnId });
    }
  };
}
