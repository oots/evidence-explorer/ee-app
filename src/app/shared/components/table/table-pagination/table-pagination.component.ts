import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IPaginationEvent } from '@shared/models/common.model';

@Component({
  selector: 'app-table-pagination',
  templateUrl: './table-pagination.component.html',
  styleUrl: './table-pagination.component.scss'
})
export class TablePaginationComponent {
  @Input() paginationData: IPaginationEvent = {
    page: 1,
    pageSize: 10,
    showingFrom: 1,
    showingTo: 10,
    totalItems: 100
  };
  @Input() dropdownData: number[] = [5, 10, 20, 50, 75, 100];
  @Input() isMobile: boolean = false;
  @Output() paginationChanged: EventEmitter<IPaginationEvent> = new EventEmitter<IPaginationEvent>();

  onPaginationChange(pagination: IPaginationEvent) {
    const pageSize = pagination.showingFrom - pagination.showingTo + 1;
    if (this.paginationData.page === pagination.page && this.paginationData.pageSize === pageSize) return;

    this.paginationChanged.emit(pagination);
  }
}
