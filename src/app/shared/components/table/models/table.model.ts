export interface ITableColumn {
  id: string;
  name: string;
  type: ETableColumnType;
  tooltipText?: string[];
  allowFilter?: boolean;
}

export enum ETableColumnType {
  'string' = 'string',
  'number' = 'number',
  'link' = 'link',
  'progress' = 'progress',
  'flag' = 'flag',
  'event' = 'event',
  'connector' = 'connector',
  'link-connector' = 'link-connector',
  'expand' = 'expand',
  'linkedDataList' = 'linkedDataList'
}

export interface ITableCell {
  columnId: string;
  value: string | number | string[] | number[];
  valueId?: string | string[];
  flagCode?: string;
  eventIcon?: string[];
  emptyValue?: string;
  connector?: 'AND' | 'only';
  secondaryValues?: string[];
}

export type ITableRow = ITableCell[] & { opened?: boolean; disableExpandableRowIcon?: boolean };

export interface ITable {
  header: ITableColumn[];
  body: ITableRow[];
  totalItems?: number;
}

export interface ITableSortEvent {
  sortOrder: 'ascending' | 'descending';
  columnId: string;
}

export interface ITableColumnWidthConfig {
  columnId: string;
  width: string;
}

export interface ITableConfig {
  titleText?: string;
  descriptionText?: string;
  captionText?: string;
  showTotalCount?: boolean;
  showPaginator?: boolean;
  sortableColumns?: string[];
  columnWidths?: ITableColumnWidthConfig[];
  columnSpacer?: boolean;
  emptyStateTitle?: string;
  emptyStateDescription?: string;
  clickableRows?: boolean;
  isExpandable?: boolean;
  expandableRowTitle?: string;
  noDataTooltip?: string;
}

export interface ITableSelectFilter {
  columnId: string;
  filterValues: string[];
}

export interface ILinkEmit {
  columnId?: string;
  valueId?: string;
}
