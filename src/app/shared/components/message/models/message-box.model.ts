export interface IMessageBoxData {
  title?: string;
  description?: string;
  state?: 'info' | 'error';
}
