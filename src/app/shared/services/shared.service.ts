import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  scrollToTop() {
    if (window.scrollY) {
      window.scroll(0, 0);
    }
  }
}
