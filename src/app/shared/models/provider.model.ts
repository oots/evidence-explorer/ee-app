import { IJurisdictionCodes } from './common.model';

export interface IProvidersServiceQueryParams {
  'evidence-type-classification': string;
  'country-code': string;
  'jurisdiction-admin-l2'?: string;
  'jurisdiction-admin-l3'?: string;
  'jurisdiction-context-id'?: string;
  'evidence-type-id'?: string;
  evidenceProviderClasParam?: string;
  evidenceProviderClasValue?: string;
}

export interface IProvider {
  id: string;
  dataServiceIds?: IDataServiceIds;
  title?: string;
  localTitle?: string[];
  description?: string;
  localDescription?: string;
  jurisdiction?: IJurisdictionCodes;
  format?: string[];
  evidenceCount?: number;
}

interface IDataServiceIds {
  providerId?: string;
  evidenceTypeIds?: string[];
}
