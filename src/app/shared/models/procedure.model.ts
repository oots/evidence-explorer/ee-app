import { IJurisdictionCodes } from './common.model';

export interface IProcedure {
  code: string;
  name: string;
  lifeEventLabel?: string;
  lifeEventIcon?: string;
  detail?: IProcedureDetail;
}

export interface IProcedureDetail {
  id: string;
  title?: string;
  localTitle?: string[];
  description?: string;
  localDescription?: string[];
  jurisdiction?: IJurisdictionCodes[];
  requirementsLinked?: IProcedureDetailRequirement[];
}

export interface IProcedureDetailRequirement {
  id: string;
  name?: string;
}

export enum ELifeEventIds {
  'birth' = 'BIRTH',
  'residence' = 'RESIDENCE',
  'studying' = 'STUDYING',
  'working' = 'WORKING',
  'moving' = 'MOVING',
  'retiring' = 'RETIRING',
  'business' = 'BUSINESS'
}
