import { IJurisdictionCodes } from './common.model';
import { IProvider } from './provider.model';

export interface IEvidenceType {
  classification: string;
  evidenceTypeListId?: string;
  id?: string;
  requirementId?: string;
  title?: string;
  localTitle?: string[];
  jurisdiction?: IJurisdictionCodes[];
  procedureCode?: string;
  providersCount?: number;
  description?: string;
  providersDetails?: IProvider[];
}

export interface IEvidenceTypeServiceQueryParams {
  'requirement-id': string;
  'country-code'?: string;
  'jurisdiction-admin-l2'?: string;
  'jurisdiction-admin-l3'?: string;
}
