import { DataTypeEnum, FilterEnum } from '@shared/enums/common.enum';

export enum ECountryCodeList {
  'AT' = 'Austria',
  'BE' = 'Belgium',
  'BG' = 'Bulgaria',
  'HR' = 'Croatia',
  'CY' = 'Cyprus',
  'CZ' = 'Czech Republic',
  'DK' = 'Denmark',
  'EE' = 'Estonia',
  'FI' = 'Finland',
  'FR' = 'France',
  'DE' = 'Germany',
  'EL' = 'Greece',
  'HU' = 'Hungary',
  'IE' = 'Ireland',
  'IT' = 'Italy',
  'LV' = 'Latvia',
  'LT' = 'Lithuania',
  'LU' = 'Luxembourg',
  'MT' = 'Malta',
  'NL' = 'Netherlands',
  'PL' = 'Poland',
  'PT' = 'Portugal',
  'RO' = 'Romania',
  'SK' = 'Slovakia',
  'SI' = 'Slovenia',
  'ES' = 'Spain',
  'SE' = 'Sweden',
  'IS' = 'Iceland',
  'LI' = 'Liechtenstein',
  'NO' = 'Norway',
  'CH' = 'Switzerland',
  'EC' = 'European Commission'
}

export interface ISelectOption {
  label: string;
  value: string;
}

export interface IJurisdictionCodes {
  countryCode?: string;
  codeLevel2?: string;
  nameLevel2?: string;
  codeLevel3?: string;
  nameLevel3?: string;
}

export interface IPaginationEvent {
  page?: number;
  showingFrom?: number;
  showingTo?: number;
  pageSize?: number;
  totalItems?: number;
}

export type EntityType = 'requirement' | 'procedure' | 'evidence' | 'provider';

export interface CountryModel {
  code?: string;
  name?: string;
  iconClass?: string;
}

export interface ResponseModel<T> {
  list: T[];
  count: number;
  request: TableOptionsModel;
}

export interface TableOptionsModel {
  pageNumber?: number;
  pageSize?: number;
  filter?: FilterModel[];
  option?: unknown;
  sort?: SortModel[];
}

export interface SortModel {
  fieldName: string;
  order: string;
}

export interface FilterModel {
  fieldName: FilterEnum;
  fieldValues: string[] | boolean[];
}

export interface MsPageProcedureModel {
  id: number;
  parent: number;
  identifier: string;
  name: string;
  countryCode: string;
  countryName: string;
  levelCode: string;
  status: string;
  lifeEventId: number;
  lifeEventName: string;
  lifeEventIdentifier: string;
  reqCount: number;
  description: string;
  parentProcedure: ParentProcedureModel;
  lastModified: string;
  requirements: LinkedRequirementModel[];
}

export interface ParentProcedureModel {
  id: number;
  identifier: string;
  name: string;
}

export interface LinkedRequirementModel {
  id: number;
  identifier: string;
  title: string;
}

export interface MsPageEvidenceModel {
  id: number;
  name: string;
  providerCount: number;
  classificationUrl: string;
  providers: LinkedProvider[];
}

export interface LinkedProvider {
  id: number;
  identifier: string;
  name: string;
}

export interface MsPageRequirementModel {
  id: number;
  name: string;
  identifier: string;
  evtpCountForCountry: number;
  domains: DomainModel[];
  evidenceTypes: LinkedEvidenceType[];
}

export interface LinkedEvidenceType {
  id: number;
  identifier: string;
  name: string;
}

export interface MsPageAuthorityModel {
  id: number;
  name: string;
  dataServiceName: string;
  status: number;
  country: string;
  countryCode: string;
  linkedEvtpCount: number;
  countryInfo: CountryInfo[];
  owner: string;
  lastModified: string;
  lastModifiedName: string;
  lastModifiedBy: number;
  identifier: string;
  description: string;
  levelCodes: LevelCode[];
}

export interface CountryInfo {
  code: string;
  name: string;
}

export interface LevelCode {
  levelCode: string;
}

export interface DomainModel {
  id: number;
  description: string;
  name: string;
}

export interface LifeEventModel {
  id: number;
  identifier: string;
  name: string;
}

export interface IAssetCountPerCountry
  extends Partial<{
    countryCode: string;
    published: number;
    review: number;
    draft: number;
    count: number;
  }> {}

export interface IAssetsCount
  extends Array<{
    type: DataTypeEnum;
    count: number;
  }> {}
