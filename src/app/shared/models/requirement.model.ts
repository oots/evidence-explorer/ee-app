import { IEvidenceType } from './evidence-type.model';
import { IProcedure } from './procedure.model';

export interface IRequirement {
  id: string;
  name?: string;
  description?: string;
  evidenceTypeList?: IEvidenceType[];
  proceduresList?: IProcedure[];
  countriesCount?: number;
  identifier?: string;
  domains?: IRequirementDomain[];
}

export interface IRequirementsServiceQueryParams {
  'procedure-id'?: string;
  'country-code'?: string;
  'jurisdiction-admin-l2'?: string;
  'jurisdiction-admin-l3'?: string;
}

export interface IRequirementDetails {
  requirementId: string;
  evidenceTypes: IEvidenceType[];
  procedures: IProcedure[];
}

export interface IRequirementDomain {
  id?: string;
  name?: string;
  description?: string;
}
