import { Directive, ElementRef, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appCountdown]'
})
export class CountDirective implements OnChanges, OnDestroy {
  @Input() endVal: number;

  private currentVal: number;
  private loopCounter;

  constructor(private elementRef: ElementRef) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.endVal.currentValue) {
      this.startCount();
      this.repeatCount();
    }
  }

  ngOnDestroy(): void {
    clearInterval(this.loopCounter);
  }

  private startCount = (): void => {
    this.currentVal = 0;
    this.elementRef.nativeElement.innerHTML = 0;
    const interval = setInterval(() => {
      if (this.currentVal < this.endVal) {
        this.currentVal++;
        this.elementRef.nativeElement.innerHTML = this.currentVal;
      } else {
        clearInterval(interval);
      }
    }, 50);
  };

  private repeatCount = (): void => {
    this.loopCounter = setInterval(this.startCount, 20000);
  };
}
