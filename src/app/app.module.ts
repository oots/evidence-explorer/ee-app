import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AppStarterService } from './app-starter.service';
import { HttpClientModule } from '@angular/common/http';
import { MonitorInterceptor } from './core/interceptors/monitor.interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BrowserAnimationsModule, CoreModule, AppRoutingModule, HttpClientModule],
  providers: [
    AppStarterService,
    {
      provide: APP_INITIALIZER,
      useFactory: (appStarterService) => () =>
        new Promise<void>((resolve) => {
          appStarterService.start().subscribe(() => resolve());
        }),
      deps: [AppStarterService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MonitorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
