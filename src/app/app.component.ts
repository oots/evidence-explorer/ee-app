import { Component } from '@angular/core';

@Component({
  template: '<app-layout/>',
  selector: 'app-root'
})
export class AppComponent {}
