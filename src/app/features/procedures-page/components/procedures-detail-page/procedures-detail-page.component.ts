import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { ETableColumnType, ITable, ITableConfig, ITableRow } from '@shared/components/table/models/table.model';
import { IProcedure, IProcedureDetailRequirement } from '@shared/models/procedure.model';
import { sortItems } from '@shared/utils/utils';
import { IMessageBoxData } from '@shared/components/message/models/message-box.model';
import { IRequirement, IRequirementDetails } from '@shared/models/requirement.model';
import { ECountryCodeList, EntityType, IJurisdictionCodes, ISelectOption } from '@shared/models/common.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import {
  IProceduresPageEvidenceCard,
  IProceduresPageEvidenceItem,
  IProceduresPageRequirementItem
} from '../../models/procedures-page.model';
import { AppState } from 'src/app/core/reducers';
import { EvidenceBrokerService } from 'src/app/core/services/evidence-broker.service';
import { DataRequirementsService } from 'src/app/core/services/data-requirements.service';
import { DataServicesDirectoryService } from 'src/app/core/services/data-services-directory.service';
import { setSelectedRequirement } from 'src/app/core/reducers/requirements/requirements.actions';
import { setSelectedEvidence } from 'src/app/core/reducers/evidences/evidences.action';
import { setBreadcrumbLabelDetail } from 'src/app/core/reducers/breadcrumb/breadcrumb.actions';
import { EMainPageSectionId } from '../../../../core/components/layout/breadcrumb/models/breadcrumb.model';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-procedures-detail-page',
  templateUrl: './procedures-detail-page.component.html',
  styleUrls: ['./procedures-detail-page.component.scss']
})
export class ProceduresDetailPageComponent implements OnInit, OnDestroy {
  countryControl = new FormControl();
  selectedCountry: string;
  isMobile: boolean;
  procedureDetail: IProcedure = null;
  requirementsList: IRequirement[] = [];
  requirementListItems: IProceduresPageRequirementItem[] = [];
  evidenceCardListItems: IProceduresPageEvidenceCard[] = [];
  pageTitle = 'procedures-page.detail.title.default';
  countryLabel: string;
  region: string;
  proceduresTableData: ITable = { header: [], body: [] };
  proceduresTableConfig: ITableConfig;
  countrySelectOptions: ISelectOption[] = [];
  dataLoading = true;
  errorState = false;
  errorMessage: IMessageBoxData = {
    title: 'common.message-box.service-error.title',
    description: 'common.message-box.service-error.description',
    state: 'error'
  };
  evidencesLoading = true;
  evidencesMessage: IMessageBoxData = {
    title: 'procedures-page.detail.evidences-section.message-box.empty-state.title',
    state: 'info'
  };

  private breakpointSubscription: Subscription;
  private stateSubscription: Subscription;
  private requirementsDetailSubscription: Subscription;
  private requirementsListSubscription: Subscription;
  private providersInfoListSubscription: Subscription;

  constructor(
    private _breakpointObserver: BreakpointObserver,
    private sharedService: SharedService,
    private _store: Store<AppState>,
    private _evidenceBrokerService: EvidenceBrokerService,
    private _dataRequirementService: DataRequirementsService,
    private _dataServiceDirectory: DataServicesDirectoryService,
    private router: Router
  ) {
    this.breakpointSubscription = this._breakpointObserver.observe(['(min-width: 996px)']).subscribe((state: BreakpointState) => {
      const size = state.breakpoints['(min-width: 996px)'] ? 'desktop' : 'mobile';
      this.isMobile = size === 'mobile';
    });
    this.requirementsListSubscription = this._dataRequirementService.requirementsList$.subscribe({
      next: (requirementsList: IRequirement[]) => {
        const linkedRequirements: string[] = this.procedureDetail.detail.requirementsLinked.map(
          (requirement: IProcedureDetailRequirement) => requirement.id
        );
        this.requirementsList = requirementsList.filter((requirement: IRequirement) => linkedRequirements.includes(requirement.id));
        this._evidenceBrokerService.getEvidenceTypeList(this.requirementsList);
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.setErrorState();
      }
    });
    this.requirementsDetailSubscription = this._evidenceBrokerService.requirementsDetails$.subscribe({
      next: (requirementDetails: IRequirementDetails[]) => {
        requirementDetails.map((requirementDetail: IRequirementDetails) => {
          const requirement: IRequirement = this.requirementsList.find(
            (requirement: IRequirement) => requirement.id === requirementDetail.requirementId
          );
          if (requirement) {
            requirement.evidenceTypeList = requirementDetail.evidenceTypes;
          }
        });
        this.setSelectOptions();
        this.setRequirementsListItems();
        if (this.procedureDetail.detail.jurisdiction.length > 1) {
          this.setDataSource();
          this.setTableConfig();
        }
        this.dataLoading = false;
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.setErrorState();
      }
    });
    this.providersInfoListSubscription = this._dataServiceDirectory.providersInfoList$.subscribe({
      next: (evidenceTypesDetails: IEvidenceType[]) => {
        this.getCardsContent(evidenceTypesDetails);
        this.evidencesLoading = false;
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.evidencesMessage = {
          title: 'common.message-box.service-error.title',
          description: 'common.message-box.service-error.description',
          state: 'error'
        };
        this.evidencesLoading = false;
      }
    });
  }

  ngOnInit(): void {
    this.sharedService.scrollToTop();
    this.stateSubscription = this._store.select('procedures').subscribe((state: AppState) => {
      if (state.selectedProcedure && state.proceduresList && this.procedureDetail === null) {
        const procedureSelected: IProcedure = state.proceduresList.find(
          (procedure: IProcedure) => procedure.detail.id === state.selectedProcedure.id
        );
        if (procedureSelected) {
          this.procedureDetail = { ...procedureSelected };
          this.pageTitle = this.procedureDetail.detail.title;
          this._store.dispatch(
            setBreadcrumbLabelDetail({
              detailLabel: {
                id: EMainPageSectionId['procedure-detail'],
                label: this.procedureDetail.detail.title
              }
            })
          );
          this.countryLabel = ECountryCodeList[this.procedureDetail.detail.jurisdiction[0].countryCode];
          this.region = state.selectedProcedure.nameLevel2;
          this._dataRequirementService.getRequirementsList();
        } else {
          this.procedureDetail = undefined;
        }
      }
      if ((!state.selectedProcedure || this.procedureDetail === undefined || state.proceduresList === undefined) && !this.errorState) {
        if (!state.selectedProcedure) {
          this.errorMessage.title = 'procedures-page.detail.message-box.empty-title';
          this.errorMessage.description = 'procedures-page.detail.message-box.empty-description';
        }
        this.setErrorState();
      }
    });
    this.trackFormControlChanges();
  }

  ngOnDestroy(): void {
    this.breakpointSubscription.unsubscribe();
    this.requirementsDetailSubscription.unsubscribe();
    this.requirementsListSubscription.unsubscribe();
    this.providersInfoListSubscription.unsubscribe();
    this.stateSubscription && this.stateSubscription.unsubscribe();
  }

  onLinkClickHandler = (id: string, elementType: EntityType): void => {
    this.stateSubscription.unsubscribe();
    let url: string;
    switch (elementType) {
      case 'requirement':
        url = '/requirements/requirement-detail';
        this._store.dispatch(setSelectedRequirement({ requirementId: id }));
        break;
      case 'provider':
        url = '/evidences/evidence-provider-detail';
        this._store.dispatch(setSelectedEvidence({ evidenceProviderId: id }));
        break;
      case 'evidence':
        url = '/evidences/evidence-type-detail';
        this._store.dispatch(setSelectedEvidence({ evidenceTypeId: id }));
        break;
    }
    this.router.navigate([url]);
  };

  onLearnMoreClicked(requirementId: string): void {
    sessionStorage.setItem('selectedRequirementId', requirementId);
    window.open('#/requirements/requirement-detail', '_blank');
  }

  getLocalName = (value: string, part: 'code' | 'title'): string => {
    const valueParts: string[] = value.split('#');
    const code: string = valueParts.length === 1 ? '' : valueParts[0];
    const title: string = valueParts.length === 1 ? valueParts[0] : valueParts[1];
    if (part === 'code') return code.toUpperCase();
    return title;
  };

  changeExpandState = (requirementId: string): void => {
    const item: IProceduresPageRequirementItem = this.requirementListItems.find(
      (requirementItem: IProceduresPageRequirementItem) => requirementItem.requirementId === requirementId
    );
    if (item.expanded) {
      item.expanded = false;
    } else {
      item.expanded = true;
    }
  };

  private getCardsContent = (evidenceTypesDetails: IEvidenceType[]): void => {
    evidenceTypesDetails.map((detail: IEvidenceType) => {
      const currentItem: IProceduresPageEvidenceCard = this.evidenceCardListItems.find(
        (item: IProceduresPageEvidenceCard) => item.listId === detail.evidenceTypeListId
      );
      const newEvidence: IProceduresPageEvidenceItem = {
        evidenceId: detail.classification,
        evidenceTitle: detail.title,
        evidenceDescription: detail.description,
        evidenceProviders: detail.providersDetails ? [...detail.providersDetails] : [],
        requirementId: detail.requirementId,
        requirementName: this.requirementsList.find((requirement: IRequirement) => requirement.id === detail.requirementId).name
      };
      if (currentItem) {
        currentItem.evidencesDetails.push(newEvidence);
      } else {
        this.evidenceCardListItems.push({
          listId: detail.evidenceTypeListId,
          evidencesDetails: [newEvidence]
        });
      }
    });
    if (this.evidenceCardListItems.length === 0) {
      this.evidencesMessage = {
        title: 'procedures-page.detail.evidences-section.message-box.empty-state.title',
        state: 'info'
      };
    }
  };

  private setTableConfig = (): void => {
    this.proceduresTableData.header = [
      { id: 'jurisdiction', name: 'procedures-page.detail.procedures-table.jurisdiction-column', type: ETableColumnType.string },
      { id: 'title', name: 'procedures-page.detail.procedures-table.title-column', type: ETableColumnType.string },
      { id: 'local', name: 'procedures-page.detail.procedures-table.local-name-column', type: ETableColumnType.string }
    ];
    this.proceduresTableConfig = {
      titleText: 'procedures-page.detail.procedures-table.title',
      descriptionText: 'procedures-page.detail.procedures-table.description',
      captionText: 'list of alternative procedures',
      columnWidths: [
        { columnId: 'jurisdiction', width: '20%' },
        { columnId: 'title', width: '40%' },
        { columnId: 'local', width: '40%' }
      ],
      columnSpacer: true
    };
  };

  private setDataSource = (): void => {
    const rows: ITableRow[] = [];
    this.procedureDetail.detail.jurisdiction.map((jurisdiction: IJurisdictionCodes) => {
      rows.push([
        { columnId: 'jurisdiction', value: [`Regional (${jurisdiction.codeLevel2})`] },
        { columnId: 'title', value: [this.procedureDetail.detail.title] },
        {
          columnId: 'local',
          value: this.procedureDetail.detail.localTitle ? [...this.procedureDetail.detail.localTitle] : [],
          emptyValue: 'common.empty-message.official-names'
        }
      ]);
    });
    this.proceduresTableData.body = [...rows];
  };

  private setSelectOptions = (): void => {
    const countries: ISelectOption[] = [];
    this.requirementsList.map((requirement: IRequirement) => {
      requirement.evidenceTypeList.map((evidenceType: IEvidenceType) => {
        const countryName: string = ECountryCodeList[evidenceType.jurisdiction[0].countryCode];
        if (!countries.map((country: ISelectOption) => country.value).includes(countryName)) {
          countries.push({ label: `common.country-label.${countryName.toLowerCase()}`, value: countryName });
        }
      });
    });
    this.countrySelectOptions = [
      { label: 'procedures-page.detail.evidences-section.country-select.placeholder', value: 'default' },
      ...countries.sort((a: ISelectOption, b: ISelectOption) => {
        const valueA: string = a.value;
        const valueB: string = b.value;
        return sortItems(valueA, valueB, 'ascending');
      })
    ];
    this.countryControl.setValue('default');
  };

  private setErrorState = (): void => {
    this.errorState = true;
    this.dataLoading = false;
  };

  private setRequirementsListItems = (country?: string): void => {
    this.requirementListItems = this.requirementsList.map((requirement: IRequirement) => {
      const countries: string[] = requirement.evidenceTypeList.map(
        (evidence: IEvidenceType) => ECountryCodeList[evidence.jurisdiction[0].countryCode]
      );
      return {
        requirementId: requirement.id,
        requirementName: requirement.name,
        requirementDescription: requirement.description,
        requirementEvidenceCountries: [...new Set(countries)]
      };
    });
  };

  private getEvidenceTypes = (country: string): void => {
    const countryCode: string = Object.entries(ECountryCodeList).find((entry) => entry[1] === country)[0];
    const evidenceTypes: IEvidenceType[] = [];
    this.requirementsList.map((requirement: IRequirement) => {
      requirement.evidenceTypeList.map((evidenceType: IEvidenceType) => {
        if (ECountryCodeList[evidenceType.jurisdiction[0].countryCode] === country) {
          evidenceTypes.push(evidenceType);
        }
      });
    });
    this._dataServiceDirectory.getEvidenceProviderListByIds(evidenceTypes, countryCode);
  };

  private trackFormControlChanges = (): void => {
    this.countryControl.valueChanges.subscribe((value: string) => {
      if (value !== 'default') {
        this.evidencesLoading = true;
        this.selectedCountry = value;
        this.evidenceCardListItems = [];
        this.getEvidenceTypes(value);
      } else {
        this.selectedCountry = undefined;
      }
    });
  };
}
