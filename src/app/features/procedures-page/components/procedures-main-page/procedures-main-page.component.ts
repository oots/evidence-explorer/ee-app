import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { ETableColumnType, ITable, ITableCell, ITableConfig, ITableRow } from '@shared/components/table/models/table.model';
import { ECountryCodeList, IJurisdictionCodes, ISelectOption } from '@shared/models/common.model';
import { IProceduresPageFormField } from '../../models/procedures-page.model';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { AppState } from 'src/app/core/reducers';
import { ActivatedRoute, Router } from '@angular/router';
import { IProcedure } from '@shared/models/procedure.model';
import { setProcedures, setSelectedProcedure } from 'src/app/core/reducers/procedures/procedures.actions';
import { sortItems } from '@shared/utils/utils';
import { IProcedureIdentifier } from 'src/app/core/reducers/procedures/models/procedures-state.model';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-procedures-main-page',
  templateUrl: './procedures-main-page.component.html',
  styleUrls: ['./procedures-main-page.component.scss']
})
export class ProceduresMainPageComponent implements OnInit, OnDestroy {
  filtersForm: FormGroup = new FormGroup({
    search: new FormControl(),
    countrySelect: new FormControl(),
    eventSelect: new FormControl()
  });
  countrySelectOptions: ISelectOption[] = [];
  eventSelectOptions: ISelectOption[] = [];
  isMobile: boolean;
  dataLoading = true;
  errorState = false;
  tableData: ITable = { header: [], body: [] };
  tableConfig: ITableConfig;
  filteredValues: IProceduresPageFormField = {
    search: null,
    countrySelect: null,
    eventSelect: null
  };

  private formSubscription: Subscription;
  private breakpointSubscription: Subscription;
  private stateSubscription: Subscription;
  private proceduresList: IProcedure[];

  constructor(
    private _breakpointObserver: BreakpointObserver,
    private _store: Store<AppState>,
    private router: Router,
    private sharedService: SharedService,
    private route: ActivatedRoute
  ) {
    this.breakpointSubscription = this._breakpointObserver.observe(['(min-width: 996px)']).subscribe((state: BreakpointState) => {
      const size = state.breakpoints['(min-width: 996px)'] ? 'desktop' : 'mobile';
      this.isMobile = size === 'mobile';
      this.setTableConfig();
    });
  }

  ngOnInit(): void {
    this.sharedService.scrollToTop();
    this.stateSubscription = this._store.select('procedures').subscribe((state: AppState) => {
      if (state.proceduresList !== null) {
        if (state.proceduresList) {
          this.proceduresList = state.proceduresList;
          this.setDataSource();
          this.setSelectOptions();
        } else {
          this.errorState = true;
        }
        this.dataLoading = false;
      }
    });
    this.trackFromGroupChanges();
  }

  ngOnDestroy(): void {
    this.stateSubscription && this.stateSubscription.unsubscribe();
    this.breakpointSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
    this.errorState && this._store.dispatch(setProcedures({ procedures: null }));
  }

  onRowClickHandler = (procedureId: string): void => {
    this.stateSubscription.unsubscribe();
    const [id, region] = procedureId.split('|');
    const identifier: IProcedureIdentifier = {
      id,
      nameLevel2: region
    };
    this._store.dispatch(setSelectedProcedure({ procedureId: identifier }));
    this.router.navigate(['procedure-detail'], { relativeTo: this.route });
  };

  onClearInputHandler = (): void => {
    this.filtersForm.controls['search'].reset();
  };

  private setTableConfig = (): void => {
    this.tableData = {
      header: [
        { id: 'procedureCountry', name: 'procedures-page.main.table.member-state-column', type: ETableColumnType.flag },
        {
          id: 'jurisdiction',
          name: this.isMobile ? 'procedures-page.main.table.jurisdiction-column.mobile' : 'procedures-page.main.table.jurisdiction-column',
          type: ETableColumnType.string,
          allowFilter: true,
          tooltipText: ['common.tooltip-text.jurisdiciton-column(1)', 'common.tooltip-text.jurisdiciton-column(2)']
        },
        {
          id: 'title',
          name: 'procedures-page.main.table.english-name-column',
          type: ETableColumnType.string,
          allowFilter: true
        },
        {
          id: 'local',
          name: 'procedures-page.main.table.local-name-column',
          type: ETableColumnType.string,
          allowFilter: true
        },
        { id: 'event', name: 'procedures-page.main.table.life-event-column', type: ETableColumnType.event }
      ],
      body: []
    };
    this.tableConfig = {
      titleText: 'procedures-page.main.table-title',
      showTotalCount: true,
      showPaginator: true,
      emptyStateTitle: 'procedures-page.main.evidence-types.message-box.empty-state.title',
      columnWidths: [
        { columnId: 'procedureCountry', width: '10%' },
        { columnId: 'jurisdiction', width: '15%' },
        { columnId: 'title', width: '35%' },
        { columnId: 'local', width: '30%' },
        { columnId: 'event', width: '10%' }
      ],
      sortableColumns: ['procedureCountry', 'title'],
      clickableRows: true,
      columnSpacer: true
    };
  };

  private setDataSource = (): void => {
    // Each reference framework with one procedure code associated and several jurisdictions with just one country code
    const rows: ITableRow[] = [];
    this.proceduresList.map((procedure: IProcedure) => {
      const jurisdictionValues: string[] = procedure.detail.jurisdiction.map((jurisdiction: IJurisdictionCodes) => {
        if (jurisdiction.codeLevel2) {
          const regionName: string = jurisdiction.nameLevel2
            ? `${jurisdiction.nameLevel2} (Regional)`
            : `Regional (${jurisdiction.codeLevel2})`;
          return regionName;
        }
        return 'National';
      });
      const row: ITableCell[] = [
        {
          columnId: 'procedureCountry',
          value: ECountryCodeList[procedure.detail.jurisdiction[0].countryCode],
          flagCode: ECountryCodeList[procedure.detail.jurisdiction[0].countryCode]
        },
        // { columnId: 'title', value: [procedure.detail.title], valueId: [procedure.detail.id] },
        {
          columnId: 'local',
          value: procedure.detail.localTitle ? [...procedure.detail.localTitle] : [],
          emptyValue: 'common.empty-message.official-names'
        },
        { columnId: 'event', value: [procedure.lifeEventLabel], eventIcon: [procedure.lifeEventIcon] }
      ];
      jurisdictionValues.map((jurisdiction: string, index: number) => {
        rows.push([
          ...row,
          { columnId: 'jurisdiction', value: [jurisdiction] },
          {
            columnId: 'title',
            value: [procedure.detail.title],
            valueId: [
              `${procedure.detail.id}|${procedure.detail.jurisdiction[index].nameLevel2 ? procedure.detail.jurisdiction[index].nameLevel2 : procedure.detail.jurisdiction[index].codeLevel2}`
            ]
          }
        ]);
      });
    });
    this.tableData.body = [
      ...rows.sort((a: ITableRow, b: ITableRow) => {
        const valueA: string = a[0].value ? a[0].value.toString() : '';
        const valueB: string = b[0].value ? b[0].value.toString() : '';
        return sortItems(valueA, valueB, 'ascending');
      })
    ];
  };

  private setSelectOptions = (): void => {
    const countries: ISelectOption[] = [];
    this.proceduresList.map((procedure: IProcedure) => {
      if (
        procedure.detail.jurisdiction.length > 0 &&
        !this.countrySelectOptions
          .map((option: ISelectOption) => option.value)
          .includes(ECountryCodeList[procedure.detail.jurisdiction[0].countryCode])
      ) {
        const countryName: string = ECountryCodeList[procedure.detail.jurisdiction[0].countryCode];
        countries.push({
          value: countryName,
          label: `common.country-label.${countryName.toLowerCase()}`
        });
        this.countrySelectOptions = [
          ...countries.sort((a: ISelectOption, b: ISelectOption) => {
            const valueA: string = a.value;
            const valueB: string = b.value;
            return sortItems(valueA, valueB, 'ascending');
          })
        ];
      }
      if (!this.eventSelectOptions.map((option: ISelectOption) => option.value).includes(procedure.lifeEventLabel)) {
        this.eventSelectOptions.push({
          value: procedure.lifeEventLabel,
          label: `common.life-event.${procedure.lifeEventLabel.toLowerCase()}`
        });
      }
    });
  };

  private trackFromGroupChanges = (): void => {
    this.formSubscription = this.filtersForm.valueChanges.subscribe({
      next: (changes: IProceduresPageFormField) => {
        this.filteredValues = changes;
      }
    });
  };
}
