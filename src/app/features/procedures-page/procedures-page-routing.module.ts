import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProceduresPageComponent } from '@features/procedures-page/procedures-page.component';
import { ProceduresMainPageComponent } from '@features/procedures-page/components/procedures-main-page/procedures-main-page.component';
import { ProceduresDetailPageComponent } from '@features/procedures-page/components/procedures-detail-page/procedures-detail-page.component';

const routes: Routes = [
  {
    path: '',
    component: ProceduresPageComponent,
    children: [
      { path: '', component: ProceduresMainPageComponent },
      { path: 'procedure-detail', component: ProceduresDetailPageComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProceduresPageRoutingModule {}
