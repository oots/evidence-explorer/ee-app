import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import { EvidenceBrokerService } from 'src/app/core/services/evidence-broker.service';
import { IProcedure } from '@shared/models/procedure.model';
import { setProcedures } from 'src/app/core/reducers/procedures/procedures.actions';
import { AppState } from 'src/app/core/reducers';

@Component({
  templateUrl: './procedures-page.component.html',
  styleUrls: ['./procedures-page.component.scss']
})
export class ProceduresPageComponent implements OnInit, OnDestroy {
  private evidenceBrokerSubscription: Subscription;

  constructor(
    private _evidenceBrokerService: EvidenceBrokerService,
    // @ts-ignore
    private _store: Store<AppState>
  ) {
    this.evidenceBrokerSubscription = this._evidenceBrokerService.procedureList$.subscribe({
      next: (procedures: IProcedure[]) => {
        this._store.dispatch(setProcedures({ procedures }));
      },
      error: (error) => {
        console.error('error', error);
        this._store.dispatch(setProcedures({ procedures: undefined }));
      }
    });
  }

  ngOnInit(): void {
    this._store
      .select('procedures')
      .subscribe((state: AppState) => {
        if (!state.proceduresList) {
          this._evidenceBrokerService.getProcedureList();
        }
      })
      .unsubscribe();
  }

  ngOnDestroy(): void {
    this.evidenceBrokerSubscription.unsubscribe();
  }
}
