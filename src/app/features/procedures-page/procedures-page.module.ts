import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@shared/shared.module';
import { ProceduresPageComponent } from './procedures-page.component';
import { ProceduresMainPageComponent } from './components/procedures-main-page/procedures-main-page.component';
import { ProceduresDetailPageComponent } from './components/procedures-detail-page/procedures-detail-page.component';
import { ProceduresPageRoutingModule } from '@features/procedures-page/procedures-page-routing.module';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule, ProceduresPageRoutingModule],
  declarations: [ProceduresPageComponent, ProceduresMainPageComponent, ProceduresDetailPageComponent]
})
export class ProceduresPageModule {}
