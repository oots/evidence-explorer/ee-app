import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';

export interface IProceduresPageFormField {
  search?: string;
  countrySelect?: string[];
  eventSelect?: string[];
}

export interface IProceduresPageSelectFields {
  requirement?: string;
  country?: string;
}

export interface IProceduresPageEvidenceItem {
  evidenceId?: string;
  evidenceTitle?: string;
  evidenceDescription?: string;
  evidenceProviders?: IProvider[];
  requirementId?: string;
  requirementName?: string;
}

export interface IProceduresPageEvidenceCard {
  listId: string;
  evidencesDetails?: IProceduresPageEvidenceItem[];
}

export interface IProceduresPageRequirementItem {
  requirementId: string;
  requirementName?: string;
  requirementDescription?: string;
  requirementEvidenceCountries?: string[];
  expanded?: boolean;
}
