import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchResultsPageRoutingModule } from './search-results-page-routing.module';
import { SearchResultsPageComponent } from '@features/search-results-page/search-results-page.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [SearchResultsPageComponent],
  imports: [CommonModule, SharedModule, SearchResultsPageRoutingModule]
})
export class SearchResultsPageModule {}
