import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { SearchService } from 'src/app/core/services/search.service';
import { IResultsPageFormFields } from './models/search-results-page.model';
import { ISearchResult } from 'src/app/core/services/models/search.model';
import { ECountryCodeList, EntityType, IPaginationEvent, ISelectOption } from '@shared/models/common.model';
import { Store } from '@ngrx/store';
import { setSelectedRequirement } from 'src/app/core/reducers/requirements/requirements.actions';
import { setSelectedEvidence } from 'src/app/core/reducers/evidences/evidences.action';

@Component({
  selector: 'app-search-results-page',
  templateUrl: './search-results-page.component.html',
  styleUrls: ['./search-results-page.component.scss']
})
export class SearchResultsPageComponent implements OnInit, OnDestroy {
  searchInput: FormControl = new FormControl();
  filtersForm: FormGroup = new FormGroup({
    country: new FormControl(),
    procedure: new FormControl(),
    requirement: new FormControl(),
    evidence: new FormControl(),
    provider: new FormControl()
  });
  countrySelectOptions: ISelectOption[] = [];
  searchLoading = true;
  searchResults: ISearchResult[] = [];
  filteredResults: ISearchResult[] = [];
  filters: IResultsPageFormFields = {};
  searchText: string;
  paginatorState: IPaginationEvent = {};

  private searchSubscription: Subscription;

  constructor(
    private _searchService: SearchService,
    private _store: Store,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.searchSubscription = this._searchService.searchResults$.subscribe((results: ISearchResult[]) => {
      this.searchResults = [...results];
      this.setSelectOptions();
      this.setFilteredItems();
      this.searchLoading = false;
    });
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe((params: Params) => {
        if (params.search) {
          this.searchInput.setValue(params.search);
          this._searchService.search(params.search);
        } else {
          this.searchLoading = false;
        }
      })
      .unsubscribe();
    this.trackFormGroupChanges();
  }

  ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
  }

  search = (): void => {
    const searchText: string = this.searchInput.value;
    if (searchText) {
      this.searchLoading = true;
      this.resetPaginator();
      this._searchService.search(searchText);
    }
  };

  getElementType(type: EntityType): string {
    return `search-results-page.results-section.${type}-label`;
  }

  getCountryName(countryCode: string): string {
    return ECountryCodeList[countryCode];
  }

  getItemsInPage = (): ISearchResult[] => {
    return this.filteredResults.length > 10
      ? this.filteredResults.slice(this.paginatorState.showingFrom - 1, this.paginatorState.showingTo)
      : this.filteredResults;
  };

  pagination = (event): void => {
    this.paginatorState = event;
  };

  onClearInputHandlerHandler = (): void => {
    this.searchInput.reset();
  };

  onResultClickHandler = (type: EntityType, url: string, id?: string): void => {
    switch (type) {
      case 'requirement':
        this._store.dispatch(setSelectedRequirement({ requirementId: id }));
        break;
      case 'evidence':
        this._store.dispatch(setSelectedEvidence({ evidenceTypeId: id }));
        break;
      case 'procedure':
        // TODO: manage region issue in case we click on a procedure
        break;
      case 'provider':
        this._store.dispatch(setSelectedEvidence({ evidenceProviderId: id }));
        break;
    }
    this.router.navigate([url]);
  };

  private setSelectOptions = (): void => {
    this.countrySelectOptions = [];
    this.searchResults.map((result: ISearchResult) => {
      if (
        result.country &&
        this.countrySelectOptions.filter((option: ISelectOption) => option.value === ECountryCodeList[result.country]).length === 0
      ) {
        this.countrySelectOptions.push({
          value: ECountryCodeList[result.country],
          label: `common.country-label.${ECountryCodeList[result.country].toLowerCase()}`
        });
      }
    });
  };

  private setFilteredItems = (): void => {
    let filteredItems: ISearchResult[] = [...this.searchResults];
    const selectedCountries: ISelectOption[] = this.filters.country ? this.filters.country : [];
    const selectedTypes: string[] = Object.keys(this.filters).filter((key) => this.filters[key] === true);
    if (selectedCountries.length > 0) {
      filteredItems = filteredItems.filter((item: ISearchResult) => selectedCountries.includes(ECountryCodeList[item.country]));
    }
    if (selectedTypes.length > 0) {
      filteredItems = filteredItems.filter((item: ISearchResult) => selectedTypes.includes(item.type));
    }
    this.filteredResults = [...filteredItems];
  };

  private resetPaginator = (): void => {
    this.paginatorState = {
      page: 1,
      showingFrom: 1,
      showingTo: 10
    };
  };

  private trackFormGroupChanges = (): void => {
    this.filtersForm.valueChanges.subscribe((changes: IResultsPageFormFields) => {
      this.filters = changes;
      this.setFilteredItems();
    });
    this.searchInput.valueChanges.subscribe((search: string) => {
      this.searchText = search;
    });
  };
}
