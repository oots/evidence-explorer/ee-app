import { ISelectOption } from '@shared/models/common.model';

export interface IResultsPageFormFields {
  country?: ISelectOption[];
  procedure?: boolean;
  requirement?: boolean;
  evidence?: boolean;
  provider?: boolean;
}
