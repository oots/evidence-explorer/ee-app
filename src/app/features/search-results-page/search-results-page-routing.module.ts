import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchResultsPageComponent } from '@features/search-results-page/search-results-page.component';

const routes: Routes = [{ path: '', component: SearchResultsPageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchResultsPageRoutingModule {}
