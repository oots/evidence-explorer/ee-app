import { EntityType } from '@shared/models/common.model';

export interface IHomeSuggestionLink {
  label: string;
  type: EntityType;
  id?: string;
}
