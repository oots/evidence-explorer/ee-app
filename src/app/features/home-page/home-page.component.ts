import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';
import { IRequirement } from '@shared/models/requirement.model';
import { AppState } from 'src/app/core/reducers';
import { setSelectedEvidence } from 'src/app/core/reducers/evidences/evidences.action';
import { setSelectedRequirement } from 'src/app/core/reducers/requirements/requirements.actions';
import { DataEvidencesService } from 'src/app/core/services/data-evidences.service';
import { DataRequirementsService } from 'src/app/core/services/data-requirements.service';
import { ISubjectContent } from 'src/app/core/services/models/data-evidences.model';
import { SearchService } from 'src/app/core/services/search.service';
import { ISearchResult } from 'src/app/core/services/models/search.model';
import { IHomeSuggestionLink } from './models/home-page.model';
import { EntityType } from '@shared/models/common.model';
import { EvidenceBrokerService } from '../../core/services/evidence-broker.service';
import { IProcedure } from '@shared/models/procedure.model';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  searchInput: FormControl = new FormControl();
  requirementsCount: number;
  evidencesCount: number;
  providersCount: number;
  proceduresCount: number;
  searchText: string;
  searchResults: ISearchResult[] = [];
  popularSearchTerms: IHomeSuggestionLink[] = [
    { label: 'Proof of birth', type: 'requirement' },
    { label: 'Proof of enrolment in tertiary education institution', type: 'requirement' },
    { label: 'Proof of residence', type: 'requirement' }
  ];
  searchDetail = false;
  searchDetailType: EntityType;

  private requirementsSubscription: Subscription;
  private evidencesSubscription: Subscription;
  private providersSubscription: Subscription;
  private proceduresSubscription: Subscription;
  private formSubscription: Subscription;
  private searchSubscription: Subscription;

  constructor(
    private _searchService: SearchService,
    private _dataRequirementsService: DataRequirementsService,
    private _dataEvidencesService: DataEvidencesService,
    private _evidenceBrokerService: EvidenceBrokerService,
    private _store: Store<AppState>,
    private router: Router
  ) {
    this.requirementsSubscription = this._dataRequirementsService.requirementsList$.subscribe({
      next: (requirementsList: IRequirement[]) => {
        this.requirementsCount = requirementsList.length;
        this._searchService.loadData({ requirement: requirementsList });
      },
      error: (error) => {
        console.error('Service error: ', error);
      }
    });
    this.evidencesSubscription = this._dataEvidencesService.evidenceTypes$.subscribe({
      next: (response: ISubjectContent) => {
        this.evidencesCount = response.content.length;
        this._searchService.loadData({ evidence: response.content as IEvidenceType[] });
      },
      error: (error: ISubjectContent) => {
        console.error('Service error: ', error);
      }
    });
    this.providersSubscription = this._dataEvidencesService.providers$.subscribe({
      next: (response: ISubjectContent) => {
        this.providersCount = response.content.length;
        this._searchService.loadData({ provider: response.content as IProvider[] });
      },
      error: (error: ISubjectContent) => {
        console.error('Service error: ', error);
      }
    });
    this.proceduresSubscription = this._evidenceBrokerService.procedureList$.subscribe({
      next: (procedures: IProcedure[]) => {
        this.proceduresCount = procedures.length;
        this._searchService.loadData({ procedure: procedures as IProcedure[] });
      },
      error: (error: ISubjectContent) => {
        console.error('Service error: ', error);
      }
    });
    this.searchSubscription = this._searchService.searchResults$.subscribe((results: ISearchResult[]) => {
      if (this.searchDetail) {
        const entity: ISearchResult = results.find((result: ISearchResult) => result.type === this.searchDetailType);
        this.onLinkClickHandler(entity.type, entity.linkUrl, entity.id);
        this.searchDetail = false;
      } else {
        this.searchResults = results;
      }
    });
  }

  ngOnInit() {
    this.trackFormControlChanges();
    this._store
      .select('requirements')
      .subscribe((state: AppState) => {
        if (!state.requirementsList) {
          this._dataRequirementsService.getRequirementsList();
        } else {
          this.requirementsCount = state.requirementsList.length;
          this._searchService.loadData({ requirement: state.requirementsList });
        }
      })
      .unsubscribe();
    this._store
      .select('evidences')
      .subscribe((state: AppState) => {
        if (!state.evidencesList) {
          this._dataEvidencesService.getEvidenceTypesList('main');
        } else {
          this.evidencesCount = state.evidencesList.length;
          this._searchService.loadData({ evidence: state.evidencesList });
        }
        if (!state.providersList) {
          this._dataEvidencesService.getProvidersList('main');
        } else {
          this.providersCount = state.providersList.length;
          this._searchService.loadData({ provider: state.providersList });
        }
      })
      .unsubscribe();
    this._store
      .select('procedures')
      .subscribe((state: AppState) => {
        if (!state.proceduresList) {
          this._evidenceBrokerService.getProcedureList();
        } else {
          this.proceduresCount = state.proceduresList.length;
          this._searchService.loadData({ procedure: state.proceduresList });
        }
      })
      .unsubscribe();
  }

  ngOnDestroy(): void {
    this.requirementsSubscription.unsubscribe();
    this.evidencesSubscription.unsubscribe();
    this.providersSubscription.unsubscribe();
    this.proceduresSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
  }

  search = (): void => {
    this.router.navigate(['search-results'], { queryParams: { search: this.searchText } });
  };

  onClearInputHandler = (): void => {
    this.searchInput.reset();
  };

  onLinkClickHandler = (type: EntityType, url: string, id?: string): void => {
    switch (type) {
      case 'requirement':
        this._store.dispatch(setSelectedRequirement({ requirementId: id }));
        break;
      case 'evidence':
        this._store.dispatch(setSelectedEvidence({ evidenceTypeId: id }));
        break;
      case 'procedure':
        // TODO: manage region issue in case we click on a procedure
        break;
      case 'provider':
        this._store.dispatch(setSelectedEvidence({ evidenceProviderId: id }));
        break;
    }
    this.router.navigate([url]);
  };

  onSuggestionClickHandler = (type: EntityType, label: string): void => {
    this.searchDetail = true;
    this.searchDetailType = type;
    this._searchService.search(label);
  };

  getLinkAriaLabel(label: string, type: EntityType): string {
    if (type === 'procedure') {
      return 'navigate to procedures list page';
    } else {
      return `navigate to ${label} detail page`;
    }
  }

  private trackFormControlChanges = (): void => {
    this.formSubscription = this.searchInput.valueChanges.subscribe((search) => {
      this.searchText = search;
      this._searchService.search(search);
    });
  };
}
