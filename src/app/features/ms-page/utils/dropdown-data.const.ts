export const LIFE_EVENT_DATA: { id: number; name: string; identifier: string }[] = [
  {
    id: 1,
    name: 'Birth',
    identifier: 'R'
  },
  {
    id: 2,
    name: 'Residence',
    identifier: 'S'
  },
  {
    id: 3,
    name: 'Studying',
    identifier: 'T'
  },
  {
    id: 4,
    name: 'Working',
    identifier: 'U'
  },
  {
    id: 5,
    name: 'Moving',
    identifier: 'V'
  },
  {
    id: 6,
    name: 'Retiring',
    identifier: 'W'
  },
  {
    id: 7,
    name: 'Starting, running and closing a business',
    identifier: 'X'
  },
  {
    id: 8,
    name: 'No Life Event',
    identifier: '0'
  },
  {
    id: 9,
    name: 'Critical raw materials projects',
    identifier: 'AL'
  },
  {
    id: 10,
    name: 'Net-zero technology manufacturing projects',
    identifier: 'AK'
  }
].sort((a, b) => a.name.localeCompare(b.name));

export const DOMAIN_DATA: { id: number; name: string; description: string }[] = [
  {
    id: 1,
    name: 'Education',
    description:
      'The Education domain relates to the formal learning and academic achievements of individuals, such as secondary; post-secondary; higher education; vocational education; training. It usually refers to the European Qualification Framework (EQF), which is a learning outcomes-based framework for all types of qualifications. EQF levels for the requirements range 3-8.'
  },
  {
    id: 2,
    name: 'Population',
    description:
      "The Population domain relates to various aspects of individuals lives, including fundamental characteristics of a person. It usually refers to the public documents regulation, describing events as: birth; a person being alive; death; name; marriage, including capacity to marry and marital status; divorce, legal separation or marriage annulment; registered partnership, including capacity to enter into a registered partnership and registered partnership status; dissolution of a registered partnership, legal separation or annulment of a registered partnership; parenthood; adoption; domicile and/or residence; nationality; absence of a criminal record, provided that public documents concerning this fact are issued for a citizen of the Union by the authorities of that citizen's Member State of nationality."
  },
  {
    id: 3,
    name: 'Vehicles',
    description:
      'The Vehicles domain relates to roadworthiness tests of vehicles and vehicle registration issuance. It usually refers to aspects described in ANNEX II of DIRECTIVE 2014/45/EU and ANNEX I/II of COUNCIL DIRECTIVE 1999/37/EC.'
  },
  {
    id: 4,
    name: 'Professional Qualifications',
    description:
      'The Professional Qualifications domain relates to regulated professions. It usually refers to qualifications attested by evidence of formal qualifications, an attestation of competence referred to in Article 11 point (a)  and/or professional experience, as stated in DIRECTIVE 2005/36/EC.'
  },
  {
    id: 5,
    name: 'Social Security',
    description:
      "The Social Security domain relates to grants and benefits provided by social security systems. It usually refers to the following branches of social security, as described by REGULATION (EC) No 883/2004: sickness benefits; maternity and equivalent paternity benefits; invalidity benefits; old-age benefits (pensions); survivors' benefits; benefits in respect of accidents at work and occupational diseases; death grants; unemployment benefits; pre-retirement benefits; family benefits; special non-contributory cash benefits (guaranteed minimum subsistence)."
  },
  {
    id: 6,
    name: 'Income',
    description:
      'The Income domain relates to financial and tax information of physical persons. It includes income data, like: tax bill; cadastral income; living wage; alimony; study grants. An actual reference is still missing.'
  },
  {
    id: 7,
    name: 'Business',
    description:
      'The Business domain relates to legal persons only. It usually refers to events described in ANNEX II of REGULATION (EU) 2018/1724, like: notification of business activity; permission for exercising a business activity; changes of business activity; the termination of a business activity not involving insolvency or liquidation procedures.'
  }
].sort((a, b) => a.name.localeCompare(b.name));

export const SDG_PROCEDURES_DATA = [];
