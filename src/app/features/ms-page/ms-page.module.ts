import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { MsPageRoutingModule } from '@features/ms-page/ms-page-routing.module';
import { MsMainPageComponent } from './ms-main-page.component';

@NgModule({
  declarations: [MsMainPageComponent],
  imports: [CommonModule, SharedModule, MsPageRoutingModule]
})
export class MsPageModule {}
