import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, map, Observable } from 'rxjs';
import {
  DomainModel,
  ISelectOption,
  LifeEventModel,
  MsPageAuthorityModel,
  MsPageEvidenceModel,
  MsPageProcedureModel,
  MsPageRequirementModel,
  ResponseModel,
  TableOptionsModel
} from '@shared/models/common.model';
import {
  URL_GET_LIFE_EVENTS_LIST,
  URL_POST_AUTHORITIES_LIST,
  URL_POST_DOMAINS_LIST,
  URL_POST_EVIDENCES_LIST,
  URL_POST_PROCEDURES_LIST,
  URL_POST_REQUIREMENTS_LIST
} from '@features/ms-page/services/url.config';
import { FilterEnum } from '@shared/enums/common.enum';

@Injectable({
  providedIn: 'root'
})
export class MsService {
  protected errorSubject = new BehaviorSubject<boolean>(false);
  error$ = this.errorSubject.asObservable();

  protected proceduresSubject = new BehaviorSubject<ResponseModel<MsPageProcedureModel>>(null);
  procedures$ = this.proceduresSubject.asObservable();

  protected evidencesSubject = new BehaviorSubject<ResponseModel<MsPageEvidenceModel>>(null);
  evidences$ = this.evidencesSubject.asObservable();

  protected requirementsSubject = new BehaviorSubject<ResponseModel<MsPageRequirementModel>>(null);
  requirements$ = this.requirementsSubject.asObservable();

  protected authoritiesCountSubject = new BehaviorSubject<number>(0);
  authoritiesCount$ = this.authoritiesCountSubject.asObservable();

  constructor(private readonly http: HttpClient) {}

  defaultOptions = (countryCode?: string, noPagination = false) => {
    return {
      pageNumber: 0,
      pageSize: 0,
      filter: countryCode
        ? [
            { fieldName: FilterEnum.COUNTRY_CODE, fieldValues: [countryCode.toUpperCase()] }
            // {fieldName: FilterEnum.STATUS, fieldValues: ["2"]}
          ]
        : [],
      sort: [{ fieldName: FilterEnum.NAME, order: 'asc' }]
    } as TableOptionsModel;
  };

  fetchProcedureList(countryCode: string, payload?: TableOptionsModel): void {
    this.errorSubject.next(false);
    this.http.post<ResponseModel<MsPageProcedureModel>>(URL_POST_PROCEDURES_LIST, payload || this.defaultOptions(countryCode)).subscribe({
      next: (response) => {
        if (response) {
          this.proceduresSubject.next(response);
        } else {
          this.proceduresSubject.next({ list: [], count: 0, request: payload });
        }
      },
      error: () => {
        this.errorSubject.next(true);
      }
    });
  }

  fetchEvidencesList(countryCode: string, payload?: TableOptionsModel): void {
    this.errorSubject.next(false);
    this.http.post<ResponseModel<MsPageEvidenceModel>>(URL_POST_EVIDENCES_LIST, payload || this.defaultOptions(countryCode)).subscribe({
      next: (response) => {
        if (response) {
          this.evidencesSubject.next(response);
        } else {
          this.evidencesSubject.next({ list: [], count: 0, request: payload });
        }
      },
      error: () => {
        this.errorSubject.next(true);
      }
    });
  }

  fetchRequirementsList(countryCode: string, payload?: TableOptionsModel): void {
    this.errorSubject.next(false);
    this.http
      .post<ResponseModel<MsPageRequirementModel>>(URL_POST_REQUIREMENTS_LIST, payload || this.defaultOptions(countryCode))
      .subscribe({
        next: (response) => {
          if (response) {
            this.requirementsSubject.next(response);
          } else {
            this.requirementsSubject.next({ list: [], count: 0, request: payload });
          }
        },
        error: () => {
          this.errorSubject.next(true);
        }
      });
  }

  fetchAuthoritiesList(countryCode: string, payload?: TableOptionsModel): void {
    this.errorSubject.next(false);
    this.http.post<ResponseModel<MsPageAuthorityModel>>(URL_POST_AUTHORITIES_LIST, payload || this.defaultOptions(countryCode)).subscribe({
      next: (response) => {
        this.authoritiesCountSubject.next(response?.count || 0);
      },
      error: () => {
        this.errorSubject.next(true);
      }
    });
  }

  get procedurePayload(): TableOptionsModel {
    return this.proceduresSubject.value.request;
  }

  get requirementPayload(): TableOptionsModel {
    return this.requirementsSubject.value.request;
  }

  get evidencePayload(): TableOptionsModel {
    return this.evidencesSubject.value.request;
  }

  updateProcedures(payload: TableOptionsModel, countryCode: string, sort: boolean = false) {
    this.fetchProcedureList(countryCode, sort ? this.handleSorting(this.procedurePayload, payload) : payload);
  }

  updateRequirements(payload: TableOptionsModel, countryCode: string, sort: boolean = false) {
    this.fetchRequirementsList(countryCode, sort ? this.handleSorting(this.requirementPayload, payload) : payload);
  }

  updateEvidences(payload: TableOptionsModel, countryCode: string, sort: boolean = false) {
    this.fetchEvidencesList(countryCode, sort ? this.handleSorting(this.evidencePayload, payload) : payload);
  }

  fetchLifeEventsFilterOptionsList(): Observable<ISelectOption[]> {
    return this.http.get<LifeEventModel[]>(URL_GET_LIFE_EVENTS_LIST).pipe(
      map((response: LifeEventModel[]) =>
        response
          .map((event) => ({
            label: event.name.toString(),
            value: event.id.toString()
          }))
          .sort((a, b) => a.label.localeCompare(b.label))
      )
    );
  }

  fetchDomainFilterOptionsList(countryCode?: string): Observable<ISelectOption[]> {
    return this.http
      .post<ResponseModel<DomainModel>>(URL_POST_DOMAINS_LIST, this.defaultOptions(countryCode || null, true))
      .pipe(map((response) => response.list.map((event) => ({ label: event.name.toString(), value: event.id.toString() }))));
  }

  fetchProceduresOptionsList(countryCode: string): Observable<ISelectOption[]> {
    return this.http.post<ResponseModel<MsPageProcedureModel>>(URL_POST_PROCEDURES_LIST, this.defaultOptions(countryCode, true)).pipe(
      map((response) =>
        response?.list?.map((event) => ({
          label: `${event.parentProcedure.identifier.toString()} - ${event.parentProcedure.name.toString()}`,
          value: event.parentProcedure.id.toString()
        }))
      )
    );
  }

  private handleSorting(payloadState: TableOptionsModel, newPayload: TableOptionsModel): TableOptionsModel {
    const existingSorting = (payloadState.sort.length && payloadState?.sort[0]) || null;
    const newSorting = (newPayload.sort.length && newPayload?.sort[0]) || null;

    if (existingSorting?.fieldName === newSorting?.fieldName) {
      switch (existingSorting?.order) {
        case 'asc':
          return { ...newPayload, sort: [{ fieldName: existingSorting?.fieldName, order: 'desc' }] };
        case 'desc':
          return { ...newPayload, sort: [] };
        default:
          return { ...newPayload, sort: [{ fieldName: existingSorting?.fieldName, order: 'asc' }] };
      }
    }

    if (!existingSorting && newSorting) {
      return { ...newPayload, sort: [{ fieldName: newSorting?.fieldName, order: 'asc' }] };
    }

    return newPayload;
  }
}
