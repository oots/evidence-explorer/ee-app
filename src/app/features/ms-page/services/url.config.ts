import { environment } from '../../../../environments/environment';

const API: string = environment.apiUrl as string;
export const URL_POST_PROCEDURES_LIST = `${API}/ee/api/procedure/implementation/list`;
export const URL_POST_EVIDENCES_LIST = `${API}/ee/api/evidence-type/implementation/list`;
export const URL_POST_AUTHORITIES_LIST = `${API}/ee/api/provider/list`;
export const URL_POST_REQUIREMENTS_LIST = `${API}/ee/api/requirement/implementation/list`;
export const URL_GET_LIFE_EVENTS_LIST = `${API}/ee/api/procedure/life-events/list`;
export const URL_POST_DOMAINS_LIST = `${API}/ee/api/domain/list`;
