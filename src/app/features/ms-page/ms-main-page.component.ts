import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  CountryModel,
  IPaginationEvent,
  ISelectOption,
  LinkedEvidenceType,
  LinkedProvider,
  LinkedRequirementModel,
  MsPageEvidenceModel,
  MsPageProcedureModel,
  MsPageRequirementModel,
  ResponseModel,
  TableOptionsModel
} from '@shared/models/common.model';
import { COUNTRIES } from '@shared/utils/constants';
import { ETableColumnType, ILinkEmit, ITable, ITableConfig, ITableSortEvent } from '@shared/components/table/models/table.model';
import { MsService } from '@features/ms-page/services/ms.service';
import { filter, map, Observable, takeUntil, tap } from 'rxjs';
import { DataTypeEnum, FilterEnum, SortOrderEnum } from '@shared/enums/common.enum';
import { BaseComponent } from '../../core/components/base/base.component';
import { Store } from '@ngrx/store';
import { setSelectedProcedure } from '../../core/reducers/procedures/procedures.actions';
import { setSelectedEvidence } from '../../core/reducers/evidences/evidences.action';
import { setSelectedRequirement } from '../../core/reducers/requirements/requirements.actions';
import { AppState } from '../../core/reducers';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-ms-main-page',
  templateUrl: './ms-main-page.component.html',
  styleUrl: './ms-main-page.component.scss'
})
export class MsMainPageComponent extends BaseComponent implements OnInit {
  proceduresTableConfig: ITableConfig = {
    noDataTooltip: 'ms-page.tables.tooltip.procedures',
    showTotalCount: false,
    showPaginator: false,
    columnSpacer: true,
    clickableRows: false,
    isExpandable: true,
    emptyStateTitle: 'ms-page.noResults.title',
    emptyStateDescription: 'ms-page.noResults.description',
    sortableColumns: ['name', 'id', 'reqCount', 'lifeEventName'],
    expandableRowTitle: 'ms-page.tables.expandableRow.title.procedures',
    columnWidths: [
      { columnId: 'name', width: '40%' },
      { columnId: 'id', width: '20%' },
      { columnId: 'reqCount', width: '20%' },
      { columnId: 'lifeEventName', width: '20%' }
    ]
  };

  requirementsTableConfig: ITableConfig = {
    noDataTooltip: 'ms-page.tables.tooltip.requirements',
    showTotalCount: false,
    showPaginator: true,
    columnSpacer: true,
    clickableRows: false,
    isExpandable: true,
    emptyStateTitle: 'ms-page.noResults.title',
    emptyStateDescription: 'ms-page.noResults.description',
    sortableColumns: ['name', 'evtpCountForCountry', 'domain'],
    expandableRowTitle: 'ms-page.tables.expandableRow.title.requirements',
    columnWidths: [
      { columnId: 'name', width: '65%' },
      { columnId: 'evtpCountForCountry', width: '10%' },
      { columnId: 'domains', width: '25%' }
    ]
  };

  evidencesTableConfig: ITableConfig = {
    noDataTooltip: 'ms-page.tables.tooltip.evidences',
    showTotalCount: false,
    showPaginator: true,
    columnSpacer: true,
    clickableRows: false,
    isExpandable: true,
    emptyStateTitle: 'ms-page.noResults.title',
    emptyStateDescription: 'ms-page.noResults.description',
    sortableColumns: ['name', 'providerCount'],
    expandableRowTitle: 'ms-page.tables.expandableRow.title.evidences',
    columnWidths: [
      { columnId: 'name', width: '90%' },
      { columnId: 'providerCount', width: '10%' }
    ]
  };

  selectedCountry: CountryModel;
  DataTypeEnum = DataTypeEnum;
  FilterEnum = FilterEnum;
  selectedTab: string = '1';
  miniCardsData = [
    { type: DataTypeEnum.PROCEDURE, count: 0 },
    { type: DataTypeEnum.REQUIREMENT, count: 0 },
    { type: DataTypeEnum.EVIDENCE, count: 0 },
    { type: DataTypeEnum.AUTHORITY, count: 0 }
  ];
  domainControl = new FormControl<string>('');
  proceduresControl = new FormControl<string>('');
  lifeEventControl = new FormControl<string>('');
  proceduresDropdownOptions$: Observable<ISelectOption[]>;
  lifeEventsDropdownOptions$: Observable<ISelectOption[]>;
  domainsDropdownOptions$: Observable<ISelectOption[]>;
  forcePageSize = false;
  procedures$ = this.msService.procedures$.pipe(
    filter((procedures: ResponseModel<MsPageProcedureModel>) => !!procedures),
    tap((procedures: ResponseModel<MsPageProcedureModel>) => {
      this.setMiniCardData(DataTypeEnum.PROCEDURE, procedures?.count || 0);
    }),
    map((procedures: ResponseModel<MsPageProcedureModel>) => ({
      header: [
        {
          id: 'name',
          name: 'ms-page.tables.columns.procedures.name',
          type: ETableColumnType['link'],
          allowFilter: true
        },
        {
          id: 'id',
          name: 'ms-page.tables.columns.procedures.id',
          type: ETableColumnType['number']
        },
        {
          id: 'reqCount',
          name: 'ms-page.tables.columns.procedures.requirements',
          type: ETableColumnType.number
        },
        {
          id: 'lifeEventName',
          name: 'ms-page.tables.columns.procedures.lifeEvent',
          type: ETableColumnType['event']
        }
      ],
      body: procedures?.list?.length
        ? procedures?.list?.map((procedure: MsPageProcedureModel) => [
            {
              columnId: 'name',
              value: [procedure?.name],
              valueId: procedure.identifier // is the value that the linkClicked emits
            },
            {
              columnId: 'id',
              value: [procedure?.parentProcedure?.identifier]
            },
            {
              columnId: 'reqCount',
              value: procedure?.reqCount
            },
            {
              columnId: 'lifeEventName',
              value: [procedure?.lifeEventName?.toUpperCase()],
              eventIcon: [`${procedure?.lifeEventName?.toLowerCase()}.svg`]
            },
            {
              columnId: 'linkedDataList',
              value:
                procedure?.requirements
                  ?.filter((requirement) => !!requirement.title)
                  .map((requirement: LinkedRequirementModel) => `${requirement?.identifier}::${requirement?.title}`) || []
            }
          ])
        : [],
      totalItems: procedures.count
    }))
  );

  evidences$ = this.msService.evidences$.pipe(
    filter((evidences: ResponseModel<MsPageEvidenceModel>) => !!evidences),
    tap((evidences: ResponseModel<MsPageEvidenceModel>) => this.setMiniCardData(DataTypeEnum.EVIDENCE, evidences?.count || 0)),
    map(
      (evidences: ResponseModel<MsPageEvidenceModel>): ITable => ({
        header: [
          {
            id: 'name',
            name: 'ms-page.tables.columns.evidences.name',
            type: ETableColumnType['link'],
            allowFilter: true
          },
          {
            id: 'providerCount',
            name: 'ms-page.tables.columns.evidences.authorities',
            type: ETableColumnType['number']
          }
        ],
        body: evidences?.list?.length
          ? evidences?.list?.map((evidence: MsPageEvidenceModel) => [
              {
                columnId: 'name',
                value: [evidence?.name],
                valueId: evidence?.classificationUrl?.toString() // is the value that the linkClicked emits
              },
              {
                columnId: 'providerCount',
                value: [evidence.providerCount]
              },
              {
                columnId: 'linkedDataList',
                value:
                  evidence?.providers
                    ?.filter((provider) => !!provider.name)
                    .map((provider: LinkedProvider) => `${provider?.identifier}::${provider?.name}`) || []
              }
            ])
          : [],
        totalItems: evidences.count
      })
    )
  );

  requirements$ = this.msService.requirements$.pipe(
    filter((requirements: ResponseModel<MsPageRequirementModel>) => !!requirements),
    tap((requirements: ResponseModel<MsPageRequirementModel>) => this.setMiniCardData(DataTypeEnum.REQUIREMENT, requirements?.count || 0)),
    map(
      (requirements: ResponseModel<MsPageRequirementModel>): ITable => ({
        header: [
          {
            id: 'name',
            name: 'ms-page.tables.columns.requirements.name',
            type: ETableColumnType['link'],
            allowFilter: true
          },
          {
            id: 'evtpCountForCountry',
            name: 'ms-page.tables.columns.requirements.evidences',
            type: ETableColumnType['number']
          },
          {
            id: 'domains',
            name: 'ms-page.tables.columns.requirements.domain',
            type: ETableColumnType['event']
          }
        ],
        body: requirements?.list?.length
          ? requirements?.list?.map((requirement: MsPageRequirementModel) => [
              {
                columnId: 'name',
                value: [requirement?.name],
                valueId: requirement.identifier // is the value that the linkClicked emits
              },
              {
                columnId: 'evtpCountForCountry',
                value: [requirement?.evtpCountForCountry]
              },
              {
                columnId: 'domains',
                value: requirement?.domains.map((domain) => domain.name)
              },
              {
                columnId: 'linkedDataList',
                value:
                  requirement?.evidenceTypes
                    ?.filter((evidenceType) => !!evidenceType.name)
                    .map((evidenceType: LinkedEvidenceType) => `${evidenceType?.identifier}::${evidenceType?.name}`) || []
              }
            ])
          : [],
        totalItems: requirements.count
      })
    )
  );

  constructor(
    private readonly msService: MsService,
    private readonly route: ActivatedRoute,
    private _store: Store<AppState>,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {
    this.subscribeToCountryURLChanges();
    this.subscribeToAuthoritiesChanges();
    this.setDropdownData();
  }

  onTabSelect(event) {
    if (this.selectedTab === event?.tab?.id) return;
    this.resetControls();
    this.selectedTab = event?.tab?.id;
    const noPaginationPayload: TableOptionsModel = {
      filter: [
        {
          fieldName: FilterEnum.COUNTRY_CODE,
          fieldValues: [this.selectedCountry.code.toUpperCase()]
        }
        // {
        //   fieldName: FilterEnum.STATUS,
        //   fieldValues: ["2"]
        // }
      ],
      sort: [
        {
          fieldName: FilterEnum.NAME,
          order: 'asc'
        }
      ],
      pageNumber: 0,
      pageSize: 0
    };
    switch (event?.tab?.id) {
      case '1':
        this.msService.fetchProcedureList(this.selectedCountry.code);
        this.msService.fetchEvidencesList(this.selectedCountry.code);
        this.msService.fetchRequirementsList(this.selectedCountry.code);
        this.msService.fetchAuthoritiesList(this.selectedCountry.code);
        this.forcePageSize = false;
        break;
      case '2':
        this.msService.fetchProcedureList(this.selectedCountry.code, noPaginationPayload);
        this.forcePageSize = true;
        break;
      case '3':
        this.msService.fetchRequirementsList(this.selectedCountry.code, noPaginationPayload);
        this.forcePageSize = true;
        break;
      case '4':
        this.msService.fetchEvidencesList(this.selectedCountry.code, noPaginationPayload);
        this.forcePageSize = true;
        break;
    }
  }

  paginationChanged(event: IPaginationEvent, tableType: DataTypeEnum) {
    //todo
    // switch (tableType) {
    //   case DataTypeEnum.AUTHORITY:
    //     break;
    //   case DataTypeEnum.REQUIREMENT:
    //     const paylaod = {
    //       ...this.msService.requirementPayload,
    //       pageNumber: event?.page,
    //       pageSize: event?.pageSize
    //     };
    //     this.msService.updateRequirements(paylaod, this.selectedCountry.code);
    //     break;
    //   case DataTypeEnum.PROCEDURE:
    //     break;
    // }
  }

  sortChanged(event: ITableSortEvent, tableType: DataTypeEnum) {
    const sort = event.sortOrder
      ? [
          {
            fieldName: event.columnId,
            order: SortOrderEnum[event.sortOrder]
          }
        ]
      : [];

    switch (tableType) {
      case DataTypeEnum.REQUIREMENT:
        this.msService.updateRequirements(
          {
            ...this.msService.requirementPayload,
            sort
          },
          this.selectedCountry.code,
          true
        );
        break;
      case DataTypeEnum.EVIDENCE:
        this.msService.updateEvidences(
          {
            ...this.msService.evidencePayload,
            sort
          },
          this.selectedCountry.code,
          true
        );
        break;
      case DataTypeEnum.PROCEDURE:
        this.msService.updateProcedures(
          {
            ...this.msService.procedurePayload,
            sort
          },
          this.selectedCountry.code,
          true
        );
        break;
    }
  }

  filterChange(event: string[], filter: FilterEnum) {
    if (filter === FilterEnum.DOMAIN) {
      const requirementsFilters = this.msService.requirementPayload?.filter?.filter((f) => f.fieldName !== filter) || [];
      if (event?.length) {
        requirementsFilters.push({ fieldName: filter, fieldValues: event });
      }
      this.msService.updateRequirements(
        {
          ...this.msService.requirementPayload,
          filter: [...requirementsFilters]
        },
        this.selectedCountry.code
      );
    } else {
      const proceduresFilters = this.msService.procedurePayload.filter.filter((f) => f.fieldName !== filter) || [];
      if (event?.length) {
        proceduresFilters.push({ fieldName: filter, fieldValues: event });
      }

      this.msService.updateProcedures(
        {
          ...this.msService.procedurePayload,
          filter: [...proceduresFilters]
        },
        this.selectedCountry.code
      );
    }
  }

  redirectToDetailsPage(event: string | ILinkEmit, type: DataTypeEnum) {
    const id = typeof event === 'string' ? event : (event as ILinkEmit).valueId;

    switch (type) {
      case DataTypeEnum.REQUIREMENT:
        this._store.dispatch(setSelectedRequirement({ requirementId: id }));
        this.router.navigate(['requirements', 'requirement-detail']);
        break;
      case DataTypeEnum.PROCEDURE:
        this._store.dispatch(setSelectedProcedure({ procedureId: { id } }));
        this.router.navigate(['procedures', 'procedure-detail']);
        break;
      case DataTypeEnum.EVIDENCE:
        this._store.dispatch(setSelectedEvidence({ evidenceTypeId: id }));
        this.router.navigate(['evidences', 'evidence-type-detail']);
        break;
      case DataTypeEnum.AUTHORITY:
        this._store.dispatch(setSelectedEvidence({ evidenceProviderId: id }));
        this.router.navigate(['evidences', 'evidence-provider-detail']);
        break;
    }
  }

  private subscribeToCountryURLChanges() {
    this.route.paramMap.subscribe({
      next: (params) => {
        const countryCode = params.get('country');
        this.selectedCountry = {
          code: countryCode,
          name: COUNTRIES.find((c) => c.code.toLowerCase() === countryCode).name
        };
        this.msService.fetchProcedureList(countryCode);
        this.msService.fetchEvidencesList(countryCode);
        this.msService.fetchRequirementsList(countryCode);
        this.msService.fetchAuthoritiesList(countryCode);
        this.proceduresDropdownOptions$ = this.msService.fetchProceduresOptionsList(countryCode);
        this.resetOnCountryChange();
      }
    });
  }

  private setMiniCardData(type: DataTypeEnum, count: number) {
    this.miniCardsData = [...this.miniCardsData.filter((card) => card.type !== type)];
    this.miniCardsData.push({ type, count });
    this.miniCardsData.sort((a, b) => a.type - b.type);
  }

  private subscribeToAuthoritiesChanges() {
    this.msService.authoritiesCount$
      .pipe(takeUntil(this.destroyed))
      .subscribe((authoritiesCount) => this.setMiniCardData(DataTypeEnum.AUTHORITY, authoritiesCount));
  }

  private setDropdownData() {
    this.domainsDropdownOptions$ = this.msService.fetchDomainFilterOptionsList();
    this.lifeEventsDropdownOptions$ = this.msService.fetchLifeEventsFilterOptionsList();
  }

  private resetOnCountryChange() {
    this.resetControls();
    this.selectedTab = '1';
  }

  private resetControls() {
    this.domainControl.setValue('');
    this.proceduresControl.setValue('');
    this.lifeEventControl.setValue('');
  }
}
