import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MsMainPageComponent } from '@features/ms-page/ms-main-page.component';

const routes: Routes = [
  {
    path: '',
    component: MsMainPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MsPageRoutingModule {}
