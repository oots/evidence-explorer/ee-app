import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsMainPageComponent } from './ms-main-page.component';

describe('MsMainPageComponent', () => {
  let component: MsMainPageComponent;
  let fixture: ComponentFixture<MsMainPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MsMainPageComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(MsMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
