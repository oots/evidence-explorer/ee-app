import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequirementsPageComponent } from '@features/requirements-page/requirements-page.component';
import { RequirementsMainPageComponent } from '@features/requirements-page/components/requirements-main-page/requirements-main-page.component';
import { RequirementsDetailPageComponent } from '@features/requirements-page/components/requirements-detail-page/requirements-detail-page.component';

const routes: Routes = [
  {
    path: '',
    component: RequirementsPageComponent,
    children: [
      { path: '', component: RequirementsMainPageComponent },
      { path: 'requirement-detail', component: RequirementsDetailPageComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequirementsPageRoutingModule {}
