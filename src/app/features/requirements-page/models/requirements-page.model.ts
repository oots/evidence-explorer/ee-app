import { ITable } from '@shared/components/table/models/table.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';

export interface IRequirementsPageListItem {
  countryCode: string;
  countryName: string;
  evidenceTypeList: IEvidenceType[];
  expanded: boolean;
  loading?: boolean;
  error?: boolean;
  tableContent?: ITable;
}

export interface IRequirementsPageFormField {
  search?: string;
  select?: string[];
}
