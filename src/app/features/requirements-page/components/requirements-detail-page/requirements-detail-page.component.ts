import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Router } from '@angular/router';

import { IRequirement } from '@shared/models/requirement.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IMessageBoxData } from '@shared/components/message/models/message-box.model';
import { ECountryCodeList, IJurisdictionCodes, ISelectOption } from '@shared/models/common.model';
import { ETableColumnType, ITable, ITableConfig, ITableRow } from '@shared/components/table/models/table.model';
import { AppState } from 'src/app/core/reducers';
import { IRequirementsPageFormField } from '../../models/requirements-page.model';
import { setSelectedEvidence } from 'src/app/core/reducers/evidences/evidences.action';
import { DataEvidencesService } from 'src/app/core/services/data-evidences.service';
import { ISubjectContent } from 'src/app/core/services/models/data-evidences.model';
import { IProcedure } from '@shared/models/procedure.model';
import { setSelectedProcedure } from 'src/app/core/reducers/procedures/procedures.actions';
import { setBreadcrumbLabelDetail } from 'src/app/core/reducers/breadcrumb/breadcrumb.actions';
import { EMainPageSectionId } from '../../../../core/components/layout/breadcrumb/models/breadcrumb.model';
import { setSelectedRequirement } from '../../../../core/reducers/requirements/requirements.actions';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-requirements-detail-page',
  templateUrl: './requirements-detail-page.component.html',
  styleUrls: ['./requirements-detail-page.component.scss']
})
export class RequirementsDetailPageComponent implements OnInit, OnDestroy {
  filtersForm: FormGroup = new FormGroup({
    search: new FormControl(),
    select: new FormControl()
  });
  filteredValues: IRequirementsPageFormField = {
    search: null,
    select: null
  };
  isMobile: boolean;
  requirementDetail: IRequirement = null;
  pageTitle = 'requirements-page.detail.title.default';
  multiselectOptions: ISelectOption[] = [];
  evidencesTableConfig: ITableConfig;
  evidencesTableDataSource: ITable = { header: [], body: [] };
  evidencesTotalCount = 0;
  evidencesDataLoading = true;
  proceduresTableConfig: ITableConfig;
  proceduresTableDataSource: ITable = { header: [], body: [] };
  proceduresTotalCount = 0;
  proceduresDataLoading = true;
  globalErrorState = false;
  messageBoxData: IMessageBoxData = {
    title: 'common.message-box.service-error.title',
    description: 'common.message-box.service-error.description',
    state: 'error'
  };

  private stateSubscription: Subscription;
  private formSubscription: Subscription;
  private breakpointSubscription: Subscription;
  private dataEvidencesServiceSubs: Subscription;

  constructor(
    // @ts-ignore
    private _store: Store<AppState>,
    private sharedService: SharedService,
    private _breakpointObserver: BreakpointObserver,
    private _dataEvidencesService: DataEvidencesService,
    private router: Router
  ) {
    this.breakpointSubscription = this._breakpointObserver.observe(['(min-width: 996px)']).subscribe((state: BreakpointState) => {
      const size = state.breakpoints['(min-width: 996px)'] ? 'desktop' : 'mobile';
      this.isMobile = size === 'mobile';
      this.setTableConfig();
    });
    this.dataEvidencesServiceSubs = this._dataEvidencesService.evidenceTypes$.subscribe({
      next: (response: ISubjectContent) => {
        const evidenceTypesData: IEvidenceType[] = response.content as IEvidenceType[];
        this.setEvidencesDataSource(evidenceTypesData);
        this.initializer();
      },
      error: (error: ISubjectContent) => {
        console.error('Service error: ', error);
        this.setEvidencesDataSource();
        this.initializer();
      }
    });
  }

  ngOnInit(): void {
    this.sharedService.scrollToTop();
    const selectedRequirementId = sessionStorage.getItem('selectedRequirementId');
    if (selectedRequirementId) {
      this._store.dispatch(setSelectedRequirement({ requirementId: selectedRequirementId }));
    }

    this.stateSubscription = this._store.select('requirements').subscribe((state: AppState) => {
      if (state.selectedRequirement && state.requirementsList) {
        const requirementSelected: IRequirement = state.requirementsList.find(
          (requirement: IRequirement) => requirement.id === state.selectedRequirement
        );
        if (requirementSelected) {
          this.requirementDetail = { ...requirementSelected };
          this._store.dispatch(
            setBreadcrumbLabelDetail({
              detailLabel: {
                id: EMainPageSectionId['requirement-detail'],
                label: this.requirementDetail.name
              }
            })
          );
          this._dataEvidencesService.getEvidenceTypesList('detail');
          this.setProceduresDataSource();
        } else {
          this.requirementDetail = undefined;
        }
      }
      if (
        (!state.selectedRequirement || this.requirementDetail === undefined || state.requirementsList === undefined) &&
        !this.globalErrorState
      ) {
        if (!state.selectedRequirement) {
          this.messageBoxData.title = 'requirements-page.detail.message-box.no-requirement.title';
          this.messageBoxData.description = 'requirements-page.detail.message-box.no-requirement.description';
        }
        this.globalErrorState = true;
      }
    });
    this.trackFromGroupChanges();
  }

  ngOnDestroy(): void {
    sessionStorage.removeItem('selectedRequirementId');
    this.stateSubscription && this.stateSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
    this.breakpointSubscription.unsubscribe();
    this.dataEvidencesServiceSubs.unsubscribe();
  }

  onClearInputHandler = (): void => {
    this.filtersForm.controls['search'].reset();
  };

  onRowClickHandler = (id: string, table: 'evidence' | 'procedure'): void => {
    this.stateSubscription.unsubscribe();
    if (table === 'evidence') {
      this._store.dispatch(setSelectedEvidence({ evidenceTypeId: id }));
      this.router.navigate(['evidences', 'evidence-type-detail']);
    } else {
      this._store.dispatch(setSelectedProcedure({ procedureId: { id } }));
      this.router.navigate(['procedures', 'procedure-detail']);
    }
  };

  updateTotalItems = (totalItems: number, table: 'evidence' | 'procedure'): void => {
    if (table === 'evidence') {
      this.evidencesTotalCount = totalItems;
    } else {
      this.proceduresTotalCount = totalItems;
    }
  };

  private initializer = (): void => {
    this.pageTitle = this.requirementDetail.name;
    this.setSelectOptions();
  };

  private setSelectOptions = (): void => {
    this.requirementDetail.evidenceTypeList.map((evidenceType: IEvidenceType) => {
      const countryName: string = ECountryCodeList[evidenceType.jurisdiction[0].countryCode];
      if (this.multiselectOptions.filter((option: ISelectOption) => option.value === countryName).length === 0) {
        this.multiselectOptions.push({
          value: countryName,
          label: `common.country-label.${countryName.toLowerCase()}`
        });
      }
    });
    this.requirementDetail.proceduresList.map((procedure: IProcedure) => {
      const countryName: string = ECountryCodeList[procedure.detail.jurisdiction[0].countryCode];
      if (this.multiselectOptions.filter((option: ISelectOption) => option.value === countryName).length === 0) {
        this.multiselectOptions.push({
          value: countryName,
          label: `common.country-label.${countryName.toLowerCase()}`
        });
      }
    });
  };

  private getJurisdictionLevel = (jurisdictionCode: IJurisdictionCodes): string => {
    if (jurisdictionCode.codeLevel3) return jurisdictionCode.codeLevel3;
    if (jurisdictionCode.codeLevel2) return jurisdictionCode.codeLevel2;
    return 'National';
  };

  private setTableConfig = (): void => {
    this.evidencesTableDataSource.header = [
      { id: 'country', name: 'requirements-page.detail.evidences-table.country-column', type: ETableColumnType.flag },
      {
        id: 'jurisdiction',
        name: this.isMobile
          ? 'requirements-page.detail.evidences-table.jurisdiction-column.mobile'
          : 'requirements-page.detail.evidences-table.jurisdiction-column',
        type: ETableColumnType.string,
        tooltipText: [
          'common.tooltip-text.jurisdiciton-column(1)',
          'common.tooltip-text.jurisdiciton-column(2)',
          'common.tooltip-text.jurisdiciton-column(3)'
        ]
      },
      {
        id: 'title',
        name: 'requirements-page.detail.evidences-table.english-name-column',
        type: ETableColumnType.string,
        allowFilter: true
      },
      {
        id: 'local',
        name: 'requirements-page.detail.evidences-table.local-name-column',
        type: ETableColumnType.string,
        allowFilter: true
      },
      { id: 'provider', name: 'requirements-page.detail.evidences-table.providers-column', type: ETableColumnType.number }
    ];
    this.proceduresTableDataSource.header = [
      { id: 'country', name: 'requirements-page.detail.procedures-table.country-column', type: ETableColumnType.flag },
      {
        id: 'jurisdiction',
        name: this.isMobile
          ? 'requirements-page.detail.procedures-table.jurisdiction-column.mobile'
          : 'requirements-page.detail.procedures-table.jurisdiction-column',
        type: ETableColumnType.string,
        tooltipText: [
          'common.tooltip-text.jurisdiciton-column(1)',
          'common.tooltip-text.jurisdiciton-column(2)',
          'common.tooltip-text.jurisdiciton-column(3)'
        ]
      },
      {
        id: 'title',
        name: 'requirements-page.detail.procedures-table.english-name-column',
        type: ETableColumnType.string,
        allowFilter: true
      },
      {
        id: 'local',
        name: 'requirements-page.detail.procedures-table.local-name-column',
        type: ETableColumnType.string,
        allowFilter: true
      },
      { id: 'event', name: 'requirements-page.detail.procedures-table.event-column', type: ETableColumnType.event }
    ];
    this.evidencesTableConfig = {
      captionText: 'list of evidence types',
      columnWidths: [
        { columnId: 'country', width: '20%' },
        { columnId: 'title', width: '20%' },
        { columnId: 'local', width: '20%' },
        { columnId: 'jurisdiction', width: '20%' },
        { columnId: 'provider', width: '20%' }
      ],
      sortableColumns: ['country', 'title', 'local', 'provider'],
      clickableRows: true,
      columnSpacer: true,
      emptyStateTitle: 'requirements-page.detail.evidences-table.empty-state.title',
      emptyStateDescription: 'requirements-page.detail.evidences-table.empty-state.description',
      showPaginator: true
    };
    this.proceduresTableConfig = {
      captionText: 'list of procedures',
      columnWidths: [
        { columnId: 'country', width: '20%' },
        { columnId: 'title', width: '20%' },
        { columnId: 'local', width: '20%' },
        { columnId: 'jurisdiction', width: '20%' },
        { columnId: 'event', width: '20%' }
      ],
      sortableColumns: ['country', 'title', 'local'],
      clickableRows: true,
      columnSpacer: true,
      emptyStateTitle: 'requirements-page.detail.procedures-table.empty-state.title',
      emptyStateDescription: 'requirements-page.detail.procedures-table.empty-state.description',
      showPaginator: true
    };
  };

  private setEvidencesDataSource = (evidenceTypesData?: IEvidenceType[]): void => {
    const rows: ITableRow[] = [];
    this.requirementDetail.evidenceTypeList.map((evidenceType: IEvidenceType) => {
      const countryName: string = ECountryCodeList[evidenceType.jurisdiction[0].countryCode];
      const providerCount: number = evidenceTypesData?.find(
        (evType: IEvidenceType) => evType.classification === evidenceType.classification
      )?.providersCount;
      const row: ITableRow = [
        { columnId: 'country', value: countryName, flagCode: countryName },
        { columnId: 'title', value: [evidenceType.title], valueId: [evidenceType.classification] },
        { columnId: 'local', value: evidenceType.localTitle, emptyValue: 'common.empty-message.official-names' },
        { columnId: 'provider', value: providerCount ? providerCount : '-' }
      ];
      evidenceType.jurisdiction.map((jurisdiction: IJurisdictionCodes) => {
        row.push({ columnId: 'jurisdiction', value: [this.getJurisdictionLevel(jurisdiction)] });
        rows.push(row);
      });
    });
    this.evidencesTableDataSource.body = [...rows];
    this.evidencesTotalCount = rows.length;
    this.evidencesDataLoading = false;
  };

  private setProceduresDataSource = (): void => {
    const rows: ITableRow[] = [];
    this.requirementDetail.proceduresList.map((procedure: IProcedure) => {
      const countryName: string = ECountryCodeList[procedure.detail.jurisdiction[0].countryCode];
      const row: ITableRow = [
        { columnId: 'country', value: countryName, flagCode: countryName },
        { columnId: 'title', value: [procedure.detail.title], valueId: [procedure.detail.id] },
        { columnId: 'local', value: procedure.detail.localTitle, emptyValue: 'common.empty-message.official-names' },
        { columnId: 'event', value: [procedure.lifeEventLabel], eventIcon: [procedure.lifeEventIcon] }
      ];
      procedure.detail.jurisdiction.map((jurisdiction: IJurisdictionCodes) => {
        row.push({ columnId: 'jurisdiction', value: [this.getJurisdictionLevel(jurisdiction)] });
        rows.push(row);
      });
    });
    this.proceduresTableDataSource.body = [...rows];
    this.proceduresTotalCount = rows.length;
    this.proceduresDataLoading = false;
  };

  private trackFromGroupChanges = (): void => {
    this.formSubscription = this.filtersForm.valueChanges.subscribe((changes: IRequirementsPageFormField) => {
      this.filteredValues = changes;
    });
  };
}
