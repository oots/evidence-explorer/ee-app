import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

import { IRequirement, IRequirementDomain } from '@shared/models/requirement.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IMessageBoxData } from '@shared/components/message/models/message-box.model';
import { ETableColumnType, ITable, ITableCell, ITableConfig, ITableRow } from '@shared/components/table/models/table.model';
import { setRequirements, setSelectedRequirement } from 'src/app/core/reducers/requirements/requirements.actions';
import { IJurisdictionCodes, ISelectOption } from '@shared/models/common.model';
import { IRequirementsPageFormField } from '../../models/requirements-page.model';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-requirements-main-page',
  templateUrl: './requirements-main-page.component.html',
  styleUrls: ['./requirements-main-page.component.scss']
})
export class RequirementsMainPageComponent implements OnInit, OnDestroy {
  filtersForm: FormGroup = new FormGroup({
    search: new FormControl(),
    select: new FormControl()
  });
  filteredValues: IRequirementsPageFormField = {
    search: null,
    select: null
  };
  multiselectOptions: ISelectOption[] = [];
  tableDataSource: ITable = { header: [], body: [] };
  tableConfig: ITableConfig = {};
  requirements: IRequirement[];
  dataLoading = true;
  errorState = false;
  isMobile: boolean;
  errorMessage: IMessageBoxData = {
    title: 'common.message-box.service-error.title',
    description: 'common.message-box.service-error.description',
    state: 'error'
  };

  private stateSubscription: Subscription;
  private formSubscription: Subscription;
  private breakpointSubscription: Subscription;

  constructor(
    // @ts-ignore
    private _store: Store<AppState>,
    private _breakpointObserver: BreakpointObserver,
    private sharedService: SharedService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.breakpointSubscription = this._breakpointObserver.observe(['(min-width: 996px)']).subscribe((state: BreakpointState) => {
      const size = state.breakpoints['(min-width: 996px)'] ? 'desktop' : 'mobile';
      this.isMobile = size === 'mobile';
      this.setTableConfig();
    });
  }

  ngOnInit(): void {
    this.sharedService.scrollToTop();
    this.stateSubscription = this._store.select('requirements').subscribe((state) => {
      if (state.requirementsList !== null) {
        if (state.requirementsList) {
          this.requirements = [...state.requirementsList];
          this.setSelectOptions();
          this.setDataSource();
        } else {
          this.errorState = true;
        }
        this.dataLoading = false;
      }
    });
    this.trackFormControlChanges();
  }

  ngOnDestroy(): void {
    this.stateSubscription && this.stateSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
    this.breakpointSubscription.unsubscribe();
    this.errorState && this._store.dispatch(setRequirements({ requirements: null }));
  }

  onRowClickHandler = (requirementId: string): void => {
    this.stateSubscription.unsubscribe();
    this._store.dispatch(setSelectedRequirement({ requirementId }));
    this.router.navigate(['requirement-detail'], { relativeTo: this.route });
  };

  onClearInputHandler = (): void => {
    this.filtersForm.controls['search'].reset();
  };

  private setTableConfig = (): void => {
    this.tableDataSource.header = [
      { id: 'name', name: 'requirements-page.main.table.title-column', type: ETableColumnType.string, allowFilter: true },
      { id: 'domain', name: 'requirements-page.main.table.domain-column-name', type: ETableColumnType.event },
      { id: 'procedure', name: 'requirements-page.main.table.procedures-column', type: ETableColumnType.number },
      {
        id: 'evidenceType',
        name: this.isMobile ? 'requirements-page.main.table.evidences-column.mobile' : 'requirements-page.main.table.evidences-column',
        type: ETableColumnType.progress,
        tooltipText: ['common.tooltip-text.evidences-column']
      }
    ];
    this.tableConfig = {
      titleText: 'requirements-page.main.table.title',
      captionText: 'list of requirements',
      showTotalCount: true,
      showPaginator: true,
      columnWidths: [
        { columnId: 'name', width: '50%' },
        { columnId: 'procedure', width: '15%' },
        { columnId: 'evidenceType', width: '20%' },
        { columnId: 'domain', width: '15%' }
      ],
      sortableColumns: ['name', 'procedure', 'evidenceType', 'domain'],
      columnSpacer: true,
      emptyStateTitle: 'requirements-page.main.message-box.empty-state.title',
      clickableRows: true
    };
  };

  private setDataSource = (): void => {
    const body: ITableRow[] = [];
    this.requirements.map((requirement: IRequirement) => {
      const items: ITableCell[] = [];
      items.push(
        { columnId: 'name', value: [requirement.name], valueId: [requirement.id] },
        { columnId: 'procedure', value: requirement.proceduresList?.length },
        { columnId: 'evidenceType', value: this.getListCountriesEvidenceType(requirement.evidenceTypeList) },
        { columnId: 'domain', value: requirement.domains.map((domain: IRequirementDomain) => domain.name) }
      );
      body.push(items);
    });
    this.tableDataSource.body = [...body];
  };

  private getListCountriesEvidenceType(evidenceTypes: IEvidenceType[]): number {
    let countryList: string[] = [];
    evidenceTypes.map((evidence: IEvidenceType) => {
      countryList = [...countryList.concat(evidence.jurisdiction.map((jurisdiction: IJurisdictionCodes) => jurisdiction.countryCode))];
    });
    return [...new Set(countryList)].length;
  }

  private setSelectOptions = (): void => {
    const options: ISelectOption[] = [];
    this.requirements.map((requirement: IRequirement) => {
      requirement.domains.map((domain: IRequirementDomain) => {
        if (options.filter((option: ISelectOption) => option.value === domain.name).length === 0) {
          options.push({
            value: domain.name,
            label: `common.domain.${domain.name.toLowerCase()}`
          });
        }
      });
    });
    this.multiselectOptions = [...options];
  };

  private trackFormControlChanges = (): void => {
    this.formSubscription = this.filtersForm.valueChanges.subscribe((changes: IRequirementsPageFormField) => {
      this.filteredValues = changes;
    });
  };
}
