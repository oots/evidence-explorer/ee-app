import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@shared/shared.module';

import { RequirementsDetailPageComponent } from './components/requirements-detail-page/requirements-detail-page.component';
import { RequirementsMainPageComponent } from './components/requirements-main-page/requirements-main-page.component';
import { RequirementsPageComponent } from './requirements-page.component';
import { RequirementsPageRoutingModule } from '@features/requirements-page/requirements-page-routing.module';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule, RequirementsPageRoutingModule],
  declarations: [RequirementsMainPageComponent, RequirementsDetailPageComponent, RequirementsPageComponent]
})
export class RequirementsPageModule {}
