import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import { EvidenceBrokerService } from 'src/app/core/services/evidence-broker.service';
import { IRequirement, IRequirementDetails } from '@shared/models/requirement.model';
import { setRequirements } from 'src/app/core/reducers/requirements/requirements.actions';
import { AppState } from 'src/app/core/reducers';
import { DataRequirementsService } from 'src/app/core/services/data-requirements.service';

@Component({
  templateUrl: './requirements-page.component.html',
  styleUrls: ['./requirements-page.component.scss']
})
export class RequirementsPageComponent implements OnInit, OnDestroy {
  requirementsSubscription: Subscription;
  evidencesSubscription: Subscription;

  requirements: IRequirement[];

  constructor(
    private _evidenceBrokerService: EvidenceBrokerService,
    private _dataRequirementsService: DataRequirementsService,
    private _store: Store<AppState>
  ) {
    this.requirementsSubscription = this._dataRequirementsService.requirementsList$.subscribe({
      next: (requirementsData: IRequirement[]) => {
        if (requirementsData.length > 0) {
          this.requirements = [...requirementsData];
          this._evidenceBrokerService.getEvidenceTypeList(requirementsData);
        } else {
          this.requirements = [];
          this._store.dispatch(setRequirements({ requirements: [] }));
        }
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.requirements = undefined;
        this._store.dispatch(setRequirements({ requirements: undefined }));
      }
    });
    this.evidencesSubscription = this._evidenceBrokerService.requirementsDetails$.subscribe({
      next: (requirementDetails: IRequirementDetails[]) => {
        const requirementsList: IRequirement[] = [...this.requirements];
        requirementsList.map((requirement: IRequirement, index: number) => {
          const requirementIndex: number = requirementDetails.findIndex(
            (requirementDetail: IRequirementDetails) => requirementDetail.requirementId === requirement.id
          );
          if (requirementIndex !== -1) {
            this.requirements[index].evidenceTypeList = requirementDetails[requirementIndex].evidenceTypes
              ? [...requirementDetails[requirementIndex].evidenceTypes]
              : [];
            this.requirements[index].proceduresList = requirementDetails[requirementIndex].procedures
              ? [...requirementDetails[requirementIndex].procedures]
              : [];
          } else {
            this.requirements[index].evidenceTypeList = [];
            this.requirements[index].proceduresList = [];
          }
        });
        this._store.dispatch(setRequirements({ requirements: this.requirements }));
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.requirements = undefined;
        this._store.dispatch(setRequirements({ requirements: undefined }));
      }
    });
  }

  ngOnInit(): void {
    this._store
      .select('requirements')
      .subscribe((state: AppState) => {
        if (!state.requirementsList) {
          this._dataRequirementsService.getRequirementsList();
        }
      })
      .unsubscribe();
  }

  ngOnDestroy(): void {
    this.requirementsSubscription.unsubscribe();
    this.evidencesSubscription.unsubscribe();
  }
}
