import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EvidencesPageComponent } from '@features/evidences-providers-page/evidences-page.component';
import { EvidencesMainPageComponent } from '@features/evidences-providers-page/components/evidences-main-page/evidences-main-page.component';
import { EvidenceTypesDetailPageComponent } from '@features/evidences-providers-page/components/evidence-types-detail-page/evidence-types-detail-page.component';
import { EvidenceProvidersDetailPageComponent } from '@features/evidences-providers-page/components/evidence-providers-detail-page/evidence-providers-detail-page.component';

const routes: Routes = [
  {
    path: '',
    component: EvidencesPageComponent,
    children: [
      { path: '', component: EvidencesMainPageComponent },
      { path: 'evidence-type-detail', component: EvidenceTypesDetailPageComponent },
      { path: 'evidence-provider-detail', component: EvidenceProvidersDetailPageComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvidencesProvidersPageRoutingModule {}
