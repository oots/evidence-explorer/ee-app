import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@shared/shared.module';
import { EvidencesPageComponent } from './evidences-page.component';
import { EvidencesMainPageComponent } from './components/evidences-main-page/evidences-main-page.component';
import { EvidenceTypesDetailPageComponent } from './components/evidence-types-detail-page/evidence-types-detail-page.component';
import { EvidenceProvidersDetailPageComponent } from './components/evidence-providers-detail-page/evidence-providers-detail-page.component';
import { EvidencesProvidersPageRoutingModule } from '@features/evidences-providers-page/evidences-providers-page-routing.module';

@NgModule({
  imports: [SharedModule, ReactiveFormsModule, EvidencesProvidersPageRoutingModule],
  declarations: [EvidencesPageComponent, EvidencesMainPageComponent, EvidenceTypesDetailPageComponent, EvidenceProvidersDetailPageComponent]
})
export class EvidencesProvidersPageModule {}
