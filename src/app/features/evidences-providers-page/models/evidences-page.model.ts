import { EntityType } from '@shared/models/common.model';

export interface IEvidencesPageUrlDetailLink {
  tableId?: string;
  columnId?: string;
  urlRedirect?: string;
  elementType?: EntityType;
}

export interface IEvidencesPageFormField {
  search?: string;
  select?: string[];
}

export interface IEvidencesPageStatus {
  dataLoading: boolean;
  errorState: boolean;
}
