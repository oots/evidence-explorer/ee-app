import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from 'src/app/core/reducers';
import { DataEvidencesService } from 'src/app/core/services/data-evidences.service';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';
import { setEvidenceProvidersList, setEvidenceTypesList } from 'src/app/core/reducers/evidences/evidences.action';
import { ISubjectContent } from 'src/app/core/services/models/data-evidences.model';

@Component({
  selector: 'app-evidences-page',
  templateUrl: './evidences-page.component.html',
  styleUrls: ['./evidences-page.component.scss']
})
export class EvidencesPageComponent implements OnInit, OnDestroy {
  private evidenceTypesList: Subscription;
  private evidenceProviderList: Subscription;

  constructor(
    private _store: Store<AppState>,
    private _dataEvidencesService: DataEvidencesService
  ) {
    this.evidenceTypesList = this._dataEvidencesService.evidenceTypes$.subscribe({
      next: (response: ISubjectContent) => {
        if (response.origin === 'main') {
          this._store.dispatch(setEvidenceTypesList({ evidencesList: response.content as IEvidenceType[] }));
        }
      },
      error: (error: ISubjectContent) => {
        if (error.origin === 'main') {
          console.error('Service error: ', error.error);
          this._store.dispatch(setEvidenceTypesList({ evidencesList: undefined }));
        }
      }
    });
    this.evidenceProviderList = this._dataEvidencesService.providers$.subscribe({
      next: (response: ISubjectContent) => {
        if (response.origin === 'main') {
          this._store.dispatch(setEvidenceProvidersList({ providersList: response.content as IProvider[] }));
        }
      },
      error: (error: ISubjectContent) => {
        if (error.origin === 'main') {
          console.error('Service error: ', error.error);
          this._store.dispatch(setEvidenceProvidersList({ providersList: undefined }));
        }
      }
    });
  }

  ngOnInit(): void {
    this._store
      .select('evidences')
      .subscribe((state: AppState) => {
        if (!state.evidencesList) {
          this._dataEvidencesService.getEvidenceTypesList('main');
        }
        if (!state.providersList) {
          this._dataEvidencesService.getProvidersList('main');
        }
      })
      .unsubscribe();
  }

  ngOnDestroy(): void {
    this.evidenceProviderList.unsubscribe();
    this.evidenceTypesList.unsubscribe();
  }
}
