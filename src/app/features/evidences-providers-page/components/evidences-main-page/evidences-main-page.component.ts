import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';

import { ETableColumnType, ITable, ITableConfig, ITableRow } from '@shared/components/table/models/table.model';
import { ECountryCodeList, ISelectOption } from '@shared/models/common.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';
import { sortItems } from '@shared/utils/utils';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { IMessageBoxData } from '@shared/components/message/models/message-box.model';
import { setEvidenceProvidersList, setEvidenceTypesList, setSelectedEvidence } from 'src/app/core/reducers/evidences/evidences.action';
import { IEvidencesPageFormField, IEvidencesPageStatus } from '../../models/evidences-page.model';

@Component({
  selector: 'app-evidences-main-page',
  templateUrl: './evidences-main-page.component.html',
  styleUrls: ['./evidences-main-page.component.scss']
})
export class EvidencesMainPageComponent implements OnInit, OnDestroy {
  filtersForm: FormGroup = new FormGroup({
    search: new FormControl(),
    select: new FormControl()
  });
  filteredValues: IEvidencesPageFormField = {
    search: null,
    select: null
  };
  evTypesTableData: ITable = { header: [], body: [] };
  evTypesTableConfig: ITableConfig;
  evProvidersTableData: ITable = { header: [], body: [] };
  evProvidersTableConfig: ITableConfig;
  multiselectOptions: ISelectOption[] = [];
  evTypesTotalItems = 0;
  evProvidersTotalItems = 0;
  isMobile: boolean;
  evidencesStatus: IEvidencesPageStatus = { dataLoading: true, errorState: false };
  providersStatus: IEvidencesPageStatus = { dataLoading: true, errorState: false };
  errorMessage: IMessageBoxData = {
    title: 'common.message-box.service-error.title',
    description: 'common.message-box.service-error.description',
    state: 'error'
  };
  tabSelected: 'evidenceProviders' | 'evidenceTypes' = 'evidenceTypes';

  private stateSubscription: Subscription;
  private formSubscription: Subscription;
  private breakpointSubscription: Subscription;
  private serviceData = { evidenceTypeList: [], providersList: [] };

  constructor(
    private _breakpointObserver: BreakpointObserver,
    private _store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.breakpointSubscription = this._breakpointObserver.observe(['(min-width: 996px)']).subscribe((state: BreakpointState) => {
      const size = state.breakpoints['(min-width: 996px)'] ? 'desktop' : 'mobile';
      this.isMobile = size === 'mobile';
      this.setTablesConfig();
    });
  }

  ngOnInit(): void {
    this.stateSubscription = this._store.select('evidences').subscribe((state: AppState) => {
      if (state.evidencesList !== null) {
        if (state.evidencesList && this.serviceData.evidenceTypeList.length === 0) {
          this.serviceData.evidenceTypeList = state.evidencesList;
          this.setEvidenceTypesDataSource();
          this.setSelectOptions();
        } else if (!state.evidencesList) {
          this.evidencesStatus.errorState = true;
        }
        this.evidencesStatus.dataLoading = false;
      }
      if (state.providersList !== null) {
        if (state.providersList && this.serviceData.providersList.length === 0) {
          this.serviceData.providersList = state.providersList;
          this.setProvidersDataSource();
          this.setSelectOptions();
        } else if (!state.providersList) {
          this.providersStatus.errorState = true;
        }
        this.providersStatus.dataLoading = false;
      }
    });
    this.trackFromGroupChanges();
  }

  ngOnDestroy(): void {
    this.stateSubscription && this.stateSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
    this.breakpointSubscription.unsubscribe();
    this.evidencesStatus.errorState && this._store.dispatch(setEvidenceTypesList({ evidencesList: null }));
    this.providersStatus.errorState && this._store.dispatch(setEvidenceProvidersList({ providersList: null }));
  }

  onClearInputHandler = (): void => {
    this.filtersForm.controls['search'].reset();
  };

  updateTotalItems = (totalItems: number, table: 'evidence' | 'provider'): void => {
    if (table === 'evidence') {
      this.evTypesTotalItems = totalItems;
    } else {
      this.evProvidersTotalItems = totalItems;
    }
  };

  onRowClickHandler = (evidenceId: string, table: 'evidence' | 'provider'): void => {
    this.stateSubscription.unsubscribe();
    if (table === 'evidence') {
      this._store.dispatch(setSelectedEvidence({ evidenceTypeId: evidenceId }));
      this.router.navigate(['evidence-type-detail'], { relativeTo: this.route });
    } else {
      this._store.dispatch(setSelectedEvidence({ evidenceProviderId: evidenceId }));
      this.router.navigate(['evidence-provider-detail'], { relativeTo: this.route });
    }
  };

  showFilters = (): boolean => {
    const evidencesStatus: boolean = !this.evidencesStatus.dataLoading && !this.evidencesStatus.errorState;
    const providersStatus: boolean = !this.providersStatus.dataLoading && !this.providersStatus.errorState;
    return evidencesStatus || providersStatus;
  };

  private setEvidenceTypesDataSource = (): void => {
    const evTypesData: ITableRow[] = this.serviceData.evidenceTypeList.map((evidenceType: IEvidenceType) => {
      const countryName: string = evidenceType.jurisdiction.length > 0 ? ECountryCodeList[evidenceType.jurisdiction[0].countryCode] : '-';
      const row: ITableRow = [
        { columnId: 'evidenceCountry', value: countryName, flagCode: countryName },
        { columnId: 'title', value: [evidenceType.title], valueId: [evidenceType.classification] },
        { columnId: 'providers', value: evidenceType.providersCount }
      ];
      return row;
    });
    this.evTypesTableData.body = [
      ...evTypesData.sort((a: ITableRow, b: ITableRow) => {
        const valueA: string = a[0].value.toString();
        const valueB: string = b[0].value.toString();
        return sortItems(valueA, valueB, 'ascending');
      })
    ];
    this.evTypesTotalItems = this.evTypesTableData.body.length;
  };

  private setProvidersDataSource = (): void => {
    const evProvidersData: ITableRow[] = this.serviceData.providersList.map((provider: IProvider) => {
      const countryName: string = provider.jurisdiction.countryCode ? ECountryCodeList[provider.jurisdiction.countryCode] : '-';
      const row: ITableRow = [
        { columnId: 'providerCountry', value: countryName, flagCode: countryName },
        { columnId: 'title', value: [provider.title], valueId: [provider.id] },
        { columnId: 'evidenceTypes', value: provider.evidenceCount }
      ];
      return row;
    });
    this.evProvidersTableData.body = [
      ...evProvidersData.sort((a: ITableRow, b: ITableRow) => {
        const valueA: string = a[0].value.toString();
        const valueB: string = b[0].value.toString();
        return sortItems(valueA, valueB, 'ascending');
      })
    ];
    this.evProvidersTotalItems = this.evProvidersTableData.body.length;
  };

  private setTablesConfig = (): void => {
    this.evTypesTableData.header = [
      {
        id: 'evidenceCountry',
        name: 'evidences-page.main.evidence-types.member-state-column',
        type: ETableColumnType.flag
      },
      {
        id: 'title',
        name: 'evidences-page.main.evidence-types.english-name-column',
        type: ETableColumnType.string,
        allowFilter: true
      },
      {
        id: 'providers',
        name: 'evidences-page.main.evidence-types.evidence-providers-column',
        type: ETableColumnType.number
      }
    ];
    this.evProvidersTableData.header = [
      {
        id: 'providerCountry',
        name: 'evidences-page.main.evidence-providers.member-state-column',
        type: ETableColumnType.flag
      },
      {
        id: 'title',
        name: 'evidences-page.main.evidence-providers.name-column',
        type: ETableColumnType.string,
        allowFilter: true
      },
      {
        id: 'evidenceTypes',
        name: 'evidences-page.main.evidences-providers.linked-evidences-column',
        type: ETableColumnType.number
      }
    ];
    const tableConfig = {
      showTotalCount: false,
      showPaginator: true,
      columnSpacer: true,
      clickableRows: true
    };
    this.evTypesTableConfig = {
      ...tableConfig,
      columnWidths: [
        { columnId: 'evidenceCountry', width: '10%' },
        { columnId: 'title', width: '45%' },
        { columnId: 'providers', width: '10%' }
      ],
      sortableColumns: ['evidenceCountry', 'title', 'local', 'providers'],
      captionText: 'list of evidence types',
      emptyStateTitle: 'evidences-page.main.evidence-types.message-box.empty-state.title'
    };
    this.evProvidersTableConfig = {
      ...tableConfig,
      columnWidths: [
        { columnId: 'providerCountry', width: '10%' },
        { columnId: 'title', width: '45%' },
        { columnId: 'evidenceTypes', width: '10%' }
      ],
      sortableColumns: ['providerCountry', 'title', 'local', 'evidenceTypes'],
      captionText: 'list of evidence providers',
      emptyStateTitle: 'evidences-page.main.evidence-providers.message-box.empty-state.title'
    };
  };

  private setSelectOptions = (): void => {
    const options: ISelectOption[] = [];
    if (this.serviceData.evidenceTypeList) {
      this.serviceData.evidenceTypeList.map((evidenceType: IEvidenceType) => {
        if (evidenceType.jurisdiction.length > 0) {
          const countryName: string = ECountryCodeList[evidenceType.jurisdiction[0].countryCode];
          const newOption: ISelectOption = {
            label: `common.country-label.${countryName.toLowerCase()}`,
            value: countryName
          };
          if (!options.find((option: ISelectOption) => option.value === newOption.value)) options.push(newOption);
        }
      });
    }
    if (this.serviceData.providersList) {
      this.serviceData.providersList.map((provider: IProvider) => {
        if (provider.jurisdiction.countryCode) {
          const countryName: string = ECountryCodeList[provider.jurisdiction.countryCode];
          const newOption: ISelectOption = {
            label: `common.country-label.${countryName.toLowerCase()}`,
            value: countryName
          };
          if (!options.find((option: ISelectOption) => option.value === newOption.value)) options.push(newOption);
        }
      });
    }
    const sortedOptions: ISelectOption[] = options.sort((a: ISelectOption, b: ISelectOption) => {
      return sortItems(a.label, b.label, 'ascending');
    });
    this.multiselectOptions = [...sortedOptions];
  };

  private trackFromGroupChanges = (): void => {
    this.formSubscription = this.filtersForm.valueChanges.subscribe((changes: { search: string; select: string[] }) => {
      this.filteredValues = changes;
    });
  };
}
