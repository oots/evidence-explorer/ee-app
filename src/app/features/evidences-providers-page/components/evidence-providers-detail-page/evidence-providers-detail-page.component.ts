import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';

import { ETableColumnType, ILinkEmit, ITable, ITableCell, ITableConfig, ITableRow } from '@shared/components/table/models/table.model';
import { ECountryCodeList } from '@shared/models/common.model';
import { AppState } from 'src/app/core/reducers';
import { IProvider } from '@shared/models/provider.model';
import { IEvidencesPageUrlDetailLink } from '../../models/evidences-page.model';
import { setSelectedRequirement } from 'src/app/core/reducers/requirements/requirements.actions';
import { setSelectedEvidence } from 'src/app/core/reducers/evidences/evidences.action';
import { IRequirement, IRequirementDetails } from '@shared/models/requirement.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { DataEvidencesService } from 'src/app/core/services/data-evidences.service';
import { EvidenceBrokerService } from 'src/app/core/services/evidence-broker.service';
import { ISubjectContent } from 'src/app/core/services/models/data-evidences.model';
import { IMessageBoxData } from '@shared/components/message/models/message-box.model';
import { setBreadcrumbLabelDetail } from 'src/app/core/reducers/breadcrumb/breadcrumb.actions';
import { EMainPageSectionId } from '../../../../core/components/layout/breadcrumb/models/breadcrumb.model';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-evidence-providers-detail-page',
  templateUrl: './evidence-providers-detail-page.component.html',
  styleUrls: ['./evidence-providers-detail-page.component.scss']
})
export class EvidenceProvidersDetailPageComponent implements OnInit, OnDestroy {
  providerDetail: IProvider = null;
  evidenceTypesList: IEvidenceType[] = [];
  requirementsList: IRequirement[] = [];
  tableData: ITable;
  tableConfig: ITableConfig;
  isMobile: boolean;
  dataLoading = true;
  errorState = false;
  pageTitle = 'evidences-page.detail.evidence-providers.title.default';
  urlsConfig: IEvidencesPageUrlDetailLink[];
  countryLabel: string;
  breakpointSubscription: Subscription;
  evidenceProviderSubscription: Subscription;
  requirementSubscription: Subscription;
  requirementsDetailSubscription: Subscription;
  stateSubscription: Subscription;
  errorMessage: IMessageBoxData = {
    title: 'common.message-box.service-error.title',
    description: 'common.message-box.service-error.description',
    state: 'error'
  };

  constructor(
    private _store: Store<AppState>,
    private _breakpointObserver: BreakpointObserver,
    private _dataEvidencesService: DataEvidencesService,
    private _evidenceBrokerService: EvidenceBrokerService,
    private sharedService: SharedService,
    private router: Router
  ) {
    this.breakpointSubscription = this._breakpointObserver.observe(['(min-width: 996px)']).subscribe((state: BreakpointState) => {
      const size = state.breakpoints['(min-width: 996px)'] ? 'desktop' : 'mobile';
      this.isMobile = size === 'mobile';
    });
    this.evidenceProviderSubscription = this._dataEvidencesService.providers$.subscribe({
      next: (response: ISubjectContent) => {
        if (response.origin === 'detail' && response.contentType === 'list') {
          const selectedEvidences: IEvidenceType[] = this.evidenceTypesList.filter((evType: IEvidenceType) =>
            (response.content as IProvider[])[0].dataServiceIds.evidenceTypeIds.includes(evType.id)
          );
          this.evidenceTypesList = [...selectedEvidences];
          this._evidenceBrokerService.getRequirementsList();
        }
        if (response.origin === 'detail' && response.contentType === 'detail') {
          this.providerDetail.localTitle = response.content[0].localTitle;
          this._dataEvidencesService.getEvidenceTypeDetailsByProvider('detail', this.providerDetail);
        }
      },
      error: (error: ISubjectContent) => {
        if (error.origin === 'detail' && error.contentType === 'detail') {
          console.error('Service error: ', error);
          this.setErrorState();
        }
      }
    });
    this.requirementSubscription = this._evidenceBrokerService.requirementList$.subscribe({
      next: (requirementsListResponse: IRequirement[]) => {
        this.requirementsList = [...requirementsListResponse];
        this._evidenceBrokerService.getEvidenceTypeList(requirementsListResponse, this.providerDetail.jurisdiction.countryCode);
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.setErrorState();
      }
    });
    this.requirementsDetailSubscription = this._evidenceBrokerService.requirementsDetails$.subscribe({
      next: (requirementsDetailsResponse: IRequirementDetails[]) => {
        const selectedRequirements: IRequirement[] = [];
        const referenceEvidecesIds: string[] = this.evidenceTypesList.map((evidence: IEvidenceType) => evidence.classification);
        requirementsDetailsResponse.map((requirementDetail: IRequirementDetails) => {
          if (requirementDetail.evidenceTypes.some((item) => referenceEvidecesIds.includes(item.classification))) {
            const requirement: IRequirement = this.requirementsList.find(
              (requirement: IRequirement) => requirement.id === requirementDetail.requirementId
            );
            requirement.evidenceTypeList = [...requirementDetail.evidenceTypes];
            selectedRequirements.push(requirement);
          }
        });
        this.requirementsList = [...selectedRequirements];
        this.setDataSource();
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.setErrorState();
      }
    });
  }

  ngOnInit(): void {
    this.sharedService.scrollToTop();
    this.stateSubscription = this._store.select('evidences').subscribe((state: AppState) => {
      if (state.selectedProvider && state.providersList && state.evidencesList && this.providerDetail === null) {
        const providerSelected: IProvider = state.providersList.find((provider: IProvider) => provider.id === state.selectedProvider);
        if (providerSelected) {
          this.providerDetail = { ...providerSelected };
          this.pageTitle = this.providerDetail.title;
          this._store.dispatch(
            setBreadcrumbLabelDetail({
              detailLabel: {
                id: EMainPageSectionId['provider-detail'],
                label: this.providerDetail.title
              }
            })
          );
          this.countryLabel = ECountryCodeList[this.providerDetail.jurisdiction.countryCode];
          this.evidenceTypesList = [...state.evidencesList];
          this.setTablesConfig();
          this._dataEvidencesService.getProvidersDetailList('detail', [providerSelected]);
        } else {
          this.providerDetail = undefined;
        }
      }
      if (
        (!state.selectedProvider ||
          this.providerDetail === undefined ||
          state.providersList === undefined ||
          state.evidencesList === undefined) &&
        !this.errorState
      ) {
        if (!state.selectedProvider) {
          this.errorMessage.title = 'evidences-page.detail.evidence-providers.message-box.empty-title';
          this.errorMessage.description = 'evidences-page.detail.evidence-providers.message-box.empty-description';
        }
        this.setErrorState();
      }
    });
  }

  ngOnDestroy(): void {
    this.stateSubscription && this.stateSubscription.unsubscribe();
    this.breakpointSubscription.unsubscribe();
    this.requirementSubscription.unsubscribe();
    this.requirementsDetailSubscription.unsubscribe();
    this.evidenceProviderSubscription.unsubscribe();
  }

  onLinkClickHandler = (linkDetail: ILinkEmit): void => {
    this.stateSubscription.unsubscribe();
    const urlConfig: IEvidencesPageUrlDetailLink = this.urlsConfig.find((config) => config.columnId === linkDetail.columnId);
    if (urlConfig?.elementType === 'requirement') {
      this._store.dispatch(setSelectedRequirement({ requirementId: linkDetail.valueId }));
    } else if (urlConfig?.elementType === 'evidence') {
      this._store.dispatch(setSelectedEvidence({ evidenceTypeId: linkDetail.valueId }));
    }
    this.router.navigate([urlConfig?.urlRedirect]);
  };

  getLocalName = (value: string, part: 'code' | 'title'): string => {
    const valueParts: string[] = value.split('#');
    const code: string = valueParts.length === 1 ? '' : valueParts[0];
    const title: string = valueParts.length === 1 ? valueParts[0] : valueParts[1];
    if (part === 'code') return code.toUpperCase();
    return title;
  };

  private setTablesConfig = (): void => {
    this.tableData = {
      header: [
        {
          id: 'evidences',
          name: 'evidences-page.detail.evidence-providers.main-table.evidence-type-column',
          type: ETableColumnType.link
        },
        {
          id: 'requirement',
          name: 'evidences-page.detail.evidence-providers.main-table.requirement-column',
          type: ETableColumnType.link
        },
        {
          id: 'extra',
          name: 'evidences-page.detail.evidence-providers.main-table.additional-evidences-column',
          type: ETableColumnType['link-connector']
        }
      ],
      body: []
    };
    this.tableConfig = {
      titleText: 'evidences-page.detail.evidence-providers.main-table.title',
      descriptionText: 'evidences-page.detail.evidence-providers.main-table.description',
      captionText: 'list of evidence types provided',
      columnWidths: [
        { columnId: 'evidences', width: '20%' },
        { columnId: 'requirement', width: '40%' },
        { columnId: 'extra', width: '40%' }
      ],
      columnSpacer: true,
      emptyStateTitle: 'evidences-page.detail.evidence-providers.main-table.message-box.empty-state.title'
    };
    this.urlsConfig = [
      { columnId: 'evidences', urlRedirect: '/evidences/evidence-type-detail', elementType: 'evidence' },
      { columnId: 'requirement', urlRedirect: '/requirements/requirement-detail', elementType: 'requirement' },
      { columnId: 'extra', urlRedirect: '/evidences/evidence-type-detail', elementType: 'evidence' }
    ];
  };

  private setDataSource = (): void => {
    const rows: ITableRow[] = this.evidenceTypesList.map((evidenceRow: IEvidenceType) => {
      const row: ITableCell[] = [{ columnId: 'evidences', value: evidenceRow.title, valueId: evidenceRow.classification }];
      const requirement: IRequirement = this.requirementsList.find((requirement: IRequirement) =>
        requirement.evidenceTypeList.map((evidenceType: IEvidenceType) => evidenceType.classification).includes(evidenceRow.classification)
      );
      if (requirement) {
        const evidenceTypeListId: string = requirement.evidenceTypeList.find(
          (evidenceType: IEvidenceType) => evidenceType.classification === evidenceRow.classification
        ).evidenceTypeListId;
        const additionalEvidences: IEvidenceType[] = requirement.evidenceTypeList.filter(
          (evidenceType: IEvidenceType) =>
            evidenceType.evidenceTypeListId === evidenceTypeListId && evidenceType.classification !== evidenceRow.classification
        );
        row.push({ columnId: 'requirement', value: requirement.name, valueId: requirement.id });
        row.push({
          columnId: 'extra',
          value: additionalEvidences.map((evidence: IEvidenceType) => evidence.title),
          valueId: additionalEvidences.map((evidence: IEvidenceType) => evidence.classification),
          emptyValue: 'evidences-page.detail.evidence-providers.main-table.additional-evidences.empty-message'
        });
      } else {
        row.push({
          columnId: 'requirement',
          value: '',
          emptyValue: 'evidences-page.detail.evidence-providers.main-table.requirement.empty-message'
        });
        row.push({
          columnId: 'extra',
          value: [],
          emptyValue: 'evidences-page.detail.evidence-providers.main-table.additional-evidences.empty-message'
        });
      }
      return row;
    });
    this.tableData.body = [...rows];
    this.checkExtraRow();
    this.dataLoading = false;
  };

  private checkExtraRow = (): void => {
    const rowWithExtraEvidences: ITableRow = this.tableData.body.find((row: ITableCell[]) => (row[2].value as string[]).length > 0);
    if (!rowWithExtraEvidences) {
      this.tableData.body.map((row: ITableCell[]) => {
        row.pop();
      });
      this.tableData.header.pop();
      this.tableConfig.columnWidths = [
        { columnId: 'evidences', width: '50%' },
        { columnId: 'requirement', width: '50%' }
      ];
    }
  };

  private setErrorState = (): void => {
    this.errorState = true;
    this.dataLoading = false;
  };
}
