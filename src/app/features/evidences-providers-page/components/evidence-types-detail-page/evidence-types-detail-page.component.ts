import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { ETableColumnType, ILinkEmit, ITable, ITableConfig, ITableRow } from '@shared/components/table/models/table.model';
import { AppState } from 'src/app/core/reducers';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { ECountryCodeList } from '@shared/models/common.model';
import { IEvidencesPageUrlDetailLink } from '../../models/evidences-page.model';
import { IRequirement, IRequirementDetails } from '@shared/models/requirement.model';
import { IProvider } from '@shared/models/provider.model';
import { setSelectedRequirement } from 'src/app/core/reducers/requirements/requirements.actions';
import { setSelectedEvidence } from 'src/app/core/reducers/evidences/evidences.action';
import { EvidenceBrokerService } from 'src/app/core/services/evidence-broker.service';
import { DataEvidencesService } from 'src/app/core/services/data-evidences.service';
import { ISubjectContent } from 'src/app/core/services/models/data-evidences.model';
import { IMessageBoxData } from '@shared/components/message/models/message-box.model';
import { setBreadcrumbLabelDetail } from 'src/app/core/reducers/breadcrumb/breadcrumb.actions';
import { EMainPageSectionId } from '../../../../core/components/layout/breadcrumb/models/breadcrumb.model';
import { SharedService } from '@shared/services/shared.service';

@Component({
  selector: 'app-evidence-types-detail-page',
  templateUrl: './evidence-types-detail-page.component.html',
  styleUrls: ['./evidence-types-detail-page.component.scss']
})
export class EvidenceTypesDetailPageComponent implements OnInit, OnDestroy {
  evidenceDetail: IEvidenceType = null;
  requirementsList: IRequirement[] = [];
  providersTableConfig: ITableConfig;
  providersTableData: ITable;
  requirementsTableConfig: ITableConfig;
  requirementsTableData: ITable;
  isMobile: boolean;
  breakpointSubscription: Subscription;
  pageTitle = 'evidences-page.detail.evidence-types.title.default';
  countryLabel: string;
  urlsConfig: IEvidencesPageUrlDetailLink[];
  dataLoading = true;
  errorState = false;
  errorMessage: IMessageBoxData = {
    title: 'common.message-box.service-error.title',
    description: 'common.message-box.service-error.description',
    state: 'error'
  };

  private providersTableTitle: string;
  private stateSubscription: Subscription;
  private providersSubscription: Subscription;
  private requirementsSubscription: Subscription;
  private requirementsDetailSubscription: Subscription;
  private evidencesDetailsSubscription: Subscription;

  constructor(
    private _store: Store<AppState>,
    private _breakpointObserver: BreakpointObserver,
    private _dataEvidencesService: DataEvidencesService,
    private _evidenceBrokerService: EvidenceBrokerService,
    private sharedService: SharedService,
    private _translateService: TranslateService,
    private router: Router
  ) {
    this._translateService
      .get('evidences-page.detail.evidence-types.providers-table.title')
      .subscribe({
        next: (translation: string) => {
          this.providersTableTitle = translation;
        }
      })
      .unsubscribe();
    this.breakpointSubscription = this._breakpointObserver.observe(['(min-width: 996px)']).subscribe((state: BreakpointState) => {
      const size = state.breakpoints['(min-width: 996px)'] ? 'desktop' : 'mobile';
      this.isMobile = size === 'mobile';
      this.setTablesConfig();
      if (this.countryLabel) {
        this.providersTableConfig.titleText = `${this.providersTableTitle} ${this.countryLabel}?`;
      }
    });
    this.evidencesDetailsSubscription = this._dataEvidencesService.evidenceTypes$.subscribe({
      next: (response: ISubjectContent) => {
        if (response.origin === 'detail' && response.contentType === 'detail') {
          this.evidenceDetail.localTitle = response.content[0].localTitle;
          this._dataEvidencesService.getProviderDetailsByEvidenceType('detail', this.evidenceDetail);
        }
      },
      error: (error: ISubjectContent) => {
        if (error.origin === 'detail' && error.contentType === 'detail') {
          console.error('Service error: ', error.error);
          this.setErrorState();
        }
      }
    });
    this.providersSubscription = this._dataEvidencesService.providers$.subscribe({
      next: (response: ISubjectContent) => {
        if (response.origin === 'detail' && response.contentType === 'list') {
          if (response.content.length === 0) {
            this.evidenceDetail.providersDetails = [];
            this.setProvidersDataSource();
            this._evidenceBrokerService.getRequirementsList();
          } else {
            this._dataEvidencesService.getProvidersDetailList('detail', response.content as IProvider[]);
          }
        } else if (response.origin === 'detail' && response.contentType === 'detail') {
          this.evidenceDetail.providersDetails = [...(response.content as IProvider[])];
          this.setProvidersDataSource();
          this._evidenceBrokerService.getRequirementsList();
        }
      },
      error: (error: ISubjectContent) => {
        if (error.origin === 'detail') {
          console.error('Service error: ', error.error);
          this.setErrorState();
        }
      }
    });
    this.requirementsSubscription = this._evidenceBrokerService.requirementList$.subscribe({
      next: (requirementsResponse: IRequirement[]) => {
        if (requirementsResponse.length > 0) {
          this.requirementsList = [...requirementsResponse];
          this._evidenceBrokerService.getEvidenceTypeList(requirementsResponse, this.evidenceDetail.jurisdiction[0].countryCode);
        } else {
          this.setRequirementsDataSource();
        }
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.setErrorState();
      }
    });
    this.requirementsDetailSubscription = this._evidenceBrokerService.requirementsDetails$.subscribe({
      next: (requirementsDetailResponse: IRequirementDetails[]) => {
        const selectedRequirements: IRequirement[] = [];
        requirementsDetailResponse.map((requirementDetail: IRequirementDetails) => {
          const currentEvidenceDetail: IEvidenceType = requirementDetail.evidenceTypes.find(
            (evidence: IEvidenceType) => evidence.classification === this.evidenceDetail.classification
          );
          if (currentEvidenceDetail) {
            this.evidenceDetail.evidenceTypeListId = currentEvidenceDetail.evidenceTypeListId;
            const requirement: IRequirement = this.requirementsList.find(
              (requirement: IRequirement) => requirement.id === requirementDetail.requirementId
            );
            requirement.evidenceTypeList = [...requirementDetail.evidenceTypes];
            selectedRequirements.push(requirement);
          }
        });
        this.requirementsList = [...selectedRequirements];
        this.setRequirementsDataSource();
      },
      error: (error) => {
        console.error('Service error: ', error);
        this.setErrorState();
      }
    });
  }

  ngOnInit(): void {
    this.sharedService.scrollToTop();
    this.stateSubscription = this._store.select('evidences').subscribe((state: AppState) => {
      if (state.selectedEvidence && state.evidencesList && this.evidenceDetail === null) {
        const evidenceSelected: IEvidenceType = state.evidencesList.find(
          (evidence: IEvidenceType) => evidence.classification === state.selectedEvidence
        );
        if (evidenceSelected) {
          this.evidenceDetail = { ...evidenceSelected };
          this.pageTitle = this.evidenceDetail.title;
          this._store.dispatch(
            setBreadcrumbLabelDetail({ detailLabel: { id: EMainPageSectionId['evidence-detail'], label: this.evidenceDetail.title } })
          );
          this.countryLabel = ECountryCodeList[this.evidenceDetail.jurisdiction[0].countryCode];
          this.providersTableConfig.titleText = `${this.providersTableTitle} ${this.countryLabel}?`;
          this._dataEvidencesService.getEvidenceTypesDetailList('detail', [evidenceSelected]);
        } else {
          this.evidenceDetail = undefined;
        }
      }
      if ((!state.selectedEvidence || this.evidenceDetail === undefined || state.evidencesList === undefined) && !this.errorState) {
        if (!state.selectedEvidence) {
          this.errorMessage.title = 'evidences-page.detail.evidence-types.message-box.empty-title';
          this.errorMessage.description = 'evidences-page.detail.evidence-types.message-box.empty-description';
        }
        this.setErrorState();
      }
    });
  }

  ngOnDestroy(): void {
    this.stateSubscription && this.stateSubscription.unsubscribe();
    this.breakpointSubscription.unsubscribe();
    this.providersSubscription.unsubscribe();
    this.requirementsSubscription.unsubscribe();
    this.requirementsDetailSubscription.unsubscribe();
    this.evidencesDetailsSubscription.unsubscribe();
  }

  onLinkClickHandler = (tableId: string, linkDetail: ILinkEmit): void => {
    this.stateSubscription.unsubscribe();
    const urlConfig: IEvidencesPageUrlDetailLink = this.urlsConfig.find(
      (config) => config.tableId === tableId && config.columnId === linkDetail.columnId
    );
    if (urlConfig?.elementType === 'requirement') {
      this._store.dispatch(setSelectedRequirement({ requirementId: linkDetail.valueId }));
    } else if (urlConfig?.elementType === 'provider') {
      this._store.dispatch(setSelectedEvidence({ evidenceProviderId: linkDetail.valueId }));
    }
    this.router.navigate([urlConfig?.urlRedirect]);
  };

  getLocalName = (value: string, part: 'code' | 'title'): string => {
    const valueParts: string[] = value.split('#');
    const code: string = valueParts.length === 1 ? '' : valueParts[0];
    const title: string = valueParts.length === 1 ? valueParts[0] : valueParts[1];
    if (part === 'code') return code.toUpperCase();
    return title;
  };

  private setTablesConfig = (): void => {
    this.providersTableData = {
      header: [
        { id: 'name', name: 'evidences-page.detail.evidence-types.providers-table.evidence-provider-column', type: ETableColumnType.link },
        {
          id: 'jurisdiction',
          name: this.isMobile
            ? 'evidences-page.detail.evidence-types.providers-table.jurisdiction-column.mobile'
            : 'evidences-page.detail.evidence-types.providers-table.jurisdiction-column',
          type: ETableColumnType.string,
          tooltipText: [
            'common.tooltip-text.jurisdiciton-column(1)',
            'common.tooltip-text.jurisdiciton-column(2)',
            'common.tooltip-text.jurisdiciton-column(3)'
          ]
        },
        { id: 'format', name: 'evidences-page.detail.evidence-types.providers-table.format-column', type: ETableColumnType.string }
      ],
      body: []
    };
    this.providersTableConfig = {
      titleText: '',
      descriptionText: 'evidences-page.detail.evidence-types.providers-table.description',
      captionText: 'providers of the evidence type',
      columnWidths: [
        { columnId: 'name', width: '40%' },
        { columnId: 'jurisdiction', width: '40%' },
        { columnId: 'format', width: '20%' }
      ],
      columnSpacer: true,
      emptyStateTitle: 'evidences-page.detail.evidence-types.providers-table.message-box.empty-state.title'
    };
    this.requirementsTableData = {
      header: [
        {
          id: 'requirement',
          name: 'evidences-page.detail.evidence-types.requirements-table.procedural-requirements-column',
          type: ETableColumnType.link
        },
        {
          id: 'evidence',
          name: this.isMobile
            ? 'evidences-page.detail.evidence-types.requirements-table.evidence-types-column.mobile'
            : 'evidences-page.detail.evidence-types.requirements-table.evidence-types-column',
          type: ETableColumnType.connector,
          tooltipText: [
            'evidences-page.detail.evidence-types.requirements-table.types-tooltip1',
            'evidences-page.detail.evidence-types.requirements-table.types-tooltip2',
            'evidences-page.detail.evidence-types.requirements-table.types-tooltip3'
          ]
        }
      ],
      body: []
    };
    this.requirementsTableConfig = {
      titleText: 'evidences-page.detail.evidence-types.requirements-table.title',
      descriptionText: 'evidences-page.detail.evidence-types.requirements-table.description',
      captionText: 'list of evidence types',
      columnWidths: [
        { columnId: 'requirement', width: '40%' },
        { columnId: 'evidence', width: '60%' }
      ],
      columnSpacer: true,
      emptyStateTitle: 'evidences-page.detail.evidence-types.requirements-table.message-box.empty-state.title'
    };
    this.urlsConfig = [
      { tableId: 'providers', columnId: 'name', urlRedirect: '/evidences/evidence-provider-detail', elementType: 'provider' },
      { tableId: 'requirements', columnId: 'requirement', urlRedirect: 'requirements/requirement-detail', elementType: 'requirement' }
    ];
  };

  private setProvidersDataSource = (): void => {
    const rows: ITableRow[] = this.evidenceDetail.providersDetails.map((providerDetail: IProvider) => {
      const jurisdictionValue: string = providerDetail.jurisdiction.codeLevel2
        ? `Regional (${providerDetail.jurisdiction.nameLevel2}, ${ECountryCodeList[providerDetail.jurisdiction.countryCode]})`
        : `National (${ECountryCodeList[providerDetail.jurisdiction.countryCode]})`;
      const format: string = providerDetail.format.join(', ');
      const row: ITableRow = [
        { columnId: 'name', value: providerDetail.title, valueId: providerDetail.id },
        { columnId: 'jurisdiction', value: [jurisdictionValue] },
        { columnId: 'format', value: [format] }
      ];
      return row;
    });
    this.providersTableData.body = [...rows];
  };

  private setRequirementsDataSource = (): void => {
    const rows: ITableRow[] = this.requirementsList.map((requirement: IRequirement) => {
      const row: ITableRow = [
        { columnId: 'requirement', value: requirement.name, valueId: requirement.id },
        { columnId: 'evidence', value: this.evidenceDetail.title, connector: 'only', secondaryValues: [] }
      ];
      if (requirement.evidenceTypeList.length > 1) {
        const evidencesSameList: IEvidenceType[] = [];
        requirement.evidenceTypeList.map((evType: IEvidenceType) => {
          if (
            evType.classification !== this.evidenceDetail.classification &&
            evType.evidenceTypeListId === this.evidenceDetail.evidenceTypeListId
          ) {
            evidencesSameList.push(evType);
          }
        });
        if (evidencesSameList.length > 0) {
          row[1].connector = 'AND';
          evidencesSameList.map((evType: IEvidenceType) => {
            row[1].secondaryValues.push(evType.title);
          });
        }
      }
      return row;
    });
    this.requirementsTableData.body = [...rows];
    this.dataLoading = false;
  };

  private setErrorState = (): void => {
    this.errorState = true;
    this.dataLoading = false;
  };
}
