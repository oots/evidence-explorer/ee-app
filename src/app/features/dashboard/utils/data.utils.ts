import { ApexChart, ApexStroke } from '@eui/components/externals/charts';
import { COUNTRIES } from '@shared/utils/constants';
import { IAssetCountPerCountry } from '@shared/models/common.model';

// eslint-disable-next-line
export const DATA: { chart: ApexChart; stroke: ApexStroke; [key: string]: any } = {
  chart: {
    width: '500px',
    type: 'bar',
    zoom: {
      enabled: false
    },
    toolbar: {
      show: false
    }
  },
  colors: ['#DA8623'],
  dataLabels: {
    enabled: true
  },
  stroke: {
    width: [3, 3],
    curve: 'smooth'
  },
  series: [
    {
      name: 'Month Progress',
      data: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'].map((month, index) => ({
        x: month,
        y: [10, 14, 20, 26, 31, 38, 42, 49, 55, 61, 66, 77][index]
      }))
    }
  ],
  grid: {
    row: {
      colors: ['transparent', 'transparent'], // takes an array which will be repeated on columns
      opacity: 0.2
    },
    borderColor: '#e9ecef'
  },
  markers: {
    style: 'inverted',
    size: 6
  },
  xaxis: {
    categories: ['test', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
    title: {
      text: 'Month'
    },
    axisBorder: {
      color: '#d6ddea'
    },
    axisTicks: {
      color: '#d6ddea'
    }
  },
  yaxis: {
    title: {
      text: 'Temperature'
    },
    min: 5,
    max: 40
  },
  legend: {
    position: 'top',
    horizontalAlign: 'right',
    floating: true,
    offsetY: -25,
    offsetX: -5
  },
  tooltip: {
    theme: 'dark',
    x: { show: false }
  },
  responsive: [
    {
      breakpoint: 600,
      options: {
        chart: {
          toolbar: {
            show: false
          }
        },
        legend: {
          show: false
        }
      }
    }
  ]
};

// eslint-disable-next-line
export const stackedMockData: { chart: ApexChart; stroke: ApexStroke; [key: string]: any } = {
  chart: {
    height: 600,
    type: 'bar',
    stacked: true,
    zoom: {
      enabled: false
    },
    toolbar: {
      show: false
    }
  },
  colors: ['#4073AF', 'transparent'],
  stroke: {
    width: [1, 1],
    curve: 'smooth'
  },
  legend: {
    show: true,
    position: 'bottom',
    horizontalAlign: 'center',
    floating: false,
    offsetY: 5,
    offsetX: -5,
    fontSize: '16px'
  },
  dataLabels: {
    enabled: true,
    offsetY: -20,
    style: {
      fontSize: '12px',
      colors: ['#304758']
    },
    formatter: function (val, opts) {
      return val > 0 ? val : '';
    }
  },
  plotOptions: {
    bar: {
      dataLabels: {
        position: 'top'
      }
    }
  },
  xaxis: {
    axisBorder: {
      color: '#d6ddea'
    },
    axisTicks: {
      show: false
    }
  },
  tooltip: {
    theme: 'dark',
    x: { show: true }
  },
  series: ['Birth', 'Residence', 'Studying', 'Moving', 'Retiring', 'Starting, running and closing a Business'].map((event) => ({
    name: event,
    data: COUNTRIES.map((country) => ({
      x: country.code.toUpperCase(),
      y: Array(4)
        .fill(0)
        .map(() => Math.floor(Math.random() * 37) + 10)
    }))
  })),
  autoUpdateSeries: true,
  annotations: {
    points: []
  }
};

// eslint-disable-next-line
export const getAssetChartData = (assetData: IAssetCountPerCountry[]): { chart: ApexChart; [key: string]: any } => {
  return {
    series: [
      {
        name: 'Published',
        data: COUNTRIES.map((country) => ({
          x: country.code,
          y: assetData.find((assetCountry) => assetCountry.countryCode === country.code).published
        })),
        color: '#26A761'
      },
      {
        name: 'In review',
        data: COUNTRIES.map((country) => ({
          x: country.code,
          y: assetData.find((assetCountry) => assetCountry.countryCode === country.code).review
        })),
        color: '#4395E1'
      },
      {
        name: 'Draft',
        data: COUNTRIES.map((country) => ({
          x: country.code,
          y: assetData.find((assetCountry) => assetCountry.countryCode === country.code).draft
        })),
        color: '#E17A00'
      }
    ],
    autoUpdateSeries: true,
    chart: {
      type: 'bar',
      height: 600,
      stacked: true,
      toolbar: {
        show: false
      }
    },
    plotOptions: {
      bar: {
        horizontal: false
      }
    },
    legend: {
      itemMargin: {
        horizontal: 20
      }
    },
    stroke: {
      width: 1,
      colors: ['#fff']
    },
    xaxis: {
      categories: COUNTRIES.map((country) => country.code),
      labels: {
        style: {
          colors: getEuCountryCodeColors(assetData)
        }
      },
      axisTicks: {
        show: false
      }
    },
    yaxis: {
      labels: {
        style: {
          colors: ['#404040']
        }
      }
    },
    fill: {
      opacity: 1
    }
  };
};

const getEuCountryCodeColors = (assetData: IAssetCountPerCountry[]) => {
  return COUNTRIES.map((country) => {
    let total = 0;
    const countryData = assetData.find((assetCountry) => assetCountry.countryCode === country.code);
    total = countryData.published + countryData.review + countryData.draft;
    return total === 0 ? '#A0A0A0' : '#404040';
  });
};
