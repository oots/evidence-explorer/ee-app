import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { DataTypeEnum } from '@shared/enums/common.enum';
import { DashboardService } from '../../core/services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent implements OnInit, OnDestroy {
  miniCardsData = [
    { type: DataTypeEnum.PROCEDURE, count: 0 },
    { type: DataTypeEnum.REQUIREMENT, count: 0 },
    { type: DataTypeEnum.EVIDENCE, count: 0 },
    { type: DataTypeEnum.AUTHORITY, count: 0 }
  ];

  private assetCountSub: Subscription;

  constructor(private readonly dashboardService: DashboardService) {}

  ngOnInit(): void {
    this.assetCountSub = this.dashboardService.assetCountResults$.subscribe({
      next: (results) => {
        this.miniCardsData = results;
      }
    });
    this.dashboardService.getAssetCountPerCountry();
  }

  ngOnDestroy(): void {
    this.assetCountSub.unsubscribe();
  }
}
