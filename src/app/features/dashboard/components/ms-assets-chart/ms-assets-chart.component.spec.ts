import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsAssetsChartComponent } from './ms-assets-chart.component';

describe('MemberStateChartComponent', () => {
  let component: MsAssetsChartComponent;
  let fixture: ComponentFixture<MsAssetsChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MsAssetsChartComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(MsAssetsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
