import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApexChart } from '@eui/components/externals/charts';

import { getAssetChartData } from '@features/dashboard/utils/data.utils';
import { DashboardService } from '../../../../core/services/dashboard.service';
import { IAssetCountPerCountry } from '@shared/models/common.model';

@Component({
  selector: 'app-member-state-chart',
  templateUrl: './ms-assets-chart.component.html',
  styleUrl: './ms-assets-chart.component.scss'
})
export class MsAssetsChartComponent implements OnInit, OnDestroy {
  loading = false;
  error = false;
  selectedAssetTab = 0;
  assetData: IAssetCountPerCountry[][];
  assetChartData: { chart: ApexChart; [key: string]: any }; // eslint-disable-line

  private assetCountSub: Subscription;

  constructor(private readonly dashboardService: DashboardService) {
    this.assetData = this.dashboardService.createDefaultAssetDataByCountry();
    this.assetChartData = getAssetChartData(this.assetData[0]);
  }

  ngOnInit(): void {
    this.loading = true;
    this.assetCountSub = this.dashboardService.assetCountResultsPerCountry$.subscribe({
      next: (assetData) => {
        this.assetData = assetData;
        this.assetChartData = getAssetChartData(assetData[0]);
        this.loading = false;
      },
      error: (error) => {
        this.error = true;
        this.loading = false;
      }
    });
  }

  ngOnDestroy(): void {
    this.assetCountSub.unsubscribe();
  }

  onAssetTabSelect(event) {
    this.selectedAssetTab = parseInt(event.tab.id);
    this.assetChartData = getAssetChartData(this.assetData[this.selectedAssetTab]);
  }
}
