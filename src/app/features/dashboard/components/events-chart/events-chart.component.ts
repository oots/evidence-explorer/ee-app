import { Component } from '@angular/core';
import { COUNTRIES } from '@shared/utils/constants';
import { stackedMockData } from '@features/dashboard/utils/data.utils';

@Component({
  selector: 'app-events-chart',
  templateUrl: './events-chart.component.html',
  styleUrl: './events-chart.component.scss'
})
export class EventsChartComponent {
  selectedAssetTab = '1';
  COUNTRIES = COUNTRIES;
  data = stackedMockData;

  onAssetTabSelect(event) {}
}
