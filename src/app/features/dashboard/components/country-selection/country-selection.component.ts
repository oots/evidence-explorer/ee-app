import { Component, inject } from '@angular/core';
import { COUNTRIES } from '@shared/utils/constants';
import { CountryModel } from '@shared/models/common.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-country-selection',
  templateUrl: './country-selection.component.html',
  styleUrl: './country-selection.component.scss'
})
export class CountrySelectionComponent {
  router = inject(Router);
  countries: CountryModel[] = COUNTRIES;

  onCountryClick(countryCode: string) {
    this.router.navigate(['member-state', countryCode]);
  }
}
