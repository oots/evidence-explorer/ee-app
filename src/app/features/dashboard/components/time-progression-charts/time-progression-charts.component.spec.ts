import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeProgressionChartsComponent } from './time-progression-charts.component';

describe('TimeProgressionChartsComponent', () => {
  let component: TimeProgressionChartsComponent;
  let fixture: ComponentFixture<TimeProgressionChartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TimeProgressionChartsComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(TimeProgressionChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
