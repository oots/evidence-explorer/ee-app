import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeChartFiltersComponent } from './time-chart-filters.component';

describe('TimeChartFiltersComponent', () => {
  let component: TimeChartFiltersComponent;
  let fixture: ComponentFixture<TimeChartFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TimeChartFiltersComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(TimeChartFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
