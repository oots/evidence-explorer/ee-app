import { Component, Input } from '@angular/core';
import { DATA } from '@features/dashboard/utils/data.utils';

@Component({
  selector: 'app-time-progression-charts',
  templateUrl: './time-progression-charts.component.html',
  styleUrl: './time-progression-charts.component.scss'
})
export class TimeProgressionChartsComponent {
  @Input() title = 'Chart title';
  @Input() color = '#DA8623';
  data = this.color ? { ...DATA, colors: [`#${this.color}`] } : DATA;
}
