import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { TimeProgressionChartsComponent } from './components/time-progression-charts/time-progression-charts.component';
import { DashboardComponent } from './dashboard.component';
import { EclPageHeaderComponentModule } from '@eui/ecl';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
import { EuiChartsModule } from '@eui/components/externals/charts';
import { CountrySelectionComponent } from '@features/dashboard/components/country-selection/country-selection.component';
import { MsAssetsChartComponent } from '@features/dashboard/components/ms-assets-chart/ms-assets-chart.component';
import { EventsChartComponent } from './components/events-chart/events-chart.component';
import { TimeChartFiltersComponent } from './components/time-progression-charts/time-chart-filters/time-chart-filters.component';

@NgModule({
  declarations: [
    TimeProgressionChartsComponent,
    DashboardComponent,
    CountrySelectionComponent,
    MsAssetsChartComponent,
    EventsChartComponent,
    TimeChartFiltersComponent
  ],
  imports: [CommonModule, DashboardRoutingModule, EclPageHeaderComponentModule, TranslateModule, SharedModule, EuiChartsModule]
})
export class DashboardModule {}
