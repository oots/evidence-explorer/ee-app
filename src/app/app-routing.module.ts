import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { memberStateGuard } from './core/guards/member-state.guard';
import { WildcardComponent } from './core/components/wildcard/wildcard.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('@features/home-page/home-page.module').then((m) => m.HomePageModule)
  },
  {
    path: 'search-results',
    loadChildren: () => import('@features/search-results-page/search-results-page.module').then((m) => m.SearchResultsPageModule)
  },
  {
    path: 'requirements',
    loadChildren: () => import('@features/requirements-page/requirements-page.module').then((m) => m.RequirementsPageModule)
  },
  {
    path: 'evidences',
    loadChildren: () => import('@features/evidences-providers-page/evidences-page.module').then((m) => m.EvidencesProvidersPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('@features/dashboard/dashboard.module').then((m) => m.DashboardModule)
  },
  {
    path: 'procedures',
    loadChildren: () => import('@features/procedures-page/procedures-page.module').then((m) => m.ProceduresPageModule)
  },
  {
    path: 'member-state/:country',
    canActivate: [memberStateGuard],
    loadChildren: () => import('@features/ms-page/ms-page.module').then((m) => m.MsPageModule)
  },
  {
    path: 'not-found',
    component: WildcardComponent
  },
  { path: '**', redirectTo: 'not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })]
})
export class AppRoutingModule {}
