import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import {
  CachePreventionInterceptor,
  CoreModule as EuiCoreModule,
  CoreModuleEffects,
  CsrfPreventionInterceptor,
  EUI_CONFIG_TOKEN,
  EuLoginSessionTimeoutHandlingInterceptor,
  translateConfig
} from '@eui/core';
import { EuiLayoutModule } from '@eui/components/layout';

import { appConfig } from '../../config';
import { environment } from '../../environments/environment';

import { getReducers, metaReducers, REDUCER_TOKEN } from './reducers';

import { SharedModule } from '@shared/shared.module';
import { BreadcrumbComponent } from './components/layout/breadcrumb/breadcrumb.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { LayoutComponent } from './components/layout/layout.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { BaseComponent } from './components/base/base.component';
import { EuiTooltipDirectiveModule } from '@eui/components/directives';
import { WildcardComponent } from './components/wildcard/wildcard.component';

const DECLARATIONS = [HeaderComponent, BreadcrumbComponent, FooterComponent, LayoutComponent, BaseComponent, WildcardComponent];

@NgModule({
  imports: [
    EuiLayoutModule,
    EuiTooltipDirectiveModule,
    SharedModule,
    HttpClientModule,
    EuiCoreModule.forRoot(),
    EffectsModule.forRoot([...CoreModuleEffects]),
    TranslateModule.forRoot(translateConfig),
    StoreModule.forRoot(REDUCER_TOKEN, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 50 }) : []
  ],
  declarations: [...DECLARATIONS],
  exports: [...DECLARATIONS, EuiLayoutModule, SharedModule, TranslateModule],
  providers: [
    {
      provide: REDUCER_TOKEN,
      deps: [],
      useFactory: getReducers
    },
    {
      provide: EUI_CONFIG_TOKEN,
      useValue: { appConfig, environment }
    },
    {
      // WARNING: in case of OpenID this is not needed since OpenID is stateless therefore no revalidation needed.
      // When the authentication session is invalid, we need to re-authenticate. The browser refreshes the current URL,
      // and lets the EU Login client redirect to the official EU Login page.
      provide: HTTP_INTERCEPTORS,
      useClass: EuLoginSessionTimeoutHandlingInterceptor,
      multi: true
    },
    {
      // Adds HTTP header to each Ajax request that ensures the request is set by a piece of JavaScript code in the application.
      // This prevents dynamically-loaded content from forging a request in the name of the currently logged-in user.
      // Be aware that this assumes that cross-site scripting (XSS) is already put in place, (default setting in Angular).
      provide: HTTP_INTERCEPTORS,
      useClass: CsrfPreventionInterceptor,
      multi: true
    },
    {
      // Asks the intermediate proxies not to return a cache copy of the resource.
      // In matter of fact forces each server in the chain to validate the freshness of the resource.
      provide: HTTP_INTERCEPTORS,
      useClass: CachePreventionInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {}
