import { Injectable } from '@angular/core';
import { Subject, zip, Observable } from 'rxjs';
import { ApiDataRequirements } from './api/api-data-requirements.service';
import { IRequirement, IRequirementDomain } from '@shared/models/requirement.model';

@Injectable({
  providedIn: 'root'
})
export class DataRequirementsService {
  readonly requirementsList$ = new Subject<IRequirement[]>();

  constructor(private _apiDataRequirements: ApiDataRequirements) {}

  /**
   * Method to get the list of requirements available (JSON format)
   */
  getRequirementsList(): void {
    this._apiDataRequirements.getRequirementsList().subscribe({
      /* eslint-disable @typescript-eslint/no-explicit-any */
      next: (response: any) => {
        const requirementsList: IRequirement[] = [];
        if (response) {
          response.list.map((requirement) => {
            const domains: IRequirementDomain[] = requirement.domains.map((domain) => {
              return {
                id: domain.id,
                name: domain.name,
                description: domain.description
              };
            });
            requirementsList.push({
              id: requirement.identifier,
              name: requirement.name,
              description: requirement.description,
              domains: [...domains]
            });
          });
        }
        this.requirementsList$.next(requirementsList);
      },
      error: (error) => {
        this.requirementsList$.error(error);
      }
    });
  }
}
