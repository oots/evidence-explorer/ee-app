import { Injectable } from '@angular/core';
import { combineLatest, Subject } from 'rxjs';

import { IAssetCountPerCountry, IAssetsCount } from '@shared/models/common.model';
import { ApiDataProcedures } from './api/api-data-procedures.service';
import { ApiDataRequirements } from './api/api-data-requirements.service';
import { ApiDataEvidences } from './api/api-data-evidences.service';
import { COUNTRIES } from '@shared/utils/constants';
import { DataTypeEnum } from '@shared/enums/common.enum';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  public readonly assetCountResults$ = new Subject<IAssetsCount>();
  public readonly assetCountResultsPerCountry$ = new Subject<IAssetCountPerCountry[][]>();

  private assetDataByCountry: IAssetCountPerCountry[][];

  constructor(
    private readonly proceduresApi: ApiDataProcedures,
    private readonly requirementsApi: ApiDataRequirements,
    private readonly evidencesApi: ApiDataEvidences
  ) {
    this.assetDataByCountry = this.createDefaultAssetDataByCountry();
  }

  public getAssetCountPerCountry = (): void => {
    this.assetDataByCountry = this.createDefaultAssetDataByCountry();
    const assetsCount = [0, 0, 0, 0];
    combineLatest([
      this.proceduresApi.getProcedureCountByCountry(),
      this.requirementsApi.getRequirementCountByCountry(),
      this.evidencesApi.getEvidenceTypeCountByCountry(),
      this.evidencesApi.getProviderCountByCountry(),
      this.requirementsApi.getRequirementCountDistinct()
    ]).subscribe({
      next: (data) => {
        const countryData = data.slice(0, 4) as IAssetCountPerCountry[][];
        const requirementsDistinct = data[4];
        countryData.forEach((assetData, i) => {
          assetData.forEach((assetCountry, j) => {
            const countryDataIdx = this.assetDataByCountry[i].findIndex((item) => item.countryCode === assetCountry.countryCode);
            if (countryDataIdx >= 0) {
              this.assetDataByCountry[i][countryDataIdx] = { ...this.assetDataByCountry[i][countryDataIdx], ...assetCountry };
              assetsCount[i] += this.assetDataByCountry[i][countryDataIdx].published;
              assetsCount[i] += this.assetDataByCountry[i][countryDataIdx].review;
              assetsCount[i] += this.assetDataByCountry[i][countryDataIdx].draft;
            }
          });
        });
        this.assetCountResults$.next([
          { type: DataTypeEnum.PROCEDURE, count: assetsCount[0] },
          { type: DataTypeEnum.REQUIREMENT, count: requirementsDistinct },
          { type: DataTypeEnum.EVIDENCE, count: assetsCount[2] },
          { type: DataTypeEnum.AUTHORITY, count: assetsCount[3] }
        ]);
        this.assetCountResultsPerCountry$.next(this.assetDataByCountry);
      },
      error: (error) => {
        this.assetCountResultsPerCountry$.error(error);
      }
    });
  };

  public createDefaultAssetDataByCountry = (): IAssetCountPerCountry[][] => {
    const assetCountPerCountry: IAssetCountPerCountry[][] = [];
    for (let i = 0; i < 4; i++) {
      assetCountPerCountry[i] = COUNTRIES.map((country) => ({
        countryCode: country.code,
        count: 0,
        published: 0,
        review: 0,
        draft: 0
      }));
    }
    return assetCountPerCountry;
  };
}
