import { Injectable } from '@angular/core';

import { xml2js } from 'xml-js';
import { Subject, Observable, zip } from 'rxjs';

import { ApiDataServicesDirectoryService } from './api/api-data-services-directory.service';
import { IProvider, IProvidersServiceQueryParams } from '@shared/models/provider.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';

@Injectable({
  providedIn: 'root'
})
export class DataServicesDirectoryService {
  readonly providersInfoList$: Subject<IEvidenceType[]> = new Subject<IEvidenceType[]>();

  constructor(private _apiDataServicesDirectoryService: ApiDataServicesDirectoryService) {}

  /**
   * Method to get the list of evidence providers that provide an evidence type (XML format)
   * @param {IEvidenceType[]} evidenceTypesInitialData - list of evidence types for which to get the providers
   * @param {string} countryCode - country-code service param
   */
  getEvidenceProviderListByIds(evidenceTypesInitialData: IEvidenceType[], countryCode: string): void {
    const requests: Observable<string>[] = evidenceTypesInitialData.map((evidenceType: IEvidenceType) => {
      return this._apiDataServicesDirectoryService.getEvidenceProviders({
        'evidence-type-classification': evidenceType.classification,
        'country-code': countryCode
      });
    });
    const additionalRequestParams: IProvidersServiceQueryParams[] = [];
    zip(requests).subscribe({
      next: (responses: string[]) => {
        let evidenceTypesUpdatedData: IEvidenceType[] = [];
        responses.map((response: string, index: number) => {
          const parsedResponse = xml2js(response, { compact: true, ignoreComment: true, ignoreDeclaration: true, ignoreInstruction: true });
          if (!(parsedResponse['query:QueryResponse']._attributes.status as string).includes('Failure')) {
            let evidenceTypeResponseArray = parsedResponse['query:QueryResponse']['rim:RegistryObjectList']['rim:RegistryObject'];
            if (!Array.isArray(evidenceTypeResponseArray)) evidenceTypeResponseArray = [evidenceTypeResponseArray];
            evidenceTypeResponseArray.map((item) => {
              const evidenceTypeResponse = item['rim:Slot']['rim:SlotValue']['oots:DataServiceEvidenceType'];
              const evidenceType: IEvidenceType = this.getEvidenceInfoFromXML(evidenceTypeResponse, evidenceTypesInitialData[index]);
              evidenceTypesUpdatedData.push(evidenceType);
            });
          } else if ((parsedResponse['query:QueryResponse']['rs:Exception']._attributes.code as string) === 'DSD:ERR:0005') {
            const queryParams: IProvidersServiceQueryParams = this.getErrorRequestParamsFromXML(
              parsedResponse,
              evidenceTypesInitialData[index],
              countryCode
            );
            additionalRequestParams.push(queryParams);
          } else {
            evidenceTypesUpdatedData = [...evidenceTypesInitialData];
          }
        });
        if (additionalRequestParams.length === 0) {
          this.providersInfoList$.next(evidenceTypesUpdatedData);
        } else {
          this.getEvidenceProviderListAfterError(evidenceTypesInitialData, evidenceTypesUpdatedData, additionalRequestParams);
        }
      },
      error: (error) => {
        console.error('Service error:', error);
        this.providersInfoList$.error(error);
      }
    });
  }

  /**
   * Method to get the list of evidence providers that provide an evidence type after getting DSD:ERR:0005 error (XML format)
   * @param {IEvidenceType[]} evidenceTypesInitialData - list of evidence types for which to get the providers
   * @param {IEvidenceType[]} evidenceTypesUpdateData - list of evidence types for which we already have the information
   * @param {IProvidersServiceQueryParams[]} queryParamsList - array with the query params the make the additional request
   */
  getEvidenceProviderListAfterError(
    evidenceTypesInitialData: IEvidenceType[],
    evidenceTypesUpdateData: IEvidenceType[],
    queryParamsList?: IProvidersServiceQueryParams[]
  ) {
    const requests: Observable<string>[] = queryParamsList.map((queryParams: IProvidersServiceQueryParams) => {
      return this._apiDataServicesDirectoryService.getEvidenceProviders(queryParams);
    });
    zip(requests).subscribe({
      next: (responses: string[]) => {
        const evidenceTypeList: IEvidenceType[] = [...evidenceTypesUpdateData];
        responses.map((response: string, index: number) => {
          const parsedResponse = xml2js(response, { compact: true, ignoreComment: true, ignoreDeclaration: true, ignoreInstruction: true });
          if (!(parsedResponse['query:QueryResponse']._attributes.status as string).includes('Failure')) {
            const evidenceTypeResponseData =
              parsedResponse['query:QueryResponse']['rim:RegistryObjectList']['rim:RegistryObject']['rim:Slot']['rim:SlotValue'][
                'oots:DataServiceEvidenceType'
              ];
            const evidenceTypeInitial: IEvidenceType = evidenceTypesInitialData.find(
              (evidence: IEvidenceType) => evidence.classification === queryParamsList[index]['evidence-type-classification']
            );
            const evidenceType: IEvidenceType = this.getEvidenceInfoFromXML(evidenceTypeResponseData, evidenceTypeInitial);
            evidenceTypeList.push(evidenceType);
          }
        });
        this.providersInfoList$.next(evidenceTypeList);
      },
      error: (error) => {
        console.error('Service error:', error);
        this.providersInfoList$.error(error);
      }
    });
  }

  /**
   * Method to get the information of an evidence type from a XML fragment
   * @param evidenceTypeData - XML fragment with the data
   * @param classification - classification code
   * @param jurisdiction - jurisdiction data
   * @returns {IEvidenceType} evidence type with all the information
   */
  private getEvidenceInfoFromXML(evidenceTypeResponseData, evidenceTypeInitialData: IEvidenceType): IEvidenceType {
    const evidenceType: IEvidenceType = { ...evidenceTypeInitialData };
    let providers = evidenceTypeResponseData['oots:AccessService'];
    if (!Array.isArray(providers)) providers = [providers];
    evidenceType.providersCount = providers.length;
    evidenceType.providersDetails = [...this.getProvidersInfo(evidenceTypeResponseData)];
    return evidenceType;
  }

  /**
   * Method to get the information of multiple providers from a XML fragment
   * @param evidenceTypeData - XML fragment with the data
   * @returns {IProvider[]} list of evidence providers
   */
  private getProvidersInfo(evidenceTypeData): IProvider[] {
    const providersInfo = Array.isArray(evidenceTypeData['oots:AccessService'])
      ? evidenceTypeData['oots:AccessService']
      : [evidenceTypeData['oots:AccessService']];
    const providers: IProvider[] = [];
    providersInfo.map((info) => {
      const publisher = info['oots:Publisher'];
      const title: string = Array.isArray(publisher['oots:Name'])
        ? publisher['oots:Name'].find((name) => name._attributes.lang.toLowerCase() === 'en')._text
        : publisher['oots:Name']._text;
      const provider: IProvider = {
        id: publisher['oots:Identifier']._text,
        title,
        jurisdiction: {
          countryCode: publisher['oots:Jurisdiction']['oots:AdminUnitLevel1']?._text,
          codeLevel2: publisher['oots:Jurisdiction']['oots:AdminUnitLevel2']?._text,
          codeLevel3: publisher['oots:Jurisdiction']['oots:AdminUnitLevel3']?._text
        }
      };
      providers.push({ ...provider, format: [] });
    });
    return providers;
  }

  /**
   * Method to get the additional params from a XML fragment
   * @param parsedResponse - XML fragment with the data
   * @param {IEvidenceType} evidenceType - evidence type for which to get the providers
   * @param {string} countryCode - country code
   * @returns {IProvidersServiceQueryParams} query params
   */
  private getErrorRequestParamsFromXML(parsedResponse, evidenceType: IEvidenceType, countryCode: string): IProvidersServiceQueryParams {
    const queryParams: IProvidersServiceQueryParams = {
      'evidence-type-classification': evidenceType.classification,
      'country-code': countryCode
    };
    const dataServiceEvidenceType = parsedResponse['query:QueryResponse']['rs:Exception']['rim:Slot'].find(
      (slot) => slot._attributes.name === 'DataServiceEvidenceType'
    );
    const evidenceProviderJurDetermination = parsedResponse['query:QueryResponse']['rs:Exception']['rim:Slot'].find(
      (slot) => slot._attributes.name === 'JurisdictionDetermination'
    );
    const evidenceProviderClas = parsedResponse['query:QueryResponse']['rs:Exception']['rim:Slot'].find(
      (slot) => slot._attributes.name === 'UserRequestedClassificationConcepts'
    );
    queryParams['evidence-type-id'] = dataServiceEvidenceType['rim:SlotValue']['oots:DataServiceEvidenceType']['oots:Identifier']._text;
    if (evidenceProviderJurDetermination) {
      queryParams['jurisdiction-context-id'] =
        evidenceProviderJurDetermination['rim:SlotValue']['oots:EvidenceProviderJurisdictionDetermination'][
          'oots:JurisdictionContextId'
        ]._text;
      if (
        evidenceProviderJurDetermination['rim:SlotValue']['oots:EvidenceProviderJurisdictionDetermination'][
          'oots:JurisdictionLevel'
        ]?._text.includes('LAU')
      ) {
        queryParams['jurisdiction-admin-l3'] = evidenceType.jurisdiction[0].codeLevel3;
      } else if (
        evidenceProviderJurDetermination['rim:SlotValue']['oots:EvidenceProviderJurisdictionDetermination'][
          'oots:JurisdictionLevel'
        ]?._text.includes('NUTS')
      ) {
        queryParams['jurisdiction-admin-l2'] = evidenceType.jurisdiction[0].codeLevel2;
      }
    }
    if (evidenceProviderClas) {
      queryParams['evidenceProviderClasParam'] =
        evidenceProviderClas['rim:SlotValue']['rim:Element']['oots:EvidenceProviderClassification']['oots:Identifier']._text;
      queryParams['evidenceProviderClasValue'] =
        evidenceProviderClas['rim:SlotValue']['rim:Element']['oots:EvidenceProviderClassification']['oots:ValueExpression']?._text;
    }
    return queryParams;
  }
}
