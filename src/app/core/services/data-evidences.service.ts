import { Injectable } from '@angular/core';
import { Observable, Subject, zip } from 'rxjs';
import { ApiDataEvidences } from './api/api-data-evidences.service';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';
import { ISubjectContent } from './models/data-evidences.model';

@Injectable({
  providedIn: 'root'
})
export class DataEvidencesService {
  readonly evidenceTypes$ = new Subject<ISubjectContent>();
  readonly providers$ = new Subject<ISubjectContent>();

  constructor(private _apiDataEvidences: ApiDataEvidences) {}

  /**
   * Method to get the list of evidence types available (JSON format)
   */
  getEvidenceTypesList(origin: 'main' | 'detail'): void {
    this._apiDataEvidences.getEvidenceTypesList().subscribe({
      /* eslint-disable @typescript-eslint/no-explicit-any */
      next: (response: any) => {
        let evidenceTypesList: IEvidenceType[] = [];
        if (response) {
          const list = response.list;
          if (list?.length > 0) {
            evidenceTypesList = list.map((evidence) => {
              const evidenceType: IEvidenceType = {
                classification: evidence.classificationUrl,
                id: evidence.id,
                title: evidence.name,
                description: evidence.description,
                providersCount: evidence.providerCount,
                jurisdiction: evidence.countries.length > 0 ? [{ countryCode: evidence.countries[0].code }] : [],
                localTitle: []
              };
              return evidenceType;
            });
          }
        }
        evidenceTypesList = evidenceTypesList.filter((evidenceType: IEvidenceType) => evidenceType.jurisdiction.length !== 0);
        this.evidenceTypes$.next({ origin, contentType: 'list', content: evidenceTypesList });
      },
      error: (error) => {
        this.evidenceTypes$.error({ origin, contentType: 'list', error });
      }
    });
  }

  /**
   * Method to get the details of the evidence types given as parameter (JSON format)
   * @param {IEvidenceType[]} evidenceTypes - list containing the evidence types for which to get the details
   */
  getEvidenceTypesDetailList(origin: 'main' | 'detail', evidenceTypes: IEvidenceType[]): void {
    /* eslint-disable @typescript-eslint/no-explicit-any */
    const requests: Observable<any>[] = evidenceTypes.map((evidenceType: IEvidenceType) => {
      return this._apiDataEvidences.getEvidenceTypeDetail(evidenceType.id);
    });
    zip(requests).subscribe({
      next: (responseList) => {
        const evidenceTypesDetails: IEvidenceType[] = [];
        responseList.map((response, index) => {
          const evidenceType: IEvidenceType = {
            ...evidenceTypes[index]
          };
          evidenceType.description = response.description;
          evidenceType.localTitle = response.translations
            ? response.translations.map((translation) => {
                return `${translation.language.toUpperCase()}#${translation.name}`;
              })
            : [];
          if (response.jurisdictions.length > 0) {
            evidenceType.jurisdiction = response.jurisdictions.map((jurisdiction) => {
              return {
                countryCode: jurisdiction.countryCode,
                codeLevel2: jurisdiction.regionCode,
                nameLevel2: jurisdiction.regionName,
                codeLevel3: jurisdiction.municipalityCode,
                nameLevel3: jurisdiction.municipalityName
              };
            });
          }
          evidenceTypesDetails.push(evidenceType);
        });
        this.evidenceTypes$.next({ origin, contentType: 'detail', content: evidenceTypesDetails });
      },
      error: (error) => {
        this.evidenceTypes$.error({ origin, contentType: 'detail', error });
      }
    });
  }

  /**
   * Method to get the details of the providers associated with an evidence type (JSON format)
   * @param {IEvidenceType} evidenceType - evidence type for which to get the providers
   */
  getProviderDetailsByEvidenceType(origin: 'main' | 'detail', evidenceType: IEvidenceType): void {
    this._apiDataEvidences.getProviderDetailByEvidenceType(evidenceType.id).subscribe({
      /* eslint-disable @typescript-eslint/no-explicit-any */
      next: (response: any) => {
        let providersDetails: IProvider[] = [];
        if (response) {
          providersDetails = response.list
            .filter((provider) => provider?.status === 2)
            .map((provider) => {
              const providerDetail: IProvider = {
                id: undefined,
                dataServiceIds: {
                  providerId: provider.providerId,
                  evidenceTypeIds: [evidenceType.id]
                },
                title: provider.name,
                format: provider.metadata.distributions
                  ? provider.metadata.distributions.map((distribution) => distribution.formatName)
                  : []
              };
              return providerDetail;
            });
        }
        this.providers$.next({ origin, contentType: 'list', content: providersDetails });
      },
      error: (error) => {
        this.providers$.error({ origin, contentType: 'list', error });
      }
    });
  }

  /**
   * Method to get the list of providers available (JSON format)
   */
  getProvidersList(origin: 'main' | 'detail'): void {
    this._apiDataEvidences.getProvidersList().subscribe({
      /* eslint-disable @typescript-eslint/no-explicit-any */
      next: (response: any) => {
        let providersList: IProvider[] = [];
        if (response) {
          const list = response.list;
          if (list?.length > 0) {
            providersList = list.map((provider) => {
              return {
                id: provider.identifier,
                title: provider.name,
                description: provider.description,
                dataServiceIds: {
                  providerId: provider.id,
                  evidenceTypeIds: []
                },
                evidenceCount: provider.linkedEvtpCount,
                localTitle: [],
                // TODO: Change .name by .code when the service bug is fixed
                jurisdiction: provider.countryInfo.length > 0 ? { countryCode: provider.countryInfo[0].name } : {}
              };
            });
          }
        }
        providersList = providersList.filter((provider: IProvider) => provider.jurisdiction.countryCode);
        this.providers$.next({ origin, contentType: 'list', content: providersList });
      },
      error: (error) => {
        this.providers$.error({ origin, contentType: 'list', error });
      }
    });
  }

  /**
   * Method to get the details of the providers given as parameter (JSON format)
   * @param {IProvider[]} providers - list containing the providers for which to get the details
   */
  getProvidersDetailList(origin: 'main' | 'detail', providers: IProvider[]): void {
    /* eslint-disable @typescript-eslint/no-explicit-any */
    const requests: Observable<any>[] = providers.map((provider: IProvider) => {
      return this._apiDataEvidences.getProvidersDetails(provider.dataServiceIds.providerId);
    });
    zip(requests).subscribe({
      next: (responseList) => {
        const providersDetails: IProvider[] = [];
        responseList.map((response, index) => {
          const provider: IProvider = {
            ...providers[index]
          };
          if (!provider.id) provider.id = response.identifier;
          provider.localTitle = response.translations
            ? response.translations.map((translation) => {
                return `${translation.language.toUpperCase()}#${translation.name}`;
              })
            : [];
          provider.description = response.description;
          if (response.jurisdictions.lenght > 0) {
            provider.jurisdiction = {
              countryCode: response.jurisdictions[0].countryCode,
              codeLevel2: response.jurisdictions[0].regionCode,
              codeLevel3: response.jurisdictions[0].municipalityCode,
              nameLevel2: response.jurisdictions[0].regionName,
              nameLevel3: response.jurisdictions[0].municipalityName
            };
          } else {
            provider.jurisdiction = {
              countryCode: response.countryCode
            };
          }
          providersDetails.push(provider);
        });
        this.providers$.next({ origin, contentType: 'detail', content: providersDetails });
      },
      error: (error) => {
        this.providers$.error({ origin, contentType: 'detail', error });
      }
    });
  }

  /**
   * Method to get the ids of the evidence types provided (JSON format)
   * @param {IProvider} provider - provider for which to get the evidence types
   */
  getEvidenceTypeDetailsByProvider(origin: 'main' | 'detail', provider: IProvider): void {
    this._apiDataEvidences.getEvidenceTypeDetailByProvider(provider.dataServiceIds.providerId).subscribe({
      /* eslint-disable @typescript-eslint/no-explicit-any */
      next: (response: any) => {
        let evTypesIds: string[] = [];
        if (response) {
          evTypesIds = response.list.map((evidenceType) => {
            return evidenceType.evidenceTypeId;
          });
        }
        const providerDetail: IProvider = {
          ...provider,
          dataServiceIds: {
            providerId: provider.dataServiceIds.providerId,
            evidenceTypeIds: [...evTypesIds]
          }
        };
        this.providers$.next({ origin, contentType: 'list', content: [providerDetail] });
      },
      error: (error) => {
        this.providers$.error({ origin, contentType: 'list', error });
      }
    });
  }
}
