import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { ISearchData, ISearchResult } from './models/search.model';
import { IRequirement } from '@shared/models/requirement.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';
import { IProcedure } from '../../shared/models/procedure.model';
import { sortItems } from '@shared/utils/utils';
import { EntityType } from '@shared/models/common.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  readonly searchResults$ = new Subject<ISearchResult[]>();
  private data: ISearchData;

  /**
   * Method that filter items that match with the search
   * @param {string} search - searched text
   */
  search = (search: string): void => {
    let searchResult: ISearchResult[] = [];
    if (this.data && search) {
      const formattedData = Object.entries(this.data);
      formattedData.map((dataItem: [string, unknown[]]) => {
        const filteredData = this.getFilteredData(search.toLowerCase(), dataItem[0] as EntityType, dataItem[1]);
        searchResult = searchResult.concat(filteredData);
      });
      searchResult = searchResult.sort((a: ISearchResult, b: ISearchResult) => {
        const valueA: string = a.title;
        const valueB: string = b.title;
        return sortItems(valueA, valueB, 'ascending');
      });
    }
    this.searchResults$.next(searchResult);
  };

  /**
   * Method to load the data
   * @param {ISearchData} data - data used to search
   */
  loadData = (data: ISearchData): void => {
    this.data = {
      ...this.data,
      ...data
    };
  };

  /**
   * Method that return the items that match with the search text
   * @param {string} search - searched text
   * @param {string} type - items type
   * @param {any[]} searchItems - data
   * @returns Results array
   */
  private getFilteredData = (search: string, type: EntityType, searchItems: unknown[]): ISearchResult[] => {
    const filteredData: ISearchResult[] = [];
    searchItems.map((searchItem) => {
      switch (type) {
        case 'requirement': {
          const requirement: IRequirement = searchItem as IRequirement;
          if (requirement.name.toLowerCase().includes(search)) {
            filteredData.push({
              id: requirement.id,
              title: requirement.name,
              linkUrl: '/requirements/requirement-detail',
              description: requirement.description,
              type: 'requirement'
            });
          }
          break;
        }
        case 'evidence': {
          const evidence: IEvidenceType = searchItem as IEvidenceType;
          if (evidence.title.toLowerCase().includes(search)) {
            filteredData.push({
              id: evidence.classification,
              title: evidence.title,
              linkUrl: '/evidences/evidence-type-detail',
              description: evidence.description,
              type: 'evidence',
              country: evidence.jurisdiction[0].countryCode
            });
          }
          break;
        }
        case 'provider': {
          const provider: IProvider = searchItem as IProvider;
          if (provider.title.toLowerCase().includes(search)) {
            filteredData.push({
              id: provider.id,
              title: provider.title,
              linkUrl: '/evidences/evidence-provider-detail',
              description: provider.description,
              type: 'provider',
              country: provider.jurisdiction.countryCode
            });
          }
          break;
        }
        case 'procedure': {
          const procedure: IProcedure = searchItem as IProcedure;
          if (procedure.detail.title.toLowerCase().includes(search)) {
            filteredData.push({
              id: procedure.detail.id,
              title: procedure.detail.title,
              linkUrl: '/procedures',
              description: procedure.detail.description,
              type: 'procedure',
              code: procedure.code
            });
          }
          break;
        }
      }
    });
    return filteredData;
  };
}
