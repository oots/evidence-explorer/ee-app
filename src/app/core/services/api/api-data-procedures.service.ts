import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { IAssetCountPerCountry } from '@shared/models/common.model';

@Injectable({
  providedIn: 'root'
})
export class ApiDataProcedures {
  // @ts-ignore
  private readonly _apiQueryUrlProcedure: string = environment.apiUrl + '/ee/api/procedure';

  constructor(private readonly _http: HttpClient) {}

  getProcedureCountByCountry() {
    const url = `${this._apiQueryUrlProcedure}/count`;
    return this._http.get<IAssetCountPerCountry[]>(url);
  }
}
