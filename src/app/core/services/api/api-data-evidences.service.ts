import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IAssetCountPerCountry } from '@shared/models/common.model';

@Injectable({
  providedIn: 'root'
})
export class ApiDataEvidences {
  // @ts-ignore
  private _apiQueryUrlEvidenceType: string = environment.api.baseUrlPublicServices + '/ee/api/evidence-type';
  // @ts-ignore
  private _apiQueryUrlProvider: string = environment.api.baseUrlPublicServices + '/ee/api/provider';

  constructor(private _http: HttpClient) {}

  getEvidenceTypesList() {
    const url = `${this._apiQueryUrlEvidenceType}/list`;
    return this._http.post(url, { filter: [{ fieldName: 'status', fieldValues: ['2'] }] });
  }

  getEvidenceTypeDetail(evidenceTypeId: string) {
    const url = `${this._apiQueryUrlEvidenceType}/details/${evidenceTypeId}`;
    return this._http.get(url);
  }

  getProviderDetailByEvidenceType(evidenceTypeId: string) {
    const url = `${this._apiQueryUrlEvidenceType}/${evidenceTypeId}/providers`;
    // return this._http.post(url, {"filter":[{"fieldName":"status","fieldValues":["2"]}]});            // fixme workaround
    return this._http.post(url, {});
  }

  getProvidersList() {
    const url = `${this._apiQueryUrlProvider}/list`;
    return this._http.post(url, { filter: [{ fieldName: 'status', fieldValues: ['2'] }] });
  }

  getProvidersDetails(providerId: string) {
    const url = `${this._apiQueryUrlProvider}/details/${providerId}`;
    return this._http.get(url);
  }

  getEvidenceTypeDetailByProvider(providerId: string) {
    const url = `${this._apiQueryUrlProvider}/${providerId}/evidence-type`;
    return this._http.post(url, { filter: [{ fieldName: 'status', fieldValues: ['2'] }] });
  }

  getEvidenceTypeCountByCountry() {
    const url = `${environment.apiUrl + '/ee/api/evidence-type'}/count`;
    return this._http.get<IAssetCountPerCountry[]>(url);
  }

  getProviderCountByCountry() {
    const url = `${environment.apiUrl + '/ee/api/provider'}/count`;
    return this._http.get<IAssetCountPerCountry[]>(url);
  }
}
