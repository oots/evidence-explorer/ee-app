import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { IAssetCountPerCountry } from '@shared/models/common.model';

@Injectable({
  providedIn: 'root'
})
export class ApiDataRequirements {
  // @ts-ignore
  private _apiQueryUrl: string = environment.api.baseUrlPublicServices + '/ee/api/requirement';

  constructor(private _http: HttpClient) {}

  getRequirementsList() {
    const url = `${this._apiQueryUrl}/list`;
    return this._http.post(url, { filter: [{ fieldName: 'status', fieldValues: ['2'] }] });
  }

  getRequirementDetails(requirementId: string) {
    const url = `${this._apiQueryUrl}/details/${requirementId}`;
    return this._http.get(url);
  }

  getRequirementCountByCountry() {
    const url = `${environment.apiUrl + '/ee/api/requirement'}/count`;
    return this._http.get<IAssetCountPerCountry[]>(url);
  }

  getRequirementCountDistinct() {
    const url = `${environment.apiUrl + '/ee/api/requirement'}/countDistinct`;
    return this._http.get<number>(url);
  }
}
