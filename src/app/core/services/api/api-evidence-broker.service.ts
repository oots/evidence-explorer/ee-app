import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { IRequirementsServiceQueryParams } from '@shared/models/requirement.model';
import { IEvidenceTypeServiceQueryParams } from '@shared/models/evidence-type.model';
import { CustomHttpParamEncoder, encodeRFC3986URIComponent } from '../../../shared/utils/http';

@Injectable({
  providedIn: 'root'
})
export class ApiEvidenceBrokerService {
  // @ts-ignore
  private _apiBaseUrl: string = environment.api.baseUrlPublicServices + '/eb/rest/search';

  constructor(private _http: HttpClient) {}

  getRequirementsList(params?: IRequirementsServiceQueryParams): Observable<string> {
    const queryParams: HttpParams = new HttpParams({
      fromObject: {
        ...params,
        // @ts-ignore
        queryId: environment.api.requirementsQuerytId
      }
    });
    const options = {
      params: queryParams,
      responseType: 'text' as const
    };
    return this._http.get(this._apiBaseUrl, options);
  }

  getEvidenceTypeList(params?: IEvidenceTypeServiceQueryParams): Observable<string> {
    params['requirement-id'] = encodeRFC3986URIComponent(params['requirement-id']);
    const queryParams: HttpParams = new HttpParams({
      fromObject: {
        ...params,
        // @ts-ignore
        queryId: environment.api.evidencesQueryId
      },
      encoder: new CustomHttpParamEncoder()
    });
    const options = {
      params: queryParams,
      responseType: 'text' as const
    };
    return this._http.get(this._apiBaseUrl, options);
  }
}
