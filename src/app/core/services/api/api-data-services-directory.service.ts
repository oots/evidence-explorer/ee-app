import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { IProvidersServiceQueryParams } from '@shared/models/provider.model';
import { CustomHttpParamEncoder, encodeRFC3986URIComponent } from '@shared/utils/http';

@Injectable({
  providedIn: 'root'
})
export class ApiDataServicesDirectoryService {
  // @ts-ignore
  private _apiBaseUrl: string = environment.api.baseUrlPublicServices + '/dsd/rest/search';

  constructor(private _http: HttpClient) {}

  getEvidenceProviders(params?: IProvidersServiceQueryParams): Observable<string> {
    params['evidence-type-classification'] = encodeRFC3986URIComponent(params['evidence-type-classification']);
    let queryParams: HttpParams = new HttpParams({
      fromObject: {
        ...params,
        // @ts-ignore
        queryId: environment.api.providersQueryId
      },
      encoder: new CustomHttpParamEncoder()
    });
    if (params.evidenceProviderClasParam) {
      queryParams = queryParams.delete('evidenceProviderClasParam');
      queryParams = queryParams.delete('evidenceProviderClasValue');
      queryParams = queryParams.append(params.evidenceProviderClasParam, params.evidenceProviderClasValue);
    }
    const options = {
      params: queryParams,
      responseType: 'text' as const
    };
    return this._http.get(this._apiBaseUrl, options);
  }
}
