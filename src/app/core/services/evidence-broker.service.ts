import { Injectable } from '@angular/core';
import { Observable, Subject, zip } from 'rxjs';
import { xml2js } from 'xml-js';

import { ApiEvidenceBrokerService } from './api/api-evidence-broker.service';
import { IRequirement, IRequirementDetails } from '@shared/models/requirement.model';
import { ProceduresList } from '@shared/utils/constants';
import { IEvidenceType, IEvidenceTypeServiceQueryParams } from '@shared/models/evidence-type.model';
import { IJurisdictionCodes } from '@shared/models/common.model';
import { IProcedure, IProcedureDetail } from '@shared/models/procedure.model';

@Injectable({
  providedIn: 'root'
})
export class EvidenceBrokerService {
  readonly requirementList$ = new Subject<IRequirement[]>();
  readonly requirementsDetails$ = new Subject<IRequirementDetails[]>();
  readonly procedureList$ = new Subject<IProcedure[]>();

  constructor(private _apiEvidenceBrokerService: ApiEvidenceBrokerService) {}

  /**
   * Method to get the list of requirements available (XML format)
   */
  getRequirementsList(): void {
    this._apiEvidenceBrokerService.getRequirementsList().subscribe({
      next: (response) => {
        const requirements: IRequirement[] = [];
        const parsedResponse = xml2js(response, { compact: true, ignoreComment: true, ignoreDeclaration: true, ignoreInstruction: true });
        if (!(parsedResponse['query:QueryResponse']._attributes.status as string).includes('Failure')) {
          let requirementsData = parsedResponse['query:QueryResponse']['rim:RegistryObjectList']['rim:RegistryObject'];
          if (!Array.isArray(requirementsData)) requirementsData = [requirementsData];
          requirementsData.map((requirement) => {
            const id: string = requirement['rim:Slot']['rim:SlotValue']['oots:Requirement']['oots:Identifier']._text;
            const names = requirement['rim:Slot']['rim:SlotValue']['oots:Requirement']['oots:Name'];
            const name: string = Array.isArray(names)
              ? names.find((name) => name._attributes.lang.toLowerCase() === 'en')?._text
              : names?._text;
            requirements.push({ id, name });
          });
        }
        this.requirementList$.next(requirements);
      },
      error: (error) => {
        console.error('Service error:', error);
        this.requirementList$.error(error);
      }
    });
  }

  /**
   * Method to get the list of procedures available (XML format)
   */
  getProcedureList(): void {
    const httpRequests: Observable<string>[] = ProceduresList.map((procedure: IProcedure) => {
      return this._apiEvidenceBrokerService.getRequirementsList({ 'procedure-id': procedure.code });
    });
    zip(httpRequests).subscribe({
      next: (responseList) => {
        const procedureList: IProcedure[] = [];
        responseList.map((response, index) => {
          const parsedResponse = xml2js(response, { compact: true, ignoreComment: true, ignoreDeclaration: true, ignoreInstruction: true });
          if (!(parsedResponse['query:QueryResponse']._attributes.status as string).includes('Failure')) {
            let requirementsData = parsedResponse['query:QueryResponse']['rim:RegistryObjectList']['rim:RegistryObject'];
            if (!Array.isArray(requirementsData)) requirementsData = [requirementsData];
            requirementsData.map((data) => {
              const requirementData = data['rim:Slot']['rim:SlotValue']['oots:Requirement'];
              let proceduresData = requirementData['oots:ReferenceFramework'];
              if (!Array.isArray(proceduresData)) proceduresData = [proceduresData];
              proceduresData.map((procedureDetail) => {
                if (!procedureList.find((procedure: IProcedure) => procedure.detail.id === procedureDetail['oots:Identifier']._text)) {
                  const titleData = Array.isArray(procedureDetail['oots:Title'])
                    ? procedureDetail['oots:Title']
                    : [procedureDetail['oots:Title']];
                  const descriptionData = procedureDetail['oots:Description']
                    ? Array.isArray(procedureDetail['oots:Description'])
                      ? procedureDetail['oots:Description']
                      : [procedureDetail['oots:Description']]
                    : undefined;
                  const jurisdictionData = Array.isArray(procedureDetail['oots:Jurisdiction'])
                    ? procedureDetail['oots:Jurisdiction']
                    : [procedureDetail['oots:Jurisdiction']];
                  const requirementTitle: string = Array.isArray(requirementData['oots:Name'])
                    ? requirementData['oots:Name'].find((name) => name._attributes.lang.toLowerCase() === 'en')._text
                    : requirementData['oots:Name']._text;
                  const detail: IProcedureDetail = {
                    requirementsLinked: [{ id: requirementData['oots:Identifier']._text, name: requirementTitle }],
                    id: procedureDetail['oots:Identifier']._text,
                    title: titleData.find((title) => title._attributes.lang.toLowerCase() === 'en')._text,
                    localTitle: titleData
                      .filter((title) => title._attributes.lang.toLowerCase() !== 'en')
                      ?.map((title) => `${title._attributes.lang.toUpperCase()}#${title._text}`),
                    description: descriptionData
                      ? descriptionData.find((description) => description._attributes.lang.toLowerCase() === 'en')?._text
                      : undefined,
                    localDescription: descriptionData
                      ? descriptionData
                          .filter((description) => description._attributes.lang.toLowerCase() !== 'en')
                          ?.map((description) => description._text)
                      : undefined,
                    jurisdiction: jurisdictionData.map((jurisdiction): IJurisdictionCodes => {
                      return {
                        countryCode: jurisdiction['oots:AdminUnitLevel1']._text,
                        codeLevel2: jurisdiction['oots:AdminUnitLevel2']?._text,
                        codeLevel3: jurisdiction['oots:AdminUnitLevel3']?._text
                      };
                    })
                  };
                  procedureList.push({
                    ...ProceduresList[index],
                    detail: { ...detail }
                  });
                } else {
                  const procedure: IProcedure = procedureList.find(
                    (procedure: IProcedure) => procedure.detail.id === procedureDetail['oots:Identifier']._text
                  );
                  const requirementTitle: string = Array.isArray(requirementData['oots:Name'])
                    ? requirementData['oots:Name'].find((name) => name._attributes.lang.toLowerCase() === 'en')._text
                    : requirementData['oots:Name']._text;
                  procedure.detail.requirementsLinked.push({ id: requirementData['oots:Identifier']._text, name: requirementTitle });
                }
              });
            });
          }
        });
        this.procedureList$.next(procedureList);
      },
      error: (error) => {
        this.procedureList$.error(error);
      }
    });
  }

  /**
   * Method to get the list of evidence types linked with a requirement (XML format)
   * @param {IRequirement[]} requirementsList - list of requirements of which to get the list of evidence types
   * @param {string} countryCode - optional param to filter by the country
   */
  getEvidenceTypeList(requirementsList: IRequirement[], countryCode?: string): void {
    const httpRequests: Observable<string>[] = requirementsList.map((requirement: IRequirement) => {
      const params: IEvidenceTypeServiceQueryParams = { 'requirement-id': requirement.id };
      if (countryCode) params['country-code'] = countryCode;
      return this._apiEvidenceBrokerService.getEvidenceTypeList(params);
    });
    const details: IRequirementDetails[] = [];
    zip(httpRequests).subscribe({
      next: (responsesList) => {
        responsesList.map((response: string, index: number) => {
          const parsedResponse = xml2js(response, { compact: true, ignoreComment: true, ignoreDeclaration: true, ignoreInstruction: true });
          if (!(parsedResponse['query:QueryResponse']._attributes.status as string).includes('Failure')) {
            const data =
              parsedResponse['query:QueryResponse']['rim:RegistryObjectList']['rim:RegistryObject']['rim:Slot']['rim:SlotValue'][
                'oots:Requirement'
              ];
            const evidenceTypesData = data['oots:EvidenceTypeList'] ? data['oots:EvidenceTypeList'] : [];
            const proceduresData = data['oots:ReferenceFramework'] ? data['oots:ReferenceFramework'] : [];
            const evidenceTypeList: IEvidenceType[] = [
              ...this.getEvidenceTypeFromXML(
                Array.isArray(evidenceTypesData) ? evidenceTypesData : [evidenceTypesData],
                requirementsList[index].id
              )
            ];
            const proceduresList: IProcedure[] = [
              ...this.getProceduresFromXML(Array.isArray(proceduresData) ? proceduresData : [proceduresData])
            ];
            details.push({ requirementId: requirementsList[index].id, evidenceTypes: evidenceTypeList, procedures: proceduresList });
          } else {
            details.push({ requirementId: requirementsList[index].id, evidenceTypes: [], procedures: [] });
          }
        });
        this.requirementsDetails$.next(details);
      },
      error: (error) => {
        console.error('Service error:', error);
        this.requirementsDetails$.error(error);
      }
    });
  }

  /**
   * Method to get the information of multiple evidence types from a XML fragment
   * @param {any[]} evidenceTypeLists - fragment of XML with the data
   * @param {string} requirementId - requirement identifier
   * @returns {IEvidenceType[]}
   */
  private getEvidenceTypeFromXML(evidenceTypeLists: unknown[], requirementId: string): IEvidenceType[] {
    const evidenceTypes: IEvidenceType[] = [];
    evidenceTypeLists.map((data) => {
      const evidenceTypesData = Array.isArray(data['oots:EvidenceType']) ? data['oots:EvidenceType'] : [data['oots:EvidenceType']];
      const evidenceTypeListIdentifier: string = data['oots:Identifier']._text;
      evidenceTypesData.map((evidenceTypeData) => {
        const jurisdictionData = Array.isArray(evidenceTypeData['oots:Jurisdiction'])
          ? evidenceTypeData['oots:Jurisdiction']
          : [evidenceTypeData['oots:Jurisdiction']];
        const titles = Array.isArray(evidenceTypeData['oots:Title']) ? evidenceTypeData['oots:Title'] : [evidenceTypeData['oots:Title']];
        const evidenceType: IEvidenceType = {
          classification: evidenceTypeData['oots:EvidenceTypeClassification']?._text,
          evidenceTypeListId: evidenceTypeListIdentifier,
          title: titles.find((title) => title._attributes.lang.toLowerCase() === 'en')?._text,
          requirementId,
          jurisdiction: jurisdictionData.map((jurisdiction): IJurisdictionCodes => {
            return {
              countryCode: jurisdiction['oots:AdminUnitLevel1']._text.toUpperCase(),
              codeLevel2: jurisdiction['oots:AdminUnitLevel2']?._text,
              codeLevel3: jurisdiction['oots:AdminUnitLevel3']?._text
            };
          })
        };
        evidenceType.localTitle =
          titles.length > 1
            ? titles
                .filter((title) => title._attributes.lang.toLowerCase() !== 'en')
                .map((title) => `${title._attributes.lang}#${title._text}`)
            : [evidenceType.title];
        evidenceTypes.push(evidenceType);
      });
    });
    return evidenceTypes;
  }

  /**
   * Method to get the information of multiple providers from a XML fragment
   * @param {any[]} proceduresData - fragment of XML with the data
   * @returns {IProcedure[]}
   */
  private getProceduresFromXML(proceduresData: unknown[]): IProcedure[] {
    const procedures: IProcedure[] = [];
    proceduresData.map((procedureData) => {
      const id: string = procedureData['oots:Identifier']._text;
      const jurisdictionData = Array.isArray(procedureData['oots:Jurisdiction'])
        ? procedureData['oots:Jurisdiction']
        : [procedureData['oots:Jurisdiction']];
      const relatedToElements = Array.isArray(procedureData['oots:RelatedTo'])
        ? procedureData['oots:RelatedTo']
        : [procedureData['oots:RelatedTo']];
      const titles = Array.isArray(procedureData['oots:Title']) ? procedureData['oots:Title'] : [procedureData['oots:Title']];
      relatedToElements.map((relatedToElement) => {
        const code: string = relatedToElement['oots:Identifier']._text;
        const procedureTypeData = ProceduresList.find((procedure: IProcedure) => procedure.code === code);
        if (!procedures.find((procedure: IProcedure) => procedure.code === code && procedure.detail.id === id)) {
          if (ProceduresList.find((procedure: IProcedure) => procedure.code === code)) {
            const procedure: IProcedure = {
              code,
              name: procedureTypeData.name,
              lifeEventLabel: procedureTypeData.lifeEventLabel,
              lifeEventIcon: procedureTypeData.lifeEventIcon,
              detail: {
                id,
                title: titles.find((title) => title._attributes.lang.toLowerCase() === 'en')?._text,
                localTitle: titles
                  .filter((title) => title._attributes.lang.toLowerCase() !== 'en')
                  ?.map((title) => `${title._attributes.lang.toUpperCase()}#${title._text}`),
                jurisdiction: jurisdictionData.map((jurisdiction) => {
                  return {
                    countryCode: jurisdiction['oots:AdminUnitLevel1']._text,
                    codeLevel2: jurisdiction['oots:AdminUnitLevel2']?._text,
                    codeLevel3: jurisdiction['oots:AdminUnitLevel2']?._text
                  };
                })
              }
            };
            procedures.push(procedure);
          } else {
            console.warn('Procedure is undefined');
            console.warn('procedure code: ' + code);
          }
        }
      });
    });
    return procedures;
  }
}
