import { EntityType } from '@shared/models/common.model';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProcedure } from '@shared/models/procedure.model';
import { IProvider } from '@shared/models/provider.model';
import { IRequirement } from '@shared/models/requirement.model';

export interface ISearchData {
  requirement?: IRequirement[];
  evidence?: IEvidenceType[];
  provider?: IProvider[];
  procedure?: IProcedure[];
}

export enum EResultTypes {
  requirement = 'requirement',
  procedure = 'procedure',
  evidence = 'evidence',
  provider = 'provider'
}

export interface ISearchResult {
  id: string;
  title: string;
  linkUrl: string;
  type: EntityType;
  description?: string;
  country?: string;
  code?: string;
}
