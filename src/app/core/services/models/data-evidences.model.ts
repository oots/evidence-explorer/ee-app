import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';

export interface ISubjectContent {
  origin: 'main' | 'detail';
  contentType: 'list' | 'detail';
  content?: IEvidenceType[] | IProvider[];
  error?: unknown;
}
