import { createAction, props } from '@ngrx/store';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';

export const setEvidenceTypesList = createAction('[Evidence] Add Evidence types', props<{ evidencesList?: IEvidenceType[] }>());

export const setEvidenceProvidersList = createAction('[Evidence] Add Evidence providers', props<{ providersList?: IProvider[] }>());

export const setSelectedEvidence = createAction(
  '[Evidence] Set Selected Evidence',
  props<{ evidenceTypeId?: string; evidenceProviderId?: string }>()
);

export const clearSelectedEvidence = createAction(
  '[Evidence] Clear Selected Evidence',
  props<{ type?: 'evidenceType' | 'evidenceProvider' }>()
);
