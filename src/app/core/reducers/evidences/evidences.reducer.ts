import { AppState } from '..';
import { createReducer, on } from '@ngrx/store';
import { clearSelectedEvidence, setSelectedEvidence, setEvidenceTypesList, setEvidenceProvidersList } from './evidences.action';

export const initialState: AppState = {
  evidencesList: null,
  providersList: null,
  selectedEvidence: null,
  selectedProvider: null
};

export const evidencesReducer = createReducer(
  initialState,
  on(setEvidenceTypesList, (state, action) => {
    const newState: AppState = { ...state };
    newState.evidencesList = action.evidencesList ? [...action.evidencesList] : undefined;
    return newState;
  }),
  on(setEvidenceProvidersList, (state, action) => {
    const newState: AppState = { ...state };
    newState.providersList = action.providersList ? [...action.providersList] : undefined;
    return newState;
  }),
  on(setSelectedEvidence, (state, action) => {
    const newState: AppState = { ...state };
    if (action.evidenceTypeId) {
      newState.selectedEvidence = action.evidenceTypeId;
    }
    if (action.evidenceProviderId) {
      newState.selectedProvider = action.evidenceProviderId;
    }
    return newState;
  }),
  on(clearSelectedEvidence, (state, action) => {
    const newState: AppState = { ...state };
    if (action === 'evidenceType') {
      newState.selectedEvidence = undefined;
    } else {
      newState.selectedProvider = undefined;
    }
    return newState;
  })
);
