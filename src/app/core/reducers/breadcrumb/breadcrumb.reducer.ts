import { createReducer, on } from '@ngrx/store';
import { AppState } from '..';
import { setBreadcrumbLabelDetail } from './breadcrumb.actions';

export const initialState: AppState = {
  breadcrumbDetailLabel: null
};

export const breadcrumbReducers = createReducer(
  initialState,
  on(setBreadcrumbLabelDetail, (state, action) => {
    const newState: AppState = { ...state };
    newState.breadcrumbDetailLabel = action.detailLabel;
    return newState;
  })
);
