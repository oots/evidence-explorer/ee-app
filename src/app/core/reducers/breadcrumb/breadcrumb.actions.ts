import { createAction, props } from '@ngrx/store';
import { IBreadcrumbItem } from '../../components/layout/breadcrumb/models/breadcrumb.model';

export const setBreadcrumbLabelDetail = createAction(
  '[Breadcrumb] Set breadcrumb label detail page',
  props<{ detailLabel: IBreadcrumbItem }>()
);
