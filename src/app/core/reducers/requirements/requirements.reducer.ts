import { createReducer, on } from '@ngrx/store';
import { setRequirements, clearSelectedRequirement, setSelectedRequirement } from './requirements.actions';
import { AppState } from '..';

export const initialState: AppState = {
  requirementsList: null,
  selectedRequirement: null
};

export const requirementsReducer = createReducer(
  initialState,
  on(setRequirements, (state, action) => {
    const newState: AppState = { ...state };
    newState.requirementsList = action.requirements ? [...action.requirements] : undefined;
    return newState;
  }),
  on(setSelectedRequirement, (state, action) => {
    const newState: AppState = { ...state };
    newState.selectedRequirement = action.requirementId;
    return newState;
  }),
  on(clearSelectedRequirement, (state) => {
    const newState: AppState = { ...state };
    newState.selectedRequirement = undefined;
    return newState;
  })
);
