import { createAction, props } from '@ngrx/store';
import { IRequirement } from '@shared/models/requirement.model';

export const setRequirements = createAction('[Requirement] Add Requirement', props<{ requirements: IRequirement[] }>());

export const setSelectedRequirement = createAction('[Requirement] Set Selected Requirement', props<{ requirementId: string }>());

export const clearSelectedRequirement = createAction('[Requirement] Clear Selected Requirement');
