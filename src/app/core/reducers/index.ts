import { InjectionToken } from '@angular/core';
import { MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { CoreState, localStorageSync, reducers as coreReducers } from '@eui/core';

import { environment } from '../../../environments/environment';
import { requirementsReducer } from './requirements/requirements.reducer';
import { IRequirement } from '@shared/models/requirement.model';
import { breadcrumbReducers } from './breadcrumb/breadcrumb.reducer';
import { IEvidenceType } from '@shared/models/evidence-type.model';
import { IProvider } from '@shared/models/provider.model';
import { evidencesReducer } from './evidences/evidences.reducer';
import { IProcedure } from '@shared/models/procedure.model';
import { procedureReducer } from './procedures/procedures.reducer';
import { IProcedureIdentifier } from './procedures/models/procedures-state.model';
import { IBreadcrumbItem } from '../components/layout/breadcrumb/models/breadcrumb.model';

// eslint-disable-next-line
export const REDUCER_TOKEN = new InjectionToken<any>('Registered Reducers');

/**
 * Define here your app state
 *
 * [IMPORTANT]
 * There are some **reserved** slice of the state
 * that you **can not** use in your application ==> app |user | notification
 */
// eslint-disable-next-line
export interface AppState extends CoreState {
  requirementsList?: IRequirement[];
  selectedRequirement?: string;
  evidencesList?: IEvidenceType[];
  selectedEvidence?: string;
  providersList?: IProvider[];
  selectedProvider?: string;
  proceduresList?: IProcedure[];
  selectedProcedure?: IProcedureIdentifier;
  breadcrumbDetailLabel?: IBreadcrumbItem;
  requirements?;
  evidences?;
  procedures?;
  breadcrumb?;
}

/**
 * Define here the reducers of your app
 */
const rootReducer = Object.assign({}, coreReducers, {
  requirements: requirementsReducer,
  evidences: evidencesReducer,
  procedures: procedureReducer,
  breadcrumb: breadcrumbReducers
});

export const getReducers = () => rootReducer;

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [localStorageSync, storeFreeze] : [localStorageSync];
