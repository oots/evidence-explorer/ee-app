import { AppState } from '..';
import { createReducer, on } from '@ngrx/store';
import { clearSelectedProcedure, setProcedures, setSelectedProcedure } from './procedures.actions';

export const initialState: AppState = {
  proceduresList: null,
  selectedProcedure: null
};

export const procedureReducer = createReducer(
  initialState,
  on(setProcedures, (state, action) => {
    const newState: AppState = { ...state };
    newState.proceduresList = action.procedures ? [...action.procedures] : undefined;
    return newState;
  }),
  on(setSelectedProcedure, (state, action) => {
    const newState: AppState = { ...state };
    newState.selectedProcedure = action.procedureId;
    return newState;
  }),
  on(clearSelectedProcedure, (state) => {
    const newState: AppState = { ...state };
    newState.selectedProcedure = undefined;
    return newState;
  })
);
