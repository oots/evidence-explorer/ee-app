export interface IProcedureIdentifier {
  id: string;
  nameLevel2?: string;
}
