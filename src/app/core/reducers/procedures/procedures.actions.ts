import { createAction, props } from '@ngrx/store';
import { IProcedure } from '@shared/models/procedure.model';
import { IProcedureIdentifier } from './models/procedures-state.model';

export const setProcedures = createAction('[Procedure] Add Procedure', props<{ procedures: IProcedure[] }>());

export const setSelectedProcedure = createAction('[Procedure] Set Selected Procedure', props<{ procedureId: IProcedureIdentifier }>());

export const clearSelectedProcedure = createAction('[Procedure] Clear Selected Procedure');
