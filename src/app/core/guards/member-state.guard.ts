import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { COUNTRIES } from '@shared/utils/constants';

export const memberStateGuard: CanActivateFn = (route, state) => {
  const selectedCountry: string = route?.params['country'];
  const router = inject(Router);
  if (COUNTRIES.find((c) => c.code.toLowerCase() === selectedCountry.toLowerCase())) {
    return true;
  }
  router.navigate(['not-found']);
  return false;
};
