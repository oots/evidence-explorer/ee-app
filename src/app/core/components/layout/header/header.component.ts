import { Component, OnInit } from '@angular/core';
import { EMainPageSectionId } from '../breadcrumb/models/breadcrumb.model';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { CountryModel, ECountryCodeList } from '@shared/models/common.model';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { COUNTRIES } from '@shared/utils/constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent implements OnInit {
  currentLang: string;
  currentResolution: string;
  mainPageSectionIds = EMainPageSectionId;
  countries: CountryModel[];

  constructor(
    private _translateService: TranslateService,
    private _breakpointObserver: BreakpointObserver,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._translateService.setDefaultLang('en');
    this.currentLang = ECountryCodeList['EN'];
    this._breakpointObserver.observe(['(max-width: 1007px)', '(max-width: 1280px)']).subscribe((state: BreakpointState) => {
      this.currentResolution = state.breakpoints['(max-width: 1007px)']
        ? 'mobile'
        : state.breakpoints['(max-width: 1280px)']
          ? 'tablet'
          : 'desktop';
      this.getCountries();
    });
  }

  checkCurrentSection = (sectionId: string): boolean => {
    const urlSections: string[] = this.router.url.split('/').slice(1);
    return urlSections.includes(sectionId);
  };

  getCountries() {
    this.countries = COUNTRIES;
  }

  trackBy = (index: number, country: CountryModel) => {
    return country.code;
  };
}
