import { Component, OnDestroy } from '@angular/core';
import { BreadcrumbItems, breadcrumbStringMaxLength, EMainPageSectionId, IBreadcrumbItem } from './models/breadcrumb.model';
import { NavigationEnd, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../reducers';
import { Subscription } from 'rxjs';
import { getSubstring } from '@shared/utils/utils';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnDestroy {
  breadcrumbPath: IBreadcrumbItem[] = [
    {
      id: EMainPageSectionId.home,
      label: 'app.header.menu-item.home',
      href: '/home'
    }
  ];

  private storeSubcription: Subscription;
  private routerSubscription: Subscription;

  constructor(
    private router: Router,
    private _store: Store<AppState>
  ) {
    this.routerSubscription = this.router.events.subscribe((routeEvent) => {
      if (routeEvent instanceof NavigationEnd) {
        this.updateBreadcrumbPath();
      }
    });
    this.storeSubcription = this._store.select('breadcrumb').subscribe((state: AppState) => {
      if (state.breadcrumbDetailLabel) {
        const breadcrumbItem: IBreadcrumbItem = this.breadcrumbPath.find(
          (item: IBreadcrumbItem) => item.id === state.breadcrumbDetailLabel.id
        );
        if (breadcrumbItem) {
          breadcrumbItem.label = getSubstring(state.breadcrumbDetailLabel.label, breadcrumbStringMaxLength);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.storeSubcription.unsubscribe();
    this.routerSubscription.unsubscribe();
  }

  onBreadcrumbSegmentClick = (item: IBreadcrumbItem): void => {
    this.router.navigate([item.href]);
  };

  checkCurrentItem = (item): boolean => {
    return this.breadcrumbPath.indexOf(item) === this.breadcrumbPath.length - 1;
  };

  checkBackground = (): boolean => {
    return this.breadcrumbPath.length > 2;
  };

  private updateBreadcrumbPath = (): void => {
    const urlRemovedParams: string = this.router.url.split('?')[0];
    const urlSections: string[] = urlRemovedParams.split('/').slice(1);
    this.breadcrumbPath = [{ id: EMainPageSectionId.home, label: 'app.header.menu-item.home', href: 'home' }];
    if (!urlSections.includes('home')) {
      urlSections.map((section: string) => {
        const item: IBreadcrumbItem = BreadcrumbItems.find((item: IBreadcrumbItem) => item.id === section);
        if (item) {
          const breadcrumbItem: IBreadcrumbItem = {
            ...item,
            href: `${section}`
          };

          if (urlSections[0] === 'member-state') {
            breadcrumbItem['label'] = `breadcrumbs.ms-page.label.${urlSections[1].toUpperCase()}`;
          }
          this.breadcrumbPath.push(breadcrumbItem);
        }
      });
    }
  };
}
