export interface IBreadcrumbItem {
  id: string;
  label?: string;
  href?: string;
}

export enum EMainPageSectionId {
  'home' = 'home',
  'search' = 'search-results',
  'requirements' = 'requirements',
  'evidences' = 'evidences',
  'procedures' = 'procedures',
  'dashboard' = 'dashboard',
  'requirement-detail' = 'requirement-detail',
  'evidence-detail' = 'evidence-type-detail',
  'provider-detail' = 'evidence-provider-detail',
  'procedure-detail' = 'procedure-detail',
  'member-state' = 'member-state'
}

export const BreadcrumbItems: IBreadcrumbItem[] = [
  {
    id: EMainPageSectionId.home,
    label: 'app.header.menu-item.home'
  },
  {
    id: EMainPageSectionId.search,
    label: 'app.header.menu-item.search'
  },
  {
    id: EMainPageSectionId.requirements,
    label: 'app.header.menu-item.requirements'
  },
  {
    id: EMainPageSectionId.evidences,
    label: 'app.header.menu-item.evidences-providers'
  },
  {
    id: EMainPageSectionId.procedures,
    label: 'app.header.menu-item.procedures'
  },
  {
    id: EMainPageSectionId['requirement-detail'],
    label: 'app.header.menu-item.requirement-detail'
  },
  {
    id: EMainPageSectionId['evidence-detail'],
    label: 'app.header.menu-item.evidence-type-detail'
  },
  {
    id: EMainPageSectionId['provider-detail'],
    label: 'app.header.menu-item.evidence-provider-detail'
  },
  {
    id: EMainPageSectionId['procedure-detail'],
    label: 'app.header.menu-item.procedure-detail'
  },
  {
    id: EMainPageSectionId['member-state'],
    label: 'app.header.menu-item.member-state'
  },
  {
    id: EMainPageSectionId['dashboard'],
    label: 'app.header.menu-item.dashboard'
  }
];

export const breadcrumbStringMaxLength = 85;
