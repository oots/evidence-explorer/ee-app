import { Component } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.scss'
})
export class FooterComponent {
  version: string = environment.version.toString();
  sitemap = [
    {
      translationKey: 'app.header.menu-item.home',
      routerLink: '/home'
    },
    {
      translationKey: 'app.header.menu-item.requirements',
      routerLink: '/requirements'
    },
    {
      translationKey: 'app.header.menu-item.evidences-providers',
      routerLink: '/evidences'
    },
    {
      translationKey: 'app.header.menu-item.procedures',
      routerLink: '/procedures'
    }
  ];
}
