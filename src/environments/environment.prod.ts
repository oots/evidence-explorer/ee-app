import { EuiEnvConfig } from '@eui/core';

export const environment: EuiEnvConfig = {
  production: true,
  enableDevToolRedux: true,
  apiUrl: 'https://query.cs.oots.tech.ec.europa.eu',
  api: {
    baseUrlPublicServices: 'https://query.cs.oots.tech.ec.europa.eu',
    requirementsQuerytId: 'urn:fdc:oots:eb:ebxml-regrep:queries:requirements-by-procedure-and-jurisdiction',
    evidencesQueryId: 'urn:fdc:oots:eb:ebxml-regrep:queries:evidence-types-by-requirement-and-jurisdiction',
    providersQueryId: 'urn:fdc:oots:dsd:ebxml-regrep:queries:dataservices-by-evidencetype-and-jurisdiction'
  },
  version: '1.2.2'
};
